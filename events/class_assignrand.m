function labels = class_assignrand(enum, tnum, cnum, niter)
labels = randi(enum, tnum, cnum, niter);
end