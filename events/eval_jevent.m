function [Precision, Recall, FScore, NaiveMeas] = ...
    eval_jevent(class_true, class_prob, enum, tnum, cnum)

total = cnum * tnum;
labels = 1:enum;

[TP, FP, TN, FN, bflag, PP, P] = calcConfusionMatrix(class_true, ...
    class_prob, labels);
[P_total, R_total, F_total, Naive_total] = ...
    calcPRF(TP, FP, TN, FN, bflag, total);

P_countries = zeros(cnum, 2);
R_countries = zeros(cnum, 2);
F_countries = zeros(cnum, 2);
Naive_countries = zeros(cnum, 1);
for n = 1:cnum
    [TP, FP, TN, FN, bflag] = ...
        calcConfusionMatrix(class_true(:,n), class_prob(:,n), labels);
    [P_countries(n,:), R_countries(n,:), F_countries(n,:), ...
        Naive_countries(n)] = calcPRF(TP, FP, TN, FN, bflag, tnum);
end

Precision.total = P_total;
Precision.countries = P_countries;
Recall.total = R_total;
Recall.countries = R_countries;
FScore.total = F_total;
FScore.countries = F_countries;
NaiveMeas.total = Naive_total;
NaiveMeas.countries = Naive_countries;
end

function [TP, FP, TN, FN, bflag, PP, P] = calcConfusionMatrix(class_true, ...
    class_prob, labels)
enum = numel(labels);
TP = zeros(1, enum);
FP = zeros(1, enum);
TN = zeros(1, enum);
FN = zeros(1, enum);
PP = zeros(1, enum);
P = zeros(1, enum);
bflag = zeros(1, enum);
for e = 1:enum
    PP(e) = numel(find(class_prob==labels(e)));
    P(e) = numel(find(class_true==labels(e)));
    TP(e) = numel(intersect(find(class_true==labels(e)), ...
        find(class_prob==labels(e)))); % TP
    FP(e) = numel(intersect(find(class_true~=labels(e)), ...
        find(class_prob==labels(e)))); % FP
    TN(e) = numel(intersect(find(class_true==labels(e)), ...
        find(class_prob~=labels(e)))); % FN
    FN(e) = numel(intersect(find(class_true~=labels(e)), ...
        find(class_prob~=labels(e)))); % TN
    
    if TP(e) == 0 && FP(e) == 0 && FN(e) == 0
        bflag(e) = 1;
    elseif TP(e) == 0 && FP(e) == 0
        bflag(e) = -1; % no predictive positive (meaningless precision)
    elseif TP(e) == 0 && FN(e) == 0
        bflag(e) = -1; % no real positive (meaningless recall)
    end
end
end

function [P, R, F, Naive] = calcPRF(TP, FP, TN, FN, bflag, total)
% TP = TP(bflag ~= -1);
% FP = FP(bflag ~= -1);
% FN = FN(bflag ~= -1);

Pidv = TP ./ (TP + FP);
Pidv(bflag == 1) = 1;
Pidv(bflag == -1) = 0;

Ridv = TP ./ (TP + FN);
Ridv(bflag == 1) = 1;
Ridv(bflag == -1) = 0;

Pmacro = mean(Pidv);
Rmacro = mean(Ridv);
Fmacro = 2 * Pmacro * Rmacro / (Pmacro + Rmacro);
Fmacro(isnan(Fmacro)) = 0;

Pmicro = sum(TP)/sum(TP + FP);
Rmicro = sum(TP)/sum(TP + FN);
Fmicro = 2 * Pmicro * Rmicro / (Pmicro + Rmicro);
Fmicro(isnan(Fmicro)) = 0;

P = [Pmacro Pmicro];
R = [Rmacro Rmicro];
F = [Fmacro Fmicro];
Naive = sum(TP) / total;
end