function jevent_prob = calc_jevent(endovar,foreignvar,freq)
% function jevent_prob = calc_jevent(endovar,foreignvar,freq)
%
% ***** Definition of Events *****
% event 1. event(endo.logtexp) >= 0 == 1 && event(foreign.logGDP) >= 0 == 1
% event 2. event(endo.logtexp) >= 0 == 0 && event(foreign.logGDP) >= 0 == 1
% event 3. event(endo.logtexp) >= 0 == 0 && event(foreign.logGDP) >= 0 == 0
% event 4. event(endo.logtexp) >= 0 == 1 && event(foreign.logGDP) >= 0 == 0
%
% ***** Required Input *****
% - endovar: a field of struct(fieldnames = endonames, field = [ntime cnum] numeric
% matrix) % endovar = endodata.logtexp;
% - foreignvar: a field of struct(fieldnames = endonames, field = 
% [ntime cnum] numeric matrix) % foreignvar = foreign.logGDP;
% - freq
%
% ***** Output *****
% - jevent_prob

fhorz = size(endovar,1); ntrials = size(endovar,3); cnum = size(endovar,2);

event_endo = event_cyclic3d(endovar, freq, 0);
event_foreign = event_cyclic3d(foreignvar, freq, 0);

jevent_prob = zeros(4, fhorz-freq-1, cnum);
for n = 1:cnum
    for t = 1:(fhorz-freq-1)
        % event 1.
        endo_tr = find(event_endo(t,n,:) == 1);
        foreign_tr = find(event_foreign(t,n,:) == 1);
        jevent_prob(1,t,n) = numel(intersect(endo_tr, foreign_tr)) / ...
            ntrials;
        
        % event 2.
        endo_tr = find(event_endo(t,n,:) == 0);
        foreign_tr = find(event_foreign(t,n,:) == 1);
        jevent_prob(2,t,n) = numel(intersect(endo_tr, foreign_tr)) / ...
            ntrials;
        
        % event 3.
        endo_tr = find(event_endo(t,n,:) == 0);
        foreign_tr = find(event_foreign(t,n,:) == 0);
        jevent_prob(3,t,n) = numel(intersect(endo_tr, foreign_tr)) / ...
            ntrials;
        
        % event 4.
        endo_tr = find(event_endo(t,n,:) == 1);
        foreign_tr = find(event_foreign(t,n,:) == 0);
        jevent_prob(4,t,n) = numel(intersect(endo_tr, foreign_tr)) / ...
            ntrials;
        
        jevent_prob(:,t,n) = jevent_prob(:,t,n) ./ sum(jevent_prob(:,t,n));
    end
    
end

end