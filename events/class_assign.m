function [labels, enum, tnum, cnum] = class_assign(jevent)

enum = size(jevent, 1);
tnum = size(jevent, 2);
cnum = size(jevent, 3);

% get class label
labels = zeros(tnum, cnum);
for i = 1:tnum
    for j = 1:cnum
        labels_tmp = find(jevent(:,i,j) == max(jevent(:,i,j)));
        if length(labels_tmp) > 1
            idx = randi(length(labels_tmp), 1);
            labels_tmp = labels_tmp(idx);
        end
        labels(i,j) = labels_tmp;
    end
end

end