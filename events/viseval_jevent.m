function [] = viseval_jevent(jevent_true, jevent_prob, cnames, ...
    savepath, caption)

if ~isempty(savepath)
    checkPath(savepath);
end

ne = size(jevent_prob, 1);
event_def = [1 1; 0 1; 0 0; 1 0];
legs = arrayfun(@(x) ['event ' num2str(x) ...
    ': [' num2str(event_def(x,1)) ', ' num2str(event_def(x,2)) ']'], ...
    1:ne, 'UniformOutput', 0);

for n = 1:numel(cnames)
    f = figure;
    ax = subplot(2,1,1);
    ax.YLimMode = 'manual';
    ax.YLim = [0 1];
    hold on
    for i = 1:ne
        plot(jevent_prob(i,:,n));
    end
    legend(legs, 'Location', 'eastoutside');
    hold off
    title('Probability Estimated with Monte Carlo Simulation');
    
    subplot(2,1,2);
    hold on
    for i = 1:ne
        plot(jevent_true(i,:,n));
    end
    legend(legs, 'Location', 'eastoutside');
    hold off
    title('Ground Truth');
    
    suptitle([cnames{n} '  ' caption]);
    
    if ~isempty(savepath)
        print(fullfile(savepath, ['joint_event_' cnames{n} '.png']), ...
            f, '-dpng');
    end
    
    close all
    delete(f);
    delete(gcf);
end


end