function event = event_cyclic3d(data, freq, threshold)
% function event = event_cyclic3d(data, freq, tdim, threshold)
%
% ***** Required Input *****
% - data: 2d/3d numeric matrix, [ntimes * nvar/ncty (* ntrials)]
% - freq: numeric integer
% - threshold
%
% ***** Output *****
% - event: 2d/3d matrix, [ntimes-freq-1 nvar/ncty ntrials]

if numel(size(data))>3 || numel(size(data))<2
    error('INPUT ERROR: illegal input.');
end

diff_term = diff(data(freq+1:end, :, :), 1, 1); % t = (freq+2) : end
diff_freq = diff_lag(data, freq, 1); % t = (freq+1) : end
diff_freq = 1/freq .* diff_freq(1:end-1, :, :);
event = diff_term - diff_freq;
event = event >= threshold;
end
