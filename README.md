# VECM Modeling

## GVAR Introduction

## Toolbox Sturcture

### Dependencies

- MATLAB 2018b
- GVAR_Toolbox2.0
- GFLseg-1.0

### Preprocessing

1. `main_preprocess_xtract_raw`
2. `main_preprocess_impute_missing_*`, `main_preprocess_add_missing`, `main_preprocess_delctys`
3. `main_preprocess_calc_newvar`
4. `main_preprocess_seasAdjust*`
5. `main_preprocess_logtrans`
6. `main_postprocess_detect_cpnts`, `main_postprocess_def_cpnts`
7. `main_postprocess_split_traindata`

### Available Models

- GVAR
    + Training: `train_gvar`
    + Forecasting: `forecast_gvar`
    + Simulation: `simulate_gvar`
- country-specific VAR
    + Training: `train_var`
    + Forecasting: `forecast_var`
    + Simulation: `simulate_var`
- country-specific VECM
    + Training: `train_vecm`
    + Forecasting: `forecast_vecm`
    + Simulation: `simulate_vecm`
- variable-specific AR
    + Training: `train_ar`
    + Forecasting: `forecast_ar`
    + Simulation: `simulate_ar`
- variable-specific ARIMA
    + Training: `train_arima`
    + Forecasting: `forecast_arima`
    + Simulation: `simulate_arima`

### Available Forecasting Method

- Point Forecasting: `forecast_*`
- Probability Forecasting: `simulate_*`
- Scenario Forecasting: 
    1. run `simulate_*` to get the probability distribution
    2. use `calc_jevent` to compute the probability of a specific event
    3. use `class_assign` to determine the event assignment based on the probability
    4. use `eval_jevent` to compute the precision, recall, f1-score and naive metrics for evaluation

## Example

## Variable Definition

- cnames: 1d char array, country names
- date_info: struct, fieldnames = {'start', 'end', 'freq'}

- endonames: 1d char array, names of endogenous variable and foreign variable
- endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
- endodata: struct, fieldnames = endonames, field = [ntime cnum ntrial] 
numeric matrix
- endog: struct, fieldnames = cnames, field = [ntime dvnum ntrial]
- endoglist = dv, struct, fieldnames = cnames, field = 1d char array
- endovar: 2d/3d numeric matrix, [ntime cnum ntrial]
- dv: struct, fieldnames = endonames, field = struct(fieldnames=cnames, field=ntime*ntrial)
- dv_t: struct, fieldnames = 'y[year]', field = struct
    (
    fieldnames=endonames, field=struct(fieldnames=cnames, field=freq*ntrial)
    )
- gvnames: 1d char array, names of global (strong+weak) variable
- gvflag: [cnum gvnum] numeirc matrix, 0=not included, 1=included as exogenous, 2=included as endogenous
- gvdata/gv_full: struct, fieldnames = gvnames, field = [ntime ntrial] numeric matrix
- exog: struct of exogenous variable, fieldnames = cnames, field = [ntime gvnum ntrial]
- exoglist = fv + gv, struct, fieldnames = cnames, field = 1d char array
- foreign: struct, fieldnames = cnames, field = [ntime vnum ntrial]
- foreigndata: struct, fieldnames = endonames, field = [ntime cnum ntrial] numeric matrix
- foreign: 2d/3d numeric matrix, [ntime cnum ntrial]
- gnvnames: 1d char array, names of global (weak) variable
- exognx: struct of weakly exogenous variable, fieldnames = cnames, field = [ntime gnvnum ntrial]
- exognxlist = fv + gnv, struct, fieldnames = cnames, field = 1d char array
- wdata: [(nyear*cnum) cnum] 2d numeric matrix
- wm_t: struct, fieldnames = yearnames, field = [dv+fv all(dv+fv)]numeric matrix, ref: def_weight_tv
- W_tv/linkmat_z: struct, fieldnames = cnames, field = [dv+fv all(dv+fv) yearnames]
- X: [nvar ntime ntrial] numeric matrix
- rank: struct, rank of country-specific vecm, fieldnames = cnames, field = numeric number
- varxlag: struct, lag of country-specific var, fieldnames = cnames, field = [1 2] numeric array

## References

- GVAR_toolbox2.0
- Group Fused Lasso