import scrapy
import os,csv,re
import pandas as pd

class UserinfoSpider(scrapy.Spider):
    name = "cntryAbbr"

    def start_requests(self):
        url = 'https://www.worldatlas.com/aatlas/ctycodes.htm'
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        cty_long = []
        cty_short = []
        path2tbl = '/html/body/div[4]/main/div/article/div[2]/table/tbody'

        i = 2
        rowpath = path2tbl + '/tr[{}]'.format(i)
        row = response.xpath(rowpath).extract()
        while len(row) > 0:
            tmp_long = response.xpath(rowpath + '/td[1]').extract_first()
            tmp_short = response.xpath(rowpath + '/td[3]').extract_first()
            tmp_long = re.sub('<.*?>', '', tmp_long)
            tmp_short = re.sub('<.*?>', '', tmp_short)

            cty_long.append(tmp_long)
            cty_short.append(tmp_short)

            i += 1
            rowpath = path2tbl + '/tr[{}]'.format(i)
            row = response.xpath(rowpath).extract()


        cty_abbr = pd.DataFrame({'cty_long':cty_long, 'cty_short':cty_short})
        cty_abbr.to_csv('scrapped_out.csv', index=False)