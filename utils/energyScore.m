function [score] = energyScore(sample, obs)
% function [score] = energyScore(sample, obs)
% sample = npaths * nvar
% obs = 1 * nvar

obsMap = repmat(obs, size(sample,1), 1);
delta = sample - obsMap;
esComp1 = (1/size(sample,1)) .* trace(sqrt(delta * delta'));
esComp2 = mean(reshape(pdist2(sample, sample, 'euclidean'),1,[])) ./ 2;
score = esComp1 - esComp2;
end