function [Wfull_tv, wmfull_t] = addnew_weight(W_tv, wm_t, wdata_new, ...
    endog, endoglist, exog, exoglist, varargin)
% wdata_new = wdata_oneyear [nyear_estimation * cnum, 1 + cnum]

nargin_bnd = 7;
if nargin < nargin_bnd + 1
    w_inityear = 1;
else
    w_inityear = varargin{1};
end

if nargin < nargin_bnd + 2
    w_lastyear = 1;
else
    w_lastyear = varargin{2};
end

cnames = fieldnames(W_tv);
cnum = numel(cnames);
if rows(wdata_new) < cnum
    error('Incomplete weight matrix.\n');
end

wm_t_new = gen_weight(wdata_new, w_inityear, w_lastyear);
[W_tv_new, weightLabels_new] = create_tv_linkmatrices(endog, ...
    endoglist, exog, exoglist, wm_t_new, 0, [], [], []);

for n = 1:cnum
    Wfull_tv.(cnames{n}) = cat(3, W_tv.(cnames{n}), W_tv_new.(cnames{n}));
end

Nnew = numel(weightLabels_new);
wmfull_t = wm_t;
for i = 1:Nnew
    wmfull_t.(weightLabels_new{i}) = wm_t_new.(weightLabels_new{i});
end

end