function [endog,endoglist,exog,exoglist,exognx,exognxlist,wm_t,foreign,X] ...
    = gen_params(cnames,date_info,endonames,gvnames,gnvnames,endoflag,...
    gvflag,endodata,gvdata,wdata,varargin)
% function [Outputs] = gen_params(Inputs)
%
% ***** Required Input *****
% > cnames: 1d char array, country names
% > date_info: struct, fieldnames = {'start', 'end', 'freq'}
% > endonames: 1d char array, names of endogenous variable and foreign
% variable
% > gvnames: 1d char array, names of global (strong+weak) variable
% > gnvnames: 1d char array, names of global (weak) variable
% > endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
% > gvflag: [cnum gvnum] numeirc matrix, 0=not included, 1=included as
% exogenous, 2=included as endogenous
% > endodata: struct, fieldnames = endonames, field = [ntime cnum] numeric
% matrix
% > gvdata: struct, fieldnames = gvnames, field = [ntime 1] numeric matrix
% > wdata: [(nyear*cnum) cnum] 2d numeric matrix
%
% ***** Output Variables *****
% %dv: domestic variable / endogenous variable
% %fv: foreign variable
% %gv: global variable
% %gnv: global weak-exogenous variable
%
% > endog: struct, fieldnames = cnames, field = [ntime dvnum]
% > endoglist = dv, struct, fieldnames = cnames, field = 1d char array
% > exog: struct of exogenous variable, fieldnames = cnames, 
% field = [ntime gvnum]
% > exoglist = fv + gv, struct, fieldnames = cnames, field = 1d char array
% > exognx: struct of weakly exogenous variable, fieldnames = cnames, 
% field = [ntime gnvnum]
% > exognxlist = fv + gnv, struct, fieldnames = cnames, field = 1d char
% array
% > wm_t: struct, fieldnames = yearnames, field = [dv+fv all(dv+fv)]
% numeric matrix, ref: def_weight_tv
% > foreign: struct, fieldnames = cnames, field = [ntime vnum]
% > X: [nvar ntime] numeric matrix

ntrial = size(endodata.(endonames{1}),3);
freq = date_info.freq;
gvdata = normalize_gvdata(gvdata,ntrial);

%%
% dv: domestic variable / endogenous variable
% fv: foreign variable
% gv: global variable
% gnv: global weak-exogenous variable
%
% > endoglist = dv, struct, fieldnames = cnames, field = 1d char array
% > exoglist = fv + gv, struct, fieldnames = cnames, field = 1d char array
% > exognxlist = fv + gnv, struct, fieldnames = cnames, field = 1d char array
% > gnvflag: 1d numeric array, flag of gnv in gv, numel(gnvflag)=gvnum,
% 0=not gnv, 1=is gnv

endoglist = get_endoglist(cnames,endonames,endoflag,gvnames,gvflag);
[exoglist,exognxlist] = get_exoglist(cnames,endonames,gvnames,...
    gnvnames,endoflag,gvflag);

%% Weight matrix
% > wdata = 504(ncountry(24)*nyear(26))*26(1(year)+1(countryName)+ncountry)
% > wm_t: struct, fieldnames = yearnames'y[year]', 
% field = [dv+fv all(dv+fv)] numeric matrix, ref: def_weight_tv

if ~isempty(wdata)
    wm_t = gen_weight(wdata, date_info.start, date_info.end);
else
    wm_t = def_weight_tv([]);
end

%% Creating foreign-specific variables
% generate structures of domestic, foreign variables and weights (where
% weights could be either fixed or time-varying)
[endog,X] = create_endogvars(endodata,gvdata,endoglist,cnames);
foreign = create_foreignvars(freq,wm_t,X,endoglist,exognxlist);
[exog,exognx] = create_exogvars(foreign,gvdata,endoglist,exoglist,...
    exognxlist,cnames);

end

%%
function gvdata = normalize_gvdata(gvdata,ntrial)
fm = fieldnames(gvdata);
for i = 1:numel(fm)
    tmp = gvdata.(fm{i});
    if size(tmp,2)==1 && size(tmp,2)<ntrial
        gvdata.(fm{i}) = repmat(tmp,1,ntrial);
    end
end

end