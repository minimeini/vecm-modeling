function [cnames,endonames,fvnames,gvnames,gnvnames,cnum,endonum,fvnum,...
    gvnum,gnvnum] = getNames(endoglist,exoglist,exognxlist)

cnames = fieldnames(endoglist); cnum = numel(cnames);

% endonames
endonames = [];
for n = 1:cnum
    if ~isempty(exognxlist)
        endonames = [endonames intersect(endoglist.(cnames{n}), ...
            exognxlist.(cnames{n}), 'stable')];
    else
        endonames = [endonames endoglist.(cnames{n})];
    end
end
endonames = unique(endonames, 'stable');
endonum = numel(endonames);

% global variables: strong+weak, exclude foreign variables
gvnames = setdiff(unique(flatten_struct(exoglist),'stable'), ...
    endonames, 'stable');
gvnum = numel(gvnames); gvnames = sort_names(gvnames, exoglist);

% global variables: weak, exclude foreign variables
if ~isempty(exognxlist)
    gnvnames = setdiff(unique(flatten_struct(exognxlist),'stable'), ...
        endonames, 'stable');
    gnvnum = numel(gnvnames); gnvnames = sort_names(gnvnames, exognxlist);
else
    gnvnames = [];
    gnvnum = 0;
end

% foreign variables
fvnames = intersect(setdiff(unique(flatten_struct(exoglist),'stable'), ...
    gvnames,'stable'), endonames, 'stable');
fvnum = numel(fvnames); fvnames = sort_names(fvnames, exoglist);


end