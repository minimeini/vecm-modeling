function [trdata, tsdata] = update_data_train_orig(var_info,trdata,tsdata)
freq = trdata.date_info.freq;
cnum = numel(var_info.cnames);
nextyear_start = mod(trdata.obs_train,freq)==0;

for i = 1:numel(var_info.endonames)
    tmp = tsdata.endodata.(var_info.endonames{i});
    trdata.endodata.(var_info.endonames{i}) = ...
        [trdata.endodata.(var_info.endonames{i}); tmp(1,:,:)];

    if size(tmp,1)>1
        tsdata.endodata.(var_info.endonames{i}) = tmp(2:end,:,:);
    else
        tsdata.endodata.(var_info.endonames{i}) = [];
    end
end

for i = 1:numel(var_info.gnvnames)
    tmp = tsdata.gvdata.(var_info.gnvnames{i});
    trdata.gvdata.(var_info.gnvnames{i}) = ...
        [trdata.gvdata.(var_info.gnvnames{i}); tmp(1,:)];

    if size(tmp,1)>1
        tsdata.gvdata.(var_info.gnvnames{i}) = tmp(2:end,:);
    else
        tsdata.gvdata.(var_info.gnvnames{i}) = [];
    end
end

if nextyear_start
    tmp = tsdata.wdata;
    trdata.wdata = [trdata.wdata; tmp(1:cnum,:)];

    trdata.date_info.end = trdata.date_info.end+1;
    tsdata.date_info.start = tsdata.date_info.start+1;

    if size(tmp,1)>cnum
        tsdata.wdata = tmp(cnum+1:end,:);
    else
        tsdata.wdata = [];
    end
end

trdata.obs_train = trdata.obs_train+1;
tsdata.obs = tsdata.obs-1;

end