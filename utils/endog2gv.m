function gv = endog2gv(endog, endoglist, gvnames)
cnames = fieldnames(endog);
cnum = numel(cnames);
gvnum = numel(gvnames);

gv = [];
for v = 1:gvnum
    for n = 1:cnum
        idx = arrayfun(@(x) strcmp(gvnames{v}, endoglist.(cnames{n}){x}),...
            1:numel(endoglist.(cnames{n})));
        if ~isempty(endog.(cnames{n})(:,idx))
            gv.(gvnames{v}) = endog.(cnames{n})(:, idx);
        end
        continue;
    end
end

end