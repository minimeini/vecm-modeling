function siglev = serial_cor(res, p, k)
  % For computation of correlation matrices, center residuals by
  % subtraction of the mean  
    % Default value for k 
  nres = size(res, 1);
  m = size(res, 2);
  ntr = size(res, 3);
  resc  = res - repmat(mean(res), [nres, 1, 1]);
  
  if (nargin < 3) 
    k   = min(20, nres-1);
  end
  if (k <= p)                           % check if k is in valid range 
    error('Maximum lag of residual correlation matrices too small.'); 
  end
  if (k >= nres) 
    error('Maximum lag of residual correlation matrices too large.'); 
  end
  

  
  % Compute correlation matrix of the residuals
  % compute lag zero correlation matrix
  c0    = zeros(m, m);
  for itr=1:ntr
    c0  = c0 + resc(:,:,itr)'*resc(:,:,itr); % resc = npnt * nvar
  end
  d     = diag(c0);
  dd    = sqrt(d*d');
  c0    = c0./dd;

  % compute lag l correlation matrix
  cl    = zeros(m, m, k);
  for l=1:k
    for itr=1:ntr
      cl(:,:,l) = cl(:,:,l) ...
          + resc(1:nres-l, :, itr)'*resc(l+1:nres, :, itr);
    end
    cl(:,:,l) = cl(:,:,l)./dd;  
  end
  
  % Get "covariance matrix" in LMP statistic
  c0_inv= inv(c0);                      % inverse of lag 0 correlation matrix
  rr    = kron(c0_inv, c0_inv);         % "covariance matrix" in LMP statistic

  % Compute modified Li-McLeod portmanteau statistic
  lmp   = 0;                            % LMP statistic initialization
  x     = zeros(m*m,1);                 % correlation matrix arranged as vector
  for l=1:k
    x   = reshape(cl(:,:,l), m^2, 1);   % arrange cl as vector by stacking columns
    lmp = lmp + x'*rr*x;                % sum up LMP statistic
  end
  ntot  = nres*ntr;                     % total number of residual vectors
  lmp   = ntot*lmp + m^2*k*(k+1)/2/ntot;% add remaining term and scale
  dof_lmp = m^2*(k-p);                  % degrees of freedom for LMP statistic
      
  % Significance level with which hypothesis of uncorrelatedness is rejected
  siglev = 1 - gammainc(lmp/2, dof_lmp/2);
end