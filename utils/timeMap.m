function [widx, widx_full] = timeMap(idx_resp, maxlag, ...
    total_time, freq)

nweight = ceil(total_time / freq);
quartidx = mod(total_time, freq);
widx_full = reshape(kron(ones(1,freq)', (1:nweight)),1,[]);
widx_full = widx_full(1:total_time);
widx = widx_full((idx_resp-maxlag) : idx_resp);

end