function [vmap, cmap] = vcmap_globalvar(endoglist, cnames, vnames)
nvc = arrayfun(@(x) numel(endoglist.(cnames{x})), 1:numel(cnames));
cmap = arrayfun(@(x) x*ones(1, nvc(x)), 1:numel(nvc),'UniformOutput',0);
cmap = cell2mat(cmap);

endoglist_flat = flatten_struct(endoglist);
for i = 1:numel(vnames)
    vmap.(vnames{i})=arrayfun(@(x) strcmp(endoglist_flat{x},vnames{i}), ...
        1:numel(endoglist_flat));
end

end
