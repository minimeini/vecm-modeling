function flag = match_TWY(trdate_start, trdate_end, freq, nobs, wm_t)

flag = match_weightANDdata(nobs, freq, wm_t);
if flag == 1
    if nobs ~= date_dist(trdate_start, trdate_end, freq)
        flag = 0;
    end
end

end