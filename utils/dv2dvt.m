function dv_t = dv2dvt(dv, year_names, quart_assign)
% function dv_t = dv2dvt(dv, year_names, quart_assign)
% 
% **input**
% dv: dv.(<vnames>).(<cnames>) = npnts * 1
% year_names: year_names = fieldnames(wm_t);
% quart_assign: quart_assign = [npnts_in_y<1>, ..., nonts_in_y<nyear>]
%
% **output**
% dv_t: dv_t.(y<year>).(<vnames>).(<cnames>) = npnts_in_<year> * 1
%
% **constraint**
% sum(quart_assign) = rows(dv.(<vnames>).(<cnames>))
%
% **example**
% quart_assign = repmat(freq, 1, numel(year_names));
if numel(quart_assign) ~= numel(year_names)
    error('quart_assign and year_names do not match.');
end

nyear = numel(year_names);
vnames = fieldnames(dv);
vnum = numel(vnames);

for t=1:nyear
    if t == 1
        s = 1;
        e = quart_assign(1);
    else
        s = sum(quart_assign(1:t-1)) + 1;
        e = sum(quart_assign(1:t));
    end
    
    for i=1:vnum
        clist = fieldnames(dv.(vnames{i}));
        for n=1:length(clist)
            dv_t.(year_names{t}).(vnames{i}).(clist{n}) = ...
                dv.(vnames{i}).(clist{n})(s:e);
        end
    end
end

end