function fv = exog2fv(exog, exoglist, fvnames)

fv = [];
cnames = fieldnames(exog);
cnum = numel(cnames);
fvnum = numel(fvnames);

for n = 1:cnum
    for v = 1:fvnum
        idx = find(ismember(fvnames{v}, exoglist.(cnames{n})));
        fv.(fvnames{v}).(cnames{n}) = exog.(cnames{n})(:,idx);
    end
end

end