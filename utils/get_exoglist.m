function [exoglist, exognxlist, gxvnames] = ...
    get_exoglist(cnames, endonames, gvnames, gnvnames, endoflag, gvflag)
% > exoglist = fv + gv, struct, fieldnames = cnames, field = 1d char array
% > exognxlist = fv + gnv, struct, fieldnames = cnames, field = 1d char array

cnum = numel(cnames);

exoglist = struct();
if ~isempty(endonames)&&~isempty(gvnames)
    if size(endonames,1)~=1, endonames = endonames'; end
    if size(gvnames,1)~=1, gvnames = gvnames'; end
    for i = 1:cnum
        tmp = [endonames(endoflag(i,:)==1) gvnames(gvflag(i,:)==1)];
        exoglist.(cnames{i}) = tmp;
    end
end

exognxlist = struct();
if ~isempty(gnvnames)&&~isempty(endonames)&&~isempty(gvnames)
    if size(gnvnames,1)~=1, gnvnames = gnvnames'; end
    gnvflag = ismember(gvnames, gnvnames);
    for i = 1:cnum
        exognxlist.(cnames{i}) = [endonames(endoflag(i,:)==1) ...
            gvnames(gvflag(i,:)==1&&gnvflag==1)];
    end
end

if ~isempty(gnvnames)&&~isempty(gvnames)
    gxvnames = setdiff(gvnames, gnvnames, 'stable');
else
    gxvnames =[];
end
end