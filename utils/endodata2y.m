function y = endodata2y(endodata, gvdata, endoglist)
cnames = fieldnames(endoglist);
vnames = fieldnames(endodata);
gvnames = fieldnames(gvdata);
cnum = numel(cnames);

K = numel(flatten_struct(endoglist));
varidx_ext = [0 arrayfun(@(x) numel(endoglist.(cnames{x})), ...
    1:numel(cnames))];

y = [];
for n = 1:cnum
    s = sum(varidx_ext(1:n)) + 1;
    e = sum(varidx_ext(1:n+1));
    nv = varidx_ext(n+1);
    vtmp = endoglist.(cnames{n});
    
    for v = 1:nv
        if ismember(vtmp{v}, vnames)
            data_tmp = endodata.(vtmp{v});
            if size(data_tmp, 1) ~= cnum
                data_tmp = data_tmp';
            end
            y = [y; data_tmp(n,:)];
        elseif ismember(vtmp{v}, gvnames)
            data_tmp = gvdata.(vtmp{v});
            if size(data_tmp, 1) ~= 1
                data_tmp = data_tmp';
            end
            y = [y; data_tmp];
        end
    end
    
end

if size(y,1) ~= sum(varidx_ext)
    error('Unexpected error in function `endodata2y`.');
end

end