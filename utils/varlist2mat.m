function varmat = varlist2mat(varlist)
% varmat = K * ntimes

fn = fieldnames(varlist);
for n = 1:numel(fn)
    if n == 1
        varmat = varlist.(fn{n});
    else
        varmat = [varmat varlist.(fn{n})];
    end
end
varmat = varmat';
end