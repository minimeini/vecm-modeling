function [exog,exognx] = create_exogvars(foreign,gvdata,endoglist,...
exoglist,exognxlist,varargin)
% > foreign: struct, fieldnames = cnames, field = [ntime vnum ntrial]
% > gvdata/gv_full: struct, fieldnames = gvnames, field = [ntime ntrial] 
% numeric matrix, strong+weak global variables, exlucde foreign variables
% > endoglist = dv, struct, fieldnames = cnames, field = 1d char array
%
% > exog: struct of exogenous variable (fv+gnv+gxv), fieldnames = cnames, 
% field = [ntime gvnum ntrial]
% > exognx: struct of weakly exogenous variable (fv+gnv), 
% fieldnames = cnames, field = [ntime gnvnum ntrial]

cnames = fieldnames(endoglist); cnum = numel(cnames);
[ntime,~,ntrial] = size(foreign.(cnames{1}));

exog = struct(); exognx = struct();
for i = 1:cnum
    exog.(cnames{i}) = zeros(ntime,0,ntrial);
    exognx.(cnames{i}) = zeros(ntime,0,ntrial);
    exlist = exoglist.(cnames{i}); nex = numel(exlist);
    for j = 1:nex
        fvORgv = ismember(endoglist.(cnames{i}),exlist{j});
        % ==1,fv; ==0,gv
        if any(fvORgv==1) % fv
            fvloc = find(fvORgv==1);
            tmp = foreign.(cnames{i})(:,fvloc,:);
            
            exog.(cnames{i}) = cat(2,exog.(cnames{i}),tmp);
            exognx.(cnames{i}) = cat(2,exognx.(cnames{i}),tmp);
        else % gv
            tmp = gvdata.(exlist{j});
            tmp = permute(tmp, [1 3 2]);
            if size(tmp,1)>ntime, tmp=tmp(1:ntime,:,:); end
            
            exog.(cnames{i}) = cat(2,exog.(cnames{i}),tmp);
            if ismember(exlist{j},exognxlist.(cnames{i})) % gnv
                exognx.(cnames{i}) = cat(2,exognx.(cnames{i}),tmp);
            end
        end
    end
end


end