function [lb_restr] = ExF_lb_restr_new(cnames_x, xnames,outdir) 

%**************************************************************************
% PURPOSE: It allows the user to input lower bound restrictions for the
%          computation of GVAR ex-ante forecasts. 
%--------------------------------------------------------------------------
% L. Vanessa Smith, Cambridge, January 2014
% lvs21@cam.ac.uk
%**************************************************************************

k=length(cnames_x);

lb_restr = NaN(k,1);


lb_restr_temp  = xlsread([outdir 'ExF_lb_restr.xls'],'restrictions',['C4:C' num2str(3+k)]);

if length(lb_restr_temp)~=k 
    
    for start_ind = 1: k

        temp  = xlsread([outdir 'ExF_lb_restr.xls'],'restrictions',['C' num2str(3+start_ind)]);

        if ~isempty(temp)
            break;
        end
    end 

    end_ind = k;
    while end_ind >start_ind 

        temp  = xlsread([outdir 'ExF_lb_restr.xls'],'restrictions',['C' num2str(3+end_ind)]);

        if ~isempty(temp)
            break;
        end
        end_ind=end_ind-1;
    end

    if start_ind<=end_ind

        lb_restr_temp  = xlsread([outdir 'ExF_lb_restr.xls'],'restrictions',['C' num2str(3+start_ind) ':C' num2str(3+end_ind)]);
 
        lb_restr(start_ind:end_ind,1) = lb_restr_temp;

    end

elseif length(lb_restr_temp)==k 

 lb_restr = lb_restr_temp;
 
end 
