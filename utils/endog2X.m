function X = endog2X(endog,cnames)
% create X: use endog, yearnames and freq
% Step 1. concate endog by var
% X: [ntime nvar ntrial]
cnum = numel(cnames);
[ntime,~,ntrial] = size(endog.(cnames{1}));
X = zeros(ntime,0,ntrial);

for i = 1:cnum, X = cat(2,X,endog.(cnames{i})); end
X = permute(X,[2 1 3]);
end