function [] = outputVars(data, date, varnames, connames, savepath, save_prefix)
% data = Ncountry * Ntimes * Nvar
% varnames = 1 * Nvar
if nargin < 6
    save_prefix = 'data';
end

if size(data, 3) ~= numel(varnames)
    error('Unmatched variable numbers.\n');
end

checkPath(savepath);
for i = 1:numel(varnames)
   temp = [date num2cell(squeeze(data(:,:,i))')];
   temp = cell2table(temp, 'VariableNames', ['date' connames]);
   writetable(temp, fullfile(savepath, ...
       [save_prefix '_' varnames{i} '.csv']), ...
       'Delimiter', ',');
end

end