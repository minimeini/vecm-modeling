function trdate_end_n = trdate_end_ahead(trdate_end, freq, nAhead)
if freq == 4
    prefix = 'Q';
    year_num = str2double(trdate_end(1:end-2));
    term_num = str2double(trdate_end(end));
end

step_len = 1 / freq;
date_num = year_num + (term_num-1) * step_len;

date_num_n = date_num + nAhead * step_len;
year_num_n = floor(date_num_n);
term_num_n = (date_num_n - year_num_n) * freq + 1;
trdate_end_n = [num2str(year_num_n) prefix num2str(term_num_n)];
end