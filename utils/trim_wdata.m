function wdata_oneyear = trim_wdata(wdata,idx,freq)
estyr_testidx = ceil(idx/freq); 
cnum = size(wdata,2);

if ~isempty(wdata)
    wdata_oneyear = ...
        wdata(((estyr_testidx-1)*cnum+1):(estyr_testidx*cnum), :);
else
    wdata_oneyear = [];
end
end