function [invH, invErr] = invMatrix(H, method)
if strcmp(method, 'SVD')
    [U,S,V] = svd(H);
    if cond(S) > 100
        error('CALCULATION ERROR: singular matrix.');
    end
    invH = V * inv(S) * U';
elseif strcmp(method, 'QR')
    [Q,R] = qr(H);
    if cond(R) > 100
        error('CALCULATION ERROR: singular matrix.');
    end
    invH = R \ Q';
elseif strcmp(method, 'default')
    if cond(H) > 100
        error('CALCULATION ERROR: singular matrix.');
    end
    invH = inv(H);
elseif strcmp(method, 'pseudo')
    invH = pinv(H);
elseif strcmp(method, 'auto')
    invErrs = zeros(1, 4);
    
    [U,S,V] = svd(H);
    invH = V * inv(S) * U';
    invErrs(1) = norm(H\invH-diag(diag(ones(size(H,1)))),2);
    
    [Q,R] = qr(H);
    invH = R \ Q';
    invErrs(2) = norm(H\invH-diag(diag(ones(size(H,1)))),2);
    
    invH = inv(H);
    invErrs(3) = norm(H\invH-diag(diag(ones(size(H,1)))),2);
    
    invH = pinv(H);
    invErrs(4) = norm(H\invH-diag(diag(ones(size(H,1)))),2);
end

invErr = norm(H\invH-diag(diag(ones(size(H,1)))),2);
end