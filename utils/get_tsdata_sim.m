function [endovar,foreignvar,tsdata_sim] = get_tsdata_sim(var_info,...
    tsdata,varargin)
nargs = -nargin('get_tsdata_sim') - 1;
simdata = []; % nvar * ntime * ntrial
update_gv = 1; % logical
if nargin >= nargs + 1, simdata = varargin{1}; end
if nargin >= nargs + 2, update_gv = varargin{2}; end

%%
if ismember('endodata_test',fieldnames(tsdata))
    tmp = tsdata; tsdata = struct();
    tsdata.endodata = tmp.endodata_test;
    tsdata.gvdata = tmp.gvdata_test;
    tsdata.wdata = tmp.wdata_test;
    tsdata.date_info = tmp.date_info_test;
    tsdata.obs = tmp.obs_test;
    clear tmp
end

[endog,endoglist,exog,exoglist,exognx,exognxlist,wm_t,foreign,ytest] = ...
    gen_params(var_info.cnames,tsdata.date_info,var_info.endonames,...
    var_info.gvnames,var_info.gnvnames,var_info.endoflag,var_info.gvflag,...
    tsdata.endodata,tsdata.gvdata,tsdata.wdata);

if isempty(simdata), simdata = ytest; end

ntrial = size(simdata,3);

Klist = arrayfun(@(x) numel(endoglist.(var_info.cnames{x})),...
    1:numel(var_info.cnames));
K = sum(Klist); Klist_ext = [0 Klist];
Ktype = [];
for i = 1:numel(var_info.cnames)
    Ktype = [Ktype endoglist.(var_info.cnames{i})];
end

%% > gen_params
tsdata_sim = tsdata;

tsdata_sim.endodata = struct();
for i = 1:numel(var_info.endonames)
    tmp = simdata(ismember(Ktype,var_info.endonames{i}),:,:);%ncty*ntime*ntrial
    tmp = permute(tmp,[2 1 3]);%ntime*ncty*ntrial
    tsdata_sim.endodata.(var_info.endonames{i}) = tmp;
end

if update_gv == 1
    tsdata_sim.gvdata = struct();
    for i = 1:numel(var_info.gnvnames)
        tmp = simdata(ismember(Ktype,var_info.gnvnames{i}),:,:);
        tmp = squeeze(tmp);
        if size(tmp,2)~=ntrial, tmp = tmp'; end
        tsdata_sim.gvdata.(var_info.gnvnames{i}) = tmp;
    end
end

[endog_sim,~,exog_sim,~,exognx_sim,~,~,foreign_sim,xsim] = ...
    gen_params(var_info.cnames,tsdata_sim.date_info,var_info.endonames,...
    var_info.gvnames,var_info.gnvnames,var_info.endoflag,var_info.gvflag,...
    tsdata_sim.endodata,tsdata_sim.gvdata,tsdata_sim.wdata);

foreigndata_sim = struct();
for i = 1:numel(var_info.endonames)
    foreigndata_sim.(var_info.endonames{i}) = zeros(var_info.fhorz,...
        numel(var_info.cnames),ntrial);
    for j = 1:numel(var_info.cnames)
        foreigndata_sim.(var_info.endonames{i})(:,j,:) = ...
            foreign_sim.(var_info.cnames{j})(:,i,:);
    end
end

endovar = tsdata_sim.endodata.logtexp;
foreignvar = foreigndata_sim.logGDP;

end