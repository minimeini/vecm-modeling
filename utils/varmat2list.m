function varlist = varmat2list(varmat, varspec_list)
% varspec_list = endoglist;
fn = fieldnames(varspec_list);
varnum = arrayfun(@(x) numel(varspec_list.(fn{x})), 1:numel(fn));

varlist = [];
for n = 1:numel(fn)
    if n == 1
        s = 1;
        e = varnum(n);
    else
        s = sum(varnum(1:n-1)) + 1;
        e = sum(varnum(1:n));
    end
    varlist.(fn{n}) = varmat(s:e, :)';
end

end