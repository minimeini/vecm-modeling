function endoglist = get_endoglist(cnames, endonames, endoflag, ...
    gvnames, gvflag)
% > endoglist = dv, struct, fieldnames = cnames, field = 1d char array

cnum = numel(cnames);
for n = 1:cnum
    tmp = [endonames(endoflag(n,:)==1) gvnames(gvflag(n,:)==2)];
    endoglist.(cnames{n}) = tmp;
end
end