function [endodata, gvdata, wdata, date_info] = getData(endonames,gvnames, ...
    dataPath, dateInfoPath, weightPath, sposfirst, sposlast)

%% preparation

if ~isnumeric(sposfirst)||~isnumeric(sposlast)||sposfirst > sposlast
    fprintf('sposfirst:%d, sposlast: %d', sposfirst, sposlast);
    error('INPUT ERROR: sposfirst <= sposlast');
end
if ~iscell(endonames)||~iscell(gvnames)
    error('INPUT ERROR: variable names is a cell array.');
end

%%
for i = 1:numel(endonames)
    fpath = fullfile(dataPath, [endonames{i} '.csv']);
    if exist(fpath, 'file')~=0
        temp = dlmread(fpath, ',');
    else
        error(['File does not exist. filename: ' fpath]);
    end
    endodata.(endonames{i}) = temp(sposfirst:sposlast, :);
end

gvdata = [];
if numel(gvnames) ~= 0
    for i = 1:numel(gvnames)
        fpath = fullfile(dataPath, [gvnames{i} '.csv']);
        if exist(fpath, 'file')~=0
            temp = dlmread(fullfile(dataPath, [gvnames{i} '.csv']), ',');
        else
            error(['File does not exist. filename: ' fpath]);
        end
        if size(temp,1) == 1, temp = temp'; end
        if size(temp,2) > 1, temp = temp(:,1); end
        gvdata.(gvnames{i}) = temp(sposfirst:sposlast);
    end
end

%%
if ~isempty(dateInfoPath)
    if exist(dateInfoPath, 'file')~=0
        try
            [~, date] = xlsread(dateInfoPath);
        catch
            fileID = fopen(dateInfoPath,'r');
            date = textscan(fileID,'%s');
            fclose(fileID);
            date = date{1};
        end
    else
        error(['File does not exist. filename: ' dateInfoPath]);
    end
    
    date = date(2:end);
    if sposfirst>numel(date)||sposlast>numel(date)
        error('INPUT ERROR: split index exceeds the limit of date');
    end
    if contains(date{1}, 'Q') && numel(date{1})==6
        cutlen = 2;
    elseif contains(date{1}, 'M') && numel(date{1})==7
        cutlen = 3;
    elseif numel(date{1})==4
        cutlen = 0;
    else
        error(['INPUT ERROR: ''1995Q1'' for quarterly data,'...
            ' ''1995'' for yearly data,'...
            ' ''1995M01'' for monthly data (string)']);
    end
    
    date = date(sposfirst:sposlast);
    year_start = str2double(date{1}(1:end-cutlen));
    year_end = str2double(date{end}(1:end-cutlen));
    freq = sum(arrayfun(@(x) str2double(date{x}(1:end-cutlen)),...
        1:length(date)) == year_start);
    date_info = struct('start', year_start, 'end', year_end, 'freq', freq);
else
    date_info = struct();
end

%%
if ~isempty(weightPath)
    if exist(weightPath, 'file')~=0
        wdata = dlmread(weightPath);
    else
        error('File does not exist.');
    end
    year_start_idx = ceil(sposfirst/freq);
    wstart_idx = (year_start_idx - 1) * size(wdata,2) + 1;
    year_end_idx = ceil(sposlast/freq);
    wend_idx = year_end_idx * size(wdata,2);
    wdata = wdata(wstart_idx:wend_idx,:);
else
    wdata = [];
end
end