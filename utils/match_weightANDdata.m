function flag = match_weightANDdata(nobs, freq, wm_t)
flag = 1;
weightLabels = fieldnames(wm_t);
maxobs = date_dist([weightLabels{1}(2:end) 'Q1'], ...
    [weightLabels{end}(2:end) 'Q4'], freq);
minobs = date_dist([weightLabels{1}(2:end) 'Q4'], ...
    [weightLabels{end}(2:end) 'Q1'], freq);
if nobs > maxobs || nobs < minobs
    flag = 0;
end
end