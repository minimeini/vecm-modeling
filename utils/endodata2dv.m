function [dv, endodata] = endodata2dv(endodata, cnames, varargin)
% function [dv, endodata] = endodata2dv(endodata, cnames)
%
% ***** Required Input *****
% > endodata: struct, fieldnames = endonames, field = [ntime cnum ntrial] 
% numeric matrix

fm = fieldnames(endodata);

for i = 1:numel(fm)
    if size(endodata.(fm{i}), 2) ~= numel(cnames)
        error('cols(endodata.(fm{i})) ~= numel(cnames)');
    end
    
    for n = 1:numel(cnames)
        dv.(fm{i}).(cnames{n}) = squeeze(endodata.(fm{i})(:,n,:));
    end
end

end