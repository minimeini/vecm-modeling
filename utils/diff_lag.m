function outdata = diff_lag(indata, lag, dim)

size_ = size(indata);
total_dim = length(size_);
size_idx = 1:total_dim;

indata = permute(indata, [dim size_idx(size_idx ~= dim)]);
newsize_1 = size(indata);
indata = reshape(indata, size(indata, 1), []);

newsize_2 = size(indata);
newsize_2(1) = newsize_2(1) - lag;
outdata = zeros(newsize_2);
for t = 1:newsize_2(1)
    outdata(t, :) = indata(t+lag,:) - indata(t,:);
end

outdata = reshape(outdata, [newsize_2(1) newsize_1(2:end)]);
newsize_3 = zeros(1, total_dim);
newsize_3(dim) = 1;
newsize_3(newsize_3 == 0) = 2:total_dim;
outdata = permute(outdata, newsize_3);

end