function endog_new = addnew_endog(endog, ynew, endoglist)
% y = K*1
% endog = struct(country: [ntimes * nendog])
endog_mat = varlist2mat(endog);
endog_mat = [endog_mat ynew];
endog_new = varmat2list(endog_mat, endoglist);

end