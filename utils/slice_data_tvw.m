function [endog_slice, exog_slice] = slice_data_tvw(endog, exog, slice_idx)

if ~isequal(fieldnames(endog), fieldnames(exog))
    error('Illegal Input.');
end
cnames = fieldnames(endog);
cnum = numel(cnames);

for n = 1:cnum
    endog_slice.(cnames{n}) = endog.(cnames{n})(slice_idx, :);
    exog_slice.(cnames{n}) = exog.(cnames{n})(slice_idx, :);
end

end