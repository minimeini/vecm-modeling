function endog = dv2endog(dv, gv, endoglist, varargin)
% function endog = dv2endog(dv, gv, endoglist)
%
% ***** Required Input *****
% > dv: struct, fieldnames = endonames/dvnames, 
% field = struct(fieldnames=cnames, field=ntime*1)
% > gv: struct, fieldnames = gvnames
% > endoglist: struct, fieldnames = cnames, field = endonames of a specific
% country
%
% ***** Output *****
% > endog: struct, fieldnames = cnames, field = [ntime dvnum]

dvnames = fieldnames(dv);
dvnum = numel(dvnames);
gvnames = fieldnames(gv);
gvnum = numel(gvnames);
cnames = fieldnames(dv.(dvnames{1}));
cnum = numel(cnames);

nobs = size(dv.(dvnames{1}).(cnames{1}),1);
ntrials = size(dv.(dvnames{1}).(cnames{1}),2);

for n = 1:cnum
    endog.(cnames{n}) = zeros(nobs, ...
        numel(endoglist.(cnames{n})), ntrials);
    for i = 1:numel(endoglist.(cnames{n}))
        dvORgv = sum(arrayfun(@(x) ...
            strcmp(dvnames{x}, endoglist.(cnames{n})(i)), 1:dvnum)) > 0;
        if dvORgv==1
            tmp = dv.(endoglist.(cnames{n}){i}).(cnames{n});   
        else
            tmp = gv.(endoglist.(cnames{n}){i});
            if size(tmp,2)==1
                tmp = repmat(tmp,1,ntrials);
            end
        end
        tmp = permute(tmp,[1 3 2]);
        endog.(cnames{n})(:,i,:) = tmp;
    end
end

end