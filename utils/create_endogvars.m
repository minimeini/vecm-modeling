function [endog,X] = create_endogvars(endodata,gvdata,endoglist,cnames)
% > endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
% > endodata/simdata: struct, fieldnames = endonames, 
% field = [ntime cnum ntrial] numeric matrix
% > endog: struct, fieldnames = cnames, field = [ntime dvnum ntrial]
% > endoglist = dv, struct, fieldnames = cnames, field = 1d char array
% > X: [ntime nvar ntrial]


cnum = numel(cnames);
endonames = fieldnames(endodata);

ntime = size(endodata.(endonames{1}),1);
ntrial = size(endodata.(endonames{1}),3);

endog = struct();
for i = 1:cnum
    tmplist = endoglist.(cnames{i}); nvar = numel(tmplist);
    endog.(cnames{i}) = zeros(ntime,0,ntrial);
    for j = 1:nvar
        var = tmplist{j}; dvORgv = ismember(endonames,var);
        if any(dvORgv==1) % dv
            endog.(cnames{i}) = cat(2,endog.(cnames{i}),...
                endodata.(var)(:,i,:));
        else % gv
            tmp = gvdata.(var);
            tmp = permute(tmp, [1 3 2]);
            endog.(cnames{i}) = cat(2,endog.(cnames{i}),tmp);
        end
    end
end

%%
% create X: use endog, yearnames and freq
% Step 1. concate endog by var
% X: [ntime nvar ntrial]
X = endog2X(endog,cnames);

end