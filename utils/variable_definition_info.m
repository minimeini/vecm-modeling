function [] = variable_definition_info()

% > cnames: 1d char array, country names
% > date_info: struct, fieldnames = {'start', 'end', 'freq'}
%
% > endonames: 1d char array, names of endogenous variable and foreign
% variable
% > endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
% > endodata/simdata: struct, fieldnames = endonames, 
% field = [ntime cnum ntrial] numeric matrix
% > endog: struct, fieldnames = cnames, field = [ntime dvnum ntrial]
% > endoglist = dv, struct, fieldnames = cnames, field = 1d char array
% > endovar: 2d/3d numeric matrix, [ntime cnum ntrial]
%
% > dv: struct, fieldnames = endonames, 
% field = struct(fieldnames=cnames, field=ntime*ntrial)
% > dv_t: struct, fieldnames = 'y[year]', 
%         field = struct(fieldnames=endonames, field=
%                        struct(fieldnames=cnames, field=freq*ntrial))
%
% > gvnames: 1d char array, names of global (strong+weak) variable
% > gvflag: [cnum gvnum] numeirc matrix, 0=not included, 1=included as
% exogenous, 2=included as endogenous
% > gvdata/gv_full: struct, fieldnames = gvnames, field = [ntime ntrial] 
% numeric matrix, strong+weak global variables, exlucde foreign variables
% > exog: struct of exogenous variable (fv+gnv+gxv), fieldnames = cnames, 
% field = [ntime gvnum ntrial]
% > exoglist = fv+gnv+gxv, struct, fieldnames = cnames, field = 1d char array
%
% > foreign: struct, fieldnames = cnames, field = [ntime vnum ntrial]
% > foreigndata: struct, fieldnames = endonames, 
% field = [ntime cnum ntrial] numeric matrix
% > foreign: 2d/3d numeric matrix, [ntime cnum ntrial]
%
% > gnvnames: 1d char array, names of global (weak) variable
% > exognx: struct of weakly exogenous variable (fv+gnv), 
% fieldnames = cnames, field = [ntime gnvnum ntrial]
% > exognxlist = fv + gnv, struct, fieldnames = cnames, field = 1d char
% array
%
% > wdata: [(nyear*cnum) cnum] 2d numeric matrix
% > wm_t: struct, fieldnames = yearnames'y[year]', 
% field = [dv+fv all(dv+fv)] numeric matrix, ref: def_weight_tv
% > W_tv/linkmat_z: struct, fieldnames = cnames, field = [dv+fv all(dv+fv) yearnames]

% > X: [nvar ntime ntrial] numeric matrix
% > Xt: struct, fieldnames = yearnames'y[year]', 
% field = [nvar freq ntrials] numeric matrix
%
% > rank: struct, rank of country-specific vecm, fieldnames = cnames, field
% = numeric number
% > varxlag: struct, lag of country-specific var, fieldnames = cnames,
% field = [1 2] numeric array
%
% > simdata: struct, fieldnames=cnames, field=[ntime nvar]

end