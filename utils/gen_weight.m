function wmatrices_t = gen_weight(wmat, w_inityear, w_lastyear)
nyears = w_lastyear - w_inityear + 1;
yearseq = w_inityear:w_lastyear;
ylabelseq_estimation = arrayfun(@(x) ['y' num2str(x)], yearseq, ...
    'UniformOutput', 0)';          

cnum = size(wmat, 2); % wmat: (nyear*cnum) * cnum
wmat = reshape(wmat',cnum,cnum,nyears); 
% wmat = [cnum * cnum * nyear]

% prev: sum(wmat(:,:,i),1) != 1
% now: sum(wmat(:,:,i),1) == 1
for i=1:nyears
    wmat(:,:,i) = wmat(:,:,i)';
end        

for t=1:nyears
    wmatrices_t.(ylabelseq_estimation{t}) = ...
        wmat(:,:,t);
end
end