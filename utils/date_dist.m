function nobs = date_dist(trdate_start, trdate_end, freq)

start_num = date_str2num(trdate_start, freq);
end_num = date_str2num(trdate_end, freq);
nobs = (end_num - start_num) * freq + 1;
end

function date_num = date_str2num(date_str, freq)

if isempty(freq)
    error('freq is empty.');
end

if isempty(date_str)
    error('date_str is empty');
end

if freq == 4
    year_num = str2double(date_str(1:end-2));
    term_num = str2double(date_str(end));
end

step_len = 1 / freq;
date_num = year_num + (term_num-1) * step_len;
end