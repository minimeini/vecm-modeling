function arr_flat = flatten_struct(astruct)
astruct = struct2cell(astruct);
arr_flat = [];
for i = 1:numel(astruct)
    arr_flat = [arr_flat reshape(astruct{i}, 1, [])];
end
end