function [G0, A0, A1, G, J0, J, stat] = stack_params(W_tv, gvlist, ...
    a0, a1, Theta, Lambda0, Lambda, Gamma0, Gamma, varargin)
%% Parsing and Prep
default_nargs = 9;
mod_type = 'ex';
if nargin>=default_nargs+1
    if ~ischar(varargin{1})
        error('INPUT ERROR: mod_type = {''ex'', ''du''}');
    end
    mod_type = varargin{1};
end

cnames = fieldnames(a0);
cnum = numel(cnames);

Klist = arrayfun(@(x) size(a0.(cnames{x}), 1), 1:cnum);
K = sum(Klist);

if ~isempty(Lambda)
    maxlag = max([arrayfun(@(x) (size(Theta.(cnames{x}), 3)), 1:cnum) ...
        arrayfun(@(x) (size(Lambda.(cnames{x}), 3)), 1:cnum)]);
elseif ~isempty(Gamma)
    maxlag = max([arrayfun(@(x) (size(Theta.(cnames{x}), 3)), 1:cnum) ...
        arrayfun(@(x) (size(Gamma.(cnames{x}), 3)), 1:cnum)]);
else
    maxlag = max(arrayfun(@(x) (size(Theta.(cnames{x}), 3)), 1:cnum));
end

%%
if ~isempty(W_tv)
    nyear = size(W_tv.(cnames{1}), 3);
    G0 = zeros(K, K, nyear);
    G = zeros(K, K, maxlag, nyear);
    stat = zeros(cnum, 2, nyear);
    for i = 1:nyear
        W_slice = struct();
        for j = 1:cnum
            if sum(reshape(W_tv.(cnames{j})(:,:,i),1,[])) == 0
                error('INPUT ERROR: all zero weights');
            end
        
            W_slice.(cnames{j}) = W_tv.(cnames{j})(:,:,i); % linkmat_z
            if isempty(W_slice.(cnames{j}))
                error('CALCULATION ERROR: empty weights');
            end
            if sum(reshape(W_slice.(cnames{j}),1,[])) == 0
                error('CALCULATION ERROR: all zero weights.');
            end
        end
    
        [G0_tmp, G_tmp, J0, J, A0, A1, stat(:,:,i)] = ...
            gen_stackpar(W_slice, gvlist, a0, a1, Theta, ...
            Lambda0, Lambda, Gamma0, Gamma, mod_type);
    
        G0(:,:,i) = G0_tmp;
        G(:,:,:,i) = G_tmp;
    end
else
    [G0, G, J0, J, A0, A1, stat] = ...
        gen_stackpar([], gvlist, a0, a1, Theta, ...
        Lambda0, Lambda, Gamma0, Gamma, mod_type);
    
end

end