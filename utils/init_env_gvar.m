function [wd, info_sp, info_tr, trdata, tsdata] = ...
    init_env_gvar(varargin)
% function [status, info_sp, info_tr, trdata, tsdata] = ...
%     init_env_gvar(varargin)
%
% ***** Optional Input *****
% - init_path: logical, default=1, if initialize the path
% - load_info: logical, default=0, if load the splitting into and training
% info
% - load_train: logical, default=0, if load the training data
% - load_test: logical, default=0, if load the testing data
%
% ***** Output *****
% - status: 0=failed to evaluate, 1=successfully run
% - info_sp: struct, splitting info
%   - cnames            - gvflag            - endoflag           
%   - gvnames           - endonames         - test_pos
%   - fhorz             - train_pos         - gnvnames
%   - endoglist         - exoglist          - exognxlist
% - info_tr: struct, training info
%   - Gamma             - eta               - Gamma0
%   - exog              - Lambda            - exoglist
%   - Lambda0           - exognxlist        - Theta
%   - logl              - a0                - rank
%   - a1                - varxlag           - aic
%   - we_test           - endog             - wm_t
%   - endoglist         - y
% - trdata: struct, training data
%   - date_info         - endodata          - gvdata
%   - obs_train         - wdata
% - tsdata: struct, testing data
%   - date_info_test    - endodata_test     - gvdata_test
%   - obs_test          - wdata_test


nargs = -nargin('init_env_gvar') - 1;
init_path = 1; load_info = 0; load_train = 0; load_test = 0;
if nargin>=nargs+1, init_path=varargin{1}; end
if nargin>=nargs+2, load_info=varargin{2}; end
if nargin>=nargs+3, load_train=varargin{3}; end
if nargin>=nargs+4, load_test=varargin{4}; end

info_sp = []; info_tr = []; trdata = []; tsdata = [];

wd = struct();
if init_path==1
    wd.main = 'C:\Users\meini\Dropbox\vecmModeling';
    wd.data = fullfile(wd.main, 'data');
    wd.vis = fullfile(wd.main, 'figure');
    wd.ana = fullfile(wd.main, 'analysis');
end

if load_info==1
    info_sp = load(fullfile(wd.data, 'parse', 'split_info.mat'));
    info_tr = load(fullfile(wd.data, 'train', 'tr_plain_info.mat'));
    
    info_sp.endoglist = get_endoglist(info_sp.cnames,info_sp.endonames,...
        info_sp.endoflag,info_sp.gvnames,info_sp.gvflag);
    [info_sp.exoglist,info_sp.exognxlist] = get_exoglist(info_sp.cnames,...
        info_sp.endonames,info_sp.gvnames,info_sp.gnvnames,...
        info_sp.endoflag,info_sp.gvflag);
end
if load_train==1
    trdata = load(fullfile(wd.data, 'parse', 'train_data.mat'));
end
if load_test==1
    tsdata = load(fullfile(wd.data, 'parse', 'test_data.mat'));
    tsdata = normalize_tsdata(tsdata);
end

end

%% normalize tsdata
function tsdata_norm = normalize_tsdata(tsdata)
fm = fieldnames(tsdata);
tsdata_norm = struct();
for i = 1:numel(fm)
    if contains(fm{i}, '_test')
        tsdata_norm.(fm{i}(1:end-5)) = tsdata.(fm{i});
    else
        tsdata_norm.(fm{i}) = tsdata.(fm{i});
    end
end
end
