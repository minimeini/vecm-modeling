function [dv, dv_t] = endog2dv(endog, vnames, endoglist, year_names, freq)

vnum = numel(vnames);
cnames = fieldnames(endog);
cnum = numel(cnames);

dv = [];
for i = 1:vnum
    for n = 1:cnum
        idx = arrayfun(@(x) ...
            strcmp(endoglist.(cnames{n})(x), vnames{i}), ...
            1:numel(endoglist.(cnames{n})));
        dv.(vnames{i}).(cnames{n}) = endog.(cnames{n})(:,idx);
    end
end

nobs = rows(endog.(cnames{1}));
quart_assign = repmat(freq, 1, floor(nobs/freq));
if mod(nobs,freq) > 0
    quart_assign = [quart_assign mod(nobs, freq)];
end

dv_t = dv2dvt(dv, year_names, quart_assign);

end