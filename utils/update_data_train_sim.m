function trdata_sim = update_data_train_sim(var_info,trdata,simdata)
trdata_sim = trdata;

for i = 1:numel(var_info.endonames)
    tmp = zeros(trdata.obs_train,numel(var_info.cnames));
    for j = 1:numel(var_info.cnames)
        loc_endo = ismember(var_info.endoglist.(var_info.cnames{j}),...
            var_info.endonames{i});
        tmp(:,j) = simdata.(var_info.cnames{j})(:,loc_endo);
    end
    trdata_sim.endodata.(var_info.endonames{i}) = tmp;
end

for i = 1:numel(var_info.gnvnames)
    tmp = zeros(trdata.obs_train,1);
    for j = 1:numel(var_info.cnames)
        loc_endo = ismember(var_info.endoglist.(var_info.cnames{j}),...
            var_info.gnvnames{i});
        if any(loc_endo==1)
            tmp = simdata.(var_info.cnames{j})(:,loc_endo);
        end
    end
    trdata_sim.gvdata.(var_info.gnvnames{i}) = tmp;    
end

end