function [trdata_hs,wdata_forc] = trim_data_train_orig(var_info,...
    trdata,h_step,varargin)

nargs = -nargin('trim_data_train_orig') - 1;
tsdata = [];
if nargin>=nargs+1, tsdata = varargin{1}; end

%%
trdata_hs = trdata;

for i = 1:numel(var_info.endonames)
    trdata_hs.endodata.(var_info.endonames{i}) = ...
        trdata.endodata.(var_info.endonames{i})(1:end-h_step+1,:,:);
end

for i = 1:numel(var_info.gnvnames)
    trdata_hs.gvdata.(var_info.gnvnames{i}) = ...
        trdata.gvdata.(var_info.gnvnames{i})(1:end-h_step+1,:);    
end

trdata_hs.obs_train = size(trdata_hs.endodata.(var_info.endonames{1}),1);
%%
yrsumnew = ceil(trdata_hs.obs_train/4); 
yrsumold = ceil(trdata.obs_train/4);
yrdiff = yrsumold - yrsumnew;
cnum = size(trdata_hs.wdata,2);

del_wdata = trdata_hs.wdata(end-cnum*yrdiff+1:end,:);
trdata_hs.wdata = trdata_hs.wdata(1:yrsumnew*cnum,:);

if ~isempty(tsdata)
    wdata_forc = [del_wdata; tsdata.wdata];
end

trdata_hs.date_info.end = trdata_hs.date_info.end - yrdiff;

end