function [endog, endodata] = y2endog(y, cnames, gvnames, endoglist)
% function [endog, endodata] = y2endog(y, cnames, gvnames, endoglist)
%
% Required Input
% - y: numeric matrix, nendog * nobs
% - cnames: cell array, list of country names
% - gvnames: cell array, list of weakly exogenous and strong exogenous,
% foreign variables excluded
% - endoglist: structure with country-specific fields, names of endogenous
% variables in each country-specific model
%
% Output
% - endog: structure with country-specific fields, nobs * nendog, contains
% domestic variables and weakly exogenous that is treated as endogenous in
% this specific model
% - endodata: structure with domestic-variable-specific fields, nobs *
% ncty, contains only domestic variables as fields

%%
flat_list = flatten_struct(endoglist);
K = numel(flat_list);
cnum = numel(cnames);
vnames = unique(flat_list);

%% isdv: identifier for domestic variables
isdv = ones(1, numel(vnames));
if ~isempty(gvnames)
    for i = 1:numel(vnames)
        isdv(i) = min(arrayfun(@(x) ~strcmp(vnames{i}, gvnames{x}), ...
            1:numel(gvnames))) > 0;
    end
end
dvnames = vnames(isdv == 1);
dvnum = numel(dvnames);

%% endodata: only contains domestic variables
for i = 1:dvnum
    idx_tmp = arrayfun(@(x) strcmp(flat_list{x}, dvnames{i}), 1:K);
    endodata.(dvnames{i}) = y(idx_tmp==1, :)';
end

%% endog: contains domestic variables and wealy exogenous which are 
% endogeous in some country-specific models
nendog_byc = arrayfun(@(x) numel(endoglist.(cnames{x})), 1:cnum);
nendog_byc = [0 nendog_byc];
for i = 1:cnum
    s = sum(nendog_byc(1:i)) + 1;
    e = sum(nendog_byc(1:(i+1)));
    endog.(cnames{i}) = y(s:e, :)';
end

end