function resid_rand = simulate_residuals(resid, fhorz, varargin)
% resid: 2d numeric array, residuals of model, [nvar ntime]
nargs = -nargin('simulate_residuals') - 1;
trEIG = 1e-05;
sim_type = 'nonparametric';

if nargin >= nargs+1, trEIG = varargin{1}; end
if nargin >= nargs+2, sim_type = varargin{2}; end

varcov = (resid * resid') / size(resid,2);
[U,S,~] = svd(varcov);
eigval = diag(S);
tridx = find(eigval>=trEIG,1,'last');

Utilde = U(:,1:tridx);
Stilde = diag(eigval(1:tridx));

resid_norm = (inv(sqrt(Stilde))*Utilde'*resid) / sqrt(size(resid,2));
res_bnd = max(abs(max(resid_norm(:))), abs(min(resid_norm(:))));

if strcmp(sim_type, 'nonparametric')
    rng('shuffle');
    select = randi(tridx, 1, fhorz);
    rand_norm = resid_norm(:,select);
    
elseif strcmp(sim_type, 'parametric')
    sigma = resid_norm * resid_norm';
    mu = mean(resid_norm, 2);
    rand_norm = zeros(tridx, fhorz);
    
    for ridx = 1:fhorz
        success = 0;
        trycnt = 200;
        
        while ~success && trycnt > 0
            trycnt = trycnt - 1;
            rnorm_tmp = mvnrnd(mu, sigma, 1);
            if max(abs(rnorm_tmp)) <= res_bnd
                success = 1;
            end
        end
        rand_norm(:, ridx) = rnorm_tmp;
    end
end

resid_rand = (Utilde * diag(sqrt(diag(Stilde))) * rand_norm) * ...
    sqrt(size(resid,2));

end