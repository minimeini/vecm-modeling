function [trace_crit95_c4, trace_crit95_c3, trace_crit95_c2, maxeig_crit95_c4, maxeig_crit95_c3, maxeig_crit95_c2] = critical_values(filepath)


trace_crit95_c4_tmp = xlsread(filepath,'Trace_case4','basic');
trace_crit95_c4 = trace_crit95_c4_tmp(2:end,2:end);

maxeig_crit95_c4_tmp = xlsread(filepath,'Maximum Eigenvalue_case4','basic');
maxeig_crit95_c4 = maxeig_crit95_c4_tmp(2:end,2:end);

trace_crit95_c3_tmp = xlsread(filepath,'Trace_case3','basic');
trace_crit95_c3 = trace_crit95_c3_tmp(2:end,2:end);

maxeig_crit95_c3_tmp = xlsread(filepath,'Maximum Eigenvalue_case3','basic');
maxeig_crit95_c3 = maxeig_crit95_c3_tmp(2:end,2:end);

trace_crit95_c2_tmp = xlsread(filepath,'Trace_case2','basic');
trace_crit95_c2 = trace_crit95_c2_tmp(2:end,2:end);

maxeig_crit95_c2_tmp = xlsread(filepath,'Maximum Eigenvalue_case2','basic');
maxeig_crit95_c2 = maxeig_crit95_c2_tmp(2:end,2:end);

end