function [flag, nobs_y, nobs_en, nobs_ex] = match_nobs(y, endog, exog)
flag = 1;
nobs_y = cols(y);

fm = fieldnames(endog);
nobs_en = rows(endog.(fm{1}));
flag_en = 1;
for i = 2:numel(fm)
    if nobs_en ~= rows(endog.(fm{i}))
        flag_en = 0;
        break
    end
end

fm = fieldnames(exog);
nobs_ex = rows(exog.(fm{1}));
flag_ex = 1;
for i = 2:numel(fm)
    if nobs_ex ~= rows(exog.(fm{i}))
        flag_ex = 0;
        break
    end
end

if flag_en == 0 || flag_ex == 0
    flag = 0;
end

if flag == 1
    flag = (nobs_y == nobs_en) && (nobs_y == nobs_ex);
end

end