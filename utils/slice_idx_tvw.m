function [widx_slice, slice_idx] = slice_idx_tvw(freq, varargin)

% function slice_idx = slice_idx_tvw(freq, varargin)
% 
% addRequired(p, 'freq', @isnumeric);
% addOptional(p, 'widx_full', [], @(x) isempty(x) || isnumeric(x));
% 
% addParameter(p, 'year_start', [], @(x) isnumeric(x)||isempty(x));
% addParameter(p, 'quart_start', [], @(x) isnumeric(x)||isempty(x));
% addParameter(p, 'year_end', [], @(x) isnumeric(x)||isempty(x));
% addParameter(p, 'quart_end', [], @(x) isnumeric(x)||isempty(x));

% p.Results.freq = 4;
% p.Results.widx_full = [];
% p.Results.year_start = year_start;
% p.Results.year_end = year_end;
% p.Results.quart_start = quart_start;
% p.Results.quart_end = quart_end;

p = inputParser;
addRequired(p, 'freq', @isnumeric);
addRequired(p, 'widx_full', @(x) isempty(x) || isnumeric(x));

addParameter(p, 'year_start', [], @(x) isnumeric(x)||isempty(x));
addParameter(p, 'quart_start', [], @(x) isnumeric(x)||isempty(x));
addParameter(p, 'year_end', [], @(x) isnumeric(x)||isempty(x));
addParameter(p, 'quart_end', [], @(x) isnumeric(x)||isempty(x));
parse(p, freq, varargin{:});

anyempty = isempty(p.Results.year_start) + ...
    isempty(p.Results.quart_start) + ...
    isempty(p.Results.year_end) + ...
    isempty(p.Results.quart_end);

if isempty(p.Results.widx_full) && anyempty > 0
    % no widx_full or date_info provided
    error('Insufficient inputs.');
    
elseif ~isempty(p.Results.widx_full) && anyempty == 0
    % both widx_full and date_info provided ==> slice
    widx_full = p.Results.widx_full;
    idx_start = max(find(widx_full==p.Results.year_start, ...
        p.Results.quart_start));
    idx_end = max(find(widx_full==p.Results.year_end, ...
        p.Results.quart_end));
    
    slice_idx = zeros(size(widx_full));
    slice_idx(idx_start:idx_end) = 1;
    
elseif ~isempty(p.Results.widx_full) && anyempty == 4
    % widx_full provided without date_info ==> no slice
    widx_full = p.Results.widx_full;
    slice_idx = ones(size(widx_full));

elseif isempty(p.Results.widx_full) && anyempty == 0
    % no widx_full ==> construct widx_full from year in date_info
    % and get slice_idx from quart in date_info
    widx_full = reshape(repmat(p.Results.year_start:p.Results.year_end, ...
        p.Results.freq, 1),1,[]);
    slice_idx = zeros(size(widx_full));
    
    idx_start = max(find(widx_full==p.Results.year_start, ...
        p.Results.quart_start));
    idx_end = max(find(widx_full==p.Results.year_end, ...
        p.Results.quart_end));
    slice_idx(idx_start:idx_end) = 1;
    
else
    error('Insufficient inputs.');
end

widx_slice = widx_full(slice_idx == 1);
end