function endodata = y2endodata(data, vdim, vnames, cnames, endoglist)

% function endodata = y2endodata(data, vdim, vnames, cnames, endoglist)
% data: size(data) = [K NT Ntrials]
% vdim: dimension where variables are placed

size_ = size(data);
ndim = length(size_);
pmt_ord = vdim;
if vdim > 1
    pmt_ord = [pmt_ord 1:vdim-1];
end
if vdim < ndim
    pmt_ord = [pmt_ord vdim+1:ndim];
end
data_tmp = permute(data, pmt_ord);
newsize_ = size(data_tmp);
data_tmp = reshape(data_tmp, newsize_(1), []);

vmap = vcmap_globalvar(endoglist, cnames, vnames);
for i = 1:numel(vnames)
    endodata.(vnames{i}) = reshape(data_tmp(vmap.(vnames{i}),:), ...
        [numel(cnames) newsize_(2:end)]);
end

end