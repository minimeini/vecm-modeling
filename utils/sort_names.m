function [sorted, ord] = sort_names(unsorted, varlist)
fm = fieldnames(varlist);
fmnum = numel(fm);
snum = numel(unsorted);
ord = zeros(fmnum, snum);
for i = 1:snum
    for j = 1:fmnum
        try
            ord(j, i) = find(ismember(varlist.(fm{j}), unsorted{i}));
        catch
            ord(j, i) = 0;
        end
    end
end

ord = sum(ord, 1);
[~, ord] = sort(ord, 'ascend');
sorted = unsorted(ord);

end