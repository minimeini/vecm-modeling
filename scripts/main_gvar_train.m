%% initialization
clear all
wd = 'C:\Users\meini\Dropbox\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';

wd_data = fullfile(wd, 'data');
wd_vis = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd_data);

%%
load(fullfile(wd_data, 'parse', 'split_info.mat'));
load(fullfile(wd_data, 'parse', 'train_data.mat'));

%%
estMdl = train_gvar(cnames, date_info, endonames, gvnames, gnvnames, ...
    endoflag, gvflag, endodata, gvdata, wdata);

%%
save(fullfile(wd_data, 'train', 'tr_plain_info.mat'), 'estMdl');

%%
yfit = y - eta;
xlswrite(fullfile(wd_data, 'train', 'tr_plain.xlsx'), yfit, 'forc');
xlswrite(fullfile(wd_data, 'train', 'tr_plain.xlsx'), y, 'real');
xlswrite(fullfile(wd_data, 'train', 'tr_plain.xlsx'), eta, 'eta');
