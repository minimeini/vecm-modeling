%% initialization
clear all
wd_code = 'C:\respository\vecm-modeling';
cd(wd_code);
[wd,var_info,mdl_info,trdata,tsdata] = init_env_gvar(1,1,1,1);

%%
nseg = 3; nparams = 1; nfuture = 100; fhorz=24;
cnum = numel(var_info.cnames);
simpath = fullfile(wd.data,'simulate');

update_params = 1; h_step = [4 8 12 16 20];

%% country-specific VAR(1) - parameter uncertainty + future uncertainty
% estMdl.Constant + estMdl.Trend*(i+1) + estMdl.AR{1} * x1;
var_lag = 1; 

for h = h_step
    for i = 1:nseg
        xsim = simulate_var(var_lag,var_info,trdata,tsdata,...
            nparams,nfuture,fhorz,h,update_params);
        save(fullfile(simpath,['simulate_var_roll' num2str(h) ...
            '_' num2str(i) '.mat']), 'xsim','-v7.3');
    end
end

%% country-specific VECM(1) - parameter uncertainty + future uncertainty
vecm_lag = 1;
[~,rank] = train_vecm(var_info,trdata,vecm_lag);

for h = h_step
    for i = 1:nseg
        xsim = simulate_vecm(rank,vecm_lag,var_info,trdata,tsdata,...
            nparams,nfuture,fhorz,h,update_params);
        save(fullfile(simpath,['simulate_vecm_roll' num2str(h) ...
            '_' num2str(i) '.mat']),'xsim','-v7.3');
    end
end
   
%% GVAR
for h = h_step
    for i = 1:nseg
        xsim = simulate_gvar(mdl_info.rank,mdl_info.varxlag,var_info,...
            trdata,tsdata,nparams,nfuture,var_info.fhorz,h);
        save(fullfile(simpath,['simulate_gvar_roll' num2str(h) ...
            '_' num2str(i) '.mat']),'xsim','-v7.3');
    end
end

% %% variable-specific AR(1)
% ar_lag = 1;
% estMdl = train_ar(var_info,trdata,ar_lag);
% for i = 1:nseg
%     xsim = simulate_ar(estMdl,ar_lag,var_info,trdata,tsdata,...
%         nparams,nfuture,fhorz);
%     save(fullfile(simpath,['simulate_ar_' num2str(i) '.mat']),...
%         'xsim','-v7.3');
% end
% 
% %% variable-specific optimal ARIMA
% [estMdl,eta,p,d,q] = train_arima(var_info,trdata);
% for i = 1:nseg
%     xsim = simulate_arima(estMdl,eta,p,d,q,var_info,trdata,tsdata,...
%         nparams,nfuture,fhorz);
%     save(fullfile(simpath,['simulate_arima_' num2str(i) '.mat']),...
%         'xsim','-v7.3');
% end