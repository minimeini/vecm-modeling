clear all
wd = 'C:\Users\meini\OneDrive\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';

wd_data = fullfile(wd, 'data');
wd_vis = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd_data);

%%
load(fullfile(wd_data, 'parse', 'split_info.mat'));
load(fullfile(wd_data, 'parse', 'train_data.mat'));
load(fullfile(wd_data, 'parse', 'test_data.mat'));

maxlag_p = 2;
maxlag_q = 2;
wscheme = 'aver';
criterion = 'aic';
psc = 4;
estcase_tmp = 4 * ones(1, obs_train);

cnames = cnames_info.short';
vnames = var_info.vnames';
gvnames = var_info.gvnames';
endoglist = varflag2varlist(var_flag.dvflag, var_flag.gvflag, ...
    vnames, gvnames, cnames);

yreal = endodata2y(endodata_test, gvdata_test, endoglist);

%% 
loadpath = fullfile(wd_data, 'forecast');
filelist = {'forc_plain.xlsx', 'forc_naive_simple.xlsx', ...
    'forc_naive_trend.xlsx', 'forc_vecm_nodum.xlsx', ...
    'forc_vecm_dum95.xlsx', 'forc_vecm_dum97.xlsx', ...
    'forc_vecm_dum03.xlsx', 'forc_vecm_dum08.xlsx', ...
    'forc_vecm_dumAll.xlsx', ...
    'forc_uniAR.xlsx', 'forc_optiARIMA.xlsx'};
labels = {'plain_gvar', 'naive_simple', 'naive_trend', 'vecm_nodum', ...
    'vecm_dum95', 'vecm_dum97', 'vecm_dum03', 'vecm_dum08', ...
    'vecm_dumAll', 'ar_univ1', 'arima_opti'};
[~, sheets] = xlsfinfo(fullfile(loadpath, filelist{1}));

res = zeros([size(yreal,1)-1 size(yreal,2)-4 numel(labels)]);
yforc = zeros([size(yreal,1)-1 size(yreal,2)-4 numel(labels)]);
mse = zeros([size(yreal,1)-1 numel(labels)]);

for i = 1:numel(labels)
    tmp = xlsread(fullfile(loadpath, filelist{i}), 'eta');
    res(:,:,i) = tmp(1:end-1, 1:end-4);
    tmp = xlsread(fullfile(loadpath, filelist{i}), 'forc');
    yforc(:,:,i) = tmp(1:end-1, 1:end-4);
    mse(:,i) = diag(squeeze(res(:,:,i)) * squeeze(res(:,:,i))') ./ ...
        (size(res(:,:,i),2)-1);
end
disp(mean(mse,1));

%%
dlmwrite(fullfile(wd_data, 'forecast', 'mse_var_pntcast.csv'), ...
    [mse; mean(mse,1)]);

tmp = mat2cell(mse, ones(1,25)*4, numel(labels));
mse_cty = zeros(25, numel(labels));
for i = 1:25
    mse_cty(i,:) = mean(tmp{i,1},1);
end

dlmwrite(fullfile(wd_data, 'forecast', 'mse_cty_pntcast.csv'), ...
    mse_cty);
