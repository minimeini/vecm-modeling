%% initialization
clear all
wd_code = 'C:\respository\vecm-modeling';
cd(wd_code);
[wd,var_info,mdl_info,trdata,tsdata] = init_env_gvar(1,1,1,1);

freq = tsdata.date_info_test.freq;
K = sum(var_info.endoflag(:)==1) + sum(var_info.gvflag(:)==2);

evalpath = fullfile(wd.data,'record');
simpath = fullfile(wd.data,'simulate');
%% BASE. true data
% evaluate event on true endodata and foreign data respectively
[endovar,foreignvar] = get_tsdata_sim(var_info,tsdata);
jevent_true = calc_jevent(endovar, foreignvar, freq);
[class_true, enum, tnum, cnum] = class_assign(jevent_true);

%% BASE. random assignment
niter = 500; eval = zeros(cnum+1, 7, niter);
class_rand = class_assignrand(enum, tnum, cnum, niter);
for i = 1:niter
    [Precision, Recall, FScore, Naive] = ...
        eval_jevent(class_true, squeeze(class_rand(:,:,i)), ...
        enum, tnum, cnum);
    eval(:,:,i) = [Precision.countries Recall.countries ...
        FScore.countries Naive.countries; ...
        Precision.total Recall.total FScore.total Naive.total];
end
eval = mean(eval, 3);
eval = array2table(eval, 'VariableNames', {'macroPrecision', ...
    'microPrecision', 'macroRecall', 'microRecall', ...
    'macroFScore', 'microFScore', 'Naive'});
writetable(eval, fullfile(evalpath, 'eval_randAssign.csv'));

%% GVAR/VAR/VECM/AR. parameter uncertainty + future uncertainty
model = 'var'; update_gv = 0;
nseg = 3; nfuture = 100; 

tsdata_sim = tsdata;

sim_future = zeros(K,var_info.fhorz,0); 
sim_param_future = [];
for i = 1:nseg
    tmp = load(fullfile(simpath,['simulate_' model '_' num2str(i) '.mat']));
    sim_future = cat(3,sim_future,tmp.xsim(:,:,1:nfuture));
    sim_param_future = cat(3,sim_param_future,...
        tmp.xsim(:,:,nfuture+1:end));
end
sim_param_future = cat(3,sim_param_future,tmp.xsim(:,:,1:nfuture));

[endovar,foreignvar] = get_tsdata_sim(var_info,tsdata,...
    sim_param_future,update_gv);
jevent_mdl = calc_jevent(endovar, foreignvar, freq);
[class_mdl, enum, tnum, cnum] = class_assign(jevent_mdl);
[Precision, Recall, FScore, Naive] = eval_jevent(class_true, ...
    class_mdl, enum, tnum, cnum);