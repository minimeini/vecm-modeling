%% initialization
clear all
wd = 'C:\Users\meini\Dropbox\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';

wd_data = fullfile(wd, 'data');
wd_vis = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd_data);
%%
load(fullfile(wd_data, 'parse', 'split_info.mat'));
load(fullfile(wd_data, 'parse', 'train_data.mat'));
load(fullfile(wd_data, 'parse', 'test_data.mat'));
load(fullfile(wd_data, 'train', 'tr_plain_info.mat'));

%%
gnvnames = {'logoilp'};
gnvdata_test = [];
for i = 1:numel(gnvnames)
    gnvdata_test.(gnvnames{i}) = gvdata_test.(gnvnames{i});
end

gvnames = fieldnames(gvdata_test); gv_full = [];
if ~isempty(gvnames)
    for g = 1:numel(gvnames)
        gv_full.(gvnames{g}) = [gvdata.(gvnames{g})' ...
            gvdata_test.(gvnames{g})']';
    end
end

yreal = endodata2y(endodata_test, gnvdata_test, endoglist);
%% 
update_params = 2;
h_step = 1;

[yforc,eta] = forecast_gvar(fhorz,wm_t,wdata_test,date_info.freq,y,...
    gv_full,varxlag,rank,a0,a1,Theta,Lambda0,Lambda,Gamma0,Gamma,...
    endoglist,exoglist,exognxlist,update_params,yreal,eta,h_step);

%%
xlswrite(fullfile(wd_data, 'forecast', 'forc_plain.xlsx'), yforc, 'forc');
xlswrite(fullfile(wd_data, 'forecast', 'forc_plain.xlsx'), yreal, 'real');
xlswrite(fullfile(wd_data, 'forecast', 'forc_plain.xlsx'), eta, 'eta');
