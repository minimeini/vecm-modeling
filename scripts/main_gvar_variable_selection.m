%% Initialization
clear all
wd = 'C:\Users\meini\OneDrive\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';

wd_data = fullfile(wd, 'data');
wd_vis = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd_data);

tr_start = '1993Q1'; tr_end = '2010Q4'; 
obs_test = 24; obs_train = 72;

maxlag_p = 2;
maxlag_q = 2;
wscheme = 'aver';
criterion = 'aic';
psc = 4;
estcase_tmp = 4 * ones(1, obs_train);

dataPath = fullfile(wd_data,'working');
weightPath = fullfile(wd_data,'trade_weight',...
    'weights2D_extended_xcld.csv');
datePath = fullfile(wd_data,'info','date_info.csv');
cityPath = fullfile(wd_data,'trim_cty','trim_cty_ctylist.csv');

%% Integration Test
% - Augmented Dickey-Fuller (ADF) test for a unit root
% ---- H0: trend-random walk (unit root)
% ---- H1: trend-stationary
%
% - Phillips-Perron (PP) Test
% ---- H0: trend-random walk
% ---- H1: trend-stationary
%
% - KPSS Test
% ---- H0: trend-stationary
% ---- H1: trend-random walk
%
% - Variance Ratio Test
% ---- H0: trend-random walk
% ---- H1: trend-stationary
%
%
% - Model
% ---- 'AR': zero mean and zero trend
% ---- 'ARD': non zero mean and zero trend
% ---- 'TS': non zero mean and non zero trend

signlev = 0.05;
test_sheet_adf = zeros(cnum*nvendo, 3);
test_sheet_pp = zeros(cnum*nvendo, 3);
test_sheet_others = zeros(cnum*nvendo, 1);
mdl_type = {'TS', 'ARD', 'AR'};

for i = 1:cnum
    ytrain = [];
    for j = 1:nvendo
        tmp = endodata.(endonames{j});
        ytrain = [ytrain tmp(:,i)];
    end
    
    kstart = nvendo*(i-1) + 1;
    kend = nvendo*i;
    klist = kstart:1:kend;
    for j = 1:nvendo
        ytmp = ytrain(:,j);
        
        for k = 1:numel(mdl_type)
            % >>> ADF Test <<<
            % H0: non-stationary
            % H1: trend-stationary
            pValue = 1; ytmp_df = ytmp; intrk = 0;
            while pValue >= signlev
                % H0: integration rank = intrk + 1
                % H1: integration rank = intrk;
                intrk = intrk + 1; % rank in the H0
                [~,pValue] = adftest(ytmp_df, 'model', mdl_type{k}, ...
                    'alpha', signlev);
                ytmp_df = diff(ytmp_df);
            end
            test_sheet_adf(klist(j),k) = intrk - 1;
            % >>> ADF Test <<<
            
            % >>> PP Test <<<
            % H0: non-stationary
            % H1: trend-stationary
            pValue = 1; ytmp_df = ytmp; intrk = 0;
            while pValue >= signlev
                % H0: integration rank = intrk + 1
                % H1: integration rank = intrk;
                intrk = intrk + 1; % rank in the H0
                [~,pValue] = pptest(ytmp_df, 'model', mdl_type{k}, ...
                    'alpha', signlev);
                ytmp_df = diff(ytmp_df);
            end
            test_sheet_pp(klist(j),k) = intrk - 1;
            % >>> PP Test <<<
        end
        
        % >>> KPSS Test <<<
        % H0: trend-stationary
        % H1: nonstationary
        pValue = 0; ytmp_df = ytmp; intrk = 0;
        while pValue < signlev
            % H0: integration rank = intrk
            % H1: integration rank = intrk + 1;
            intrk = intrk + 1; % rank in the H1
            [~,pValue] = kpsstest(ytmp_df, 'trend', true, ...
                'alpha', signlev);
            ytmp_df = diff(ytmp_df);
        end
        test_sheet_others(klist(j),1) = intrk - 1;
    end
end

test_sheet = [test_sheet_adf test_sheet_pp test_sheet_others];
rownames = repmat(endonames,cnum,1);
fileID = fopen(fullfile(wd_ana, 'integration_rank.csv'), 'w');
fprintf(fileID, 'VARN,ADF-TS,ADF-ARD,ADF-AR,PP-TS,PP-ARD,PP-AR,KPSS\n');
fprintf(fileID, '%s\n', rownames{:});
fclose('all');

dlmwrite(fullfile(wd_ana,'integration_rank.csv'),test_sheet, ...
    '-append', 'delimiter',',','coffset',1);
%% Cointegration Test And Visualization
% H1 Model: A(B'y[t-1]+c[0]) + c[1]
% cointegration relationship: B'y[t-1] (+ c[0])
% Johansen cointegration test under H1 Model:
% - H0: H(r), rank <= r
endonames = {'logtexp', 'logGDP'};
nvendo = numel(endonames);

coint_rank = zeros(cnum,1);
test_sheet_adf = [];
for i = 1:cnum
    ytrain = [];
    for j = 1:nvendo % nvendo: number of endogenous variables
        tmp = endodata.(endonames{j});
        ytrain = [ytrain tmp(:,i)]; % ntime * nvar
    end
    
    % estimate the cointegration rank with Johansen H1 Model
    rank_list = (1:nvendo) - 1;
    [h,~,~,~,mles] = jcitest(ytrain, 'model', 'H1');
    h = table2array(h);
    
    try
        coint_rank(i) = rank_list(find(~h, 1, 'first'));
    catch
        warning('Cointegration ranks == numDims.');
        coint_rank(i) = nvendo - 1;
    end
    
    mles = mles(1,coint_rank(i)+1); mles = mles{1,1};
    coints = mles.paramVals.B' * ytrain';
    
    % >>> ADF Test <<<
    % H0: non-stationary
    % H1: trend-stationary
    if size(coints,1) > 0
        test_adf = zeros(size(coints,1),2);
        test_adf(:,1) = i;
        signlev = 0.05;
        
        for j = 1:size(coints,1)
            ytmp = coints(j,:)';
            pValue = 1; ytmp_df = ytmp; intrk = 0;
            while pValue >= signlev
                % H0: integration rank = intrk + 1
                % H1: integration rank = intrk;
                intrk = intrk + 1; % rank in the H0
                [~,pValue] = adftest(ytmp_df, 'model', 'TS', ...
                    'alpha', signlev);
                ytmp_df = diff(ytmp_df);
            end
            test_adf(j,2) = intrk - 1;
        end
        
        test_sheet_adf = [test_sheet_adf; test_adf];
    end
    % >>> ADF Test <<<
    
    f = figure;
    nsubs = size(ytrain,2) + size(coints,1);
    ncol = 2;
    nrow = ceil(nsubs/ncol);
    flag = 0;
    for j = 1:nsubs
        subplot(nrow,ncol,j);
        if j <= size(ytrain,2)
            plot(1:size(ytrain,1), ytrain(:,j));
            title(['Endogenous Var - ' endonames{j}]);
        elseif size(coints,1) > 0
            plot(1:size(coints,2), coints(j-size(ytrain,2),:));
            title(['Cointegration - ' num2str(j-size(ytrain,2))]);
            flag = 1;
        end
    end
    
    suptitle([cnames{i} ' -cointegration = ' num2str(flag)]);
    print(f, fullfile(wd_vis, 'cointegration', ...
        ['coint_' num2str(i) '_' cnames{i} '_endo' ...
        num2str(nvendo) '.png']), '-dpng');
    close all
    delete(f);
    delete(gcf);
end

dlmwrite(fullfile(wd_ana,...
    ['cointegration_rank_endog' num2str(nvendo) '.csv']),coint_rank);

%% Endogenous only model (== VECM Variable Selection)
% without any foreign variables and global variables, we have GVAR==VECM
%
% Comparison Criteria
% 1. compare the point forecast on logtexp and logGDP only
% 2. time series cross validation
%
% Evaluate with MAPE on two horizon step: 1 or 4.

signlev = 0.05;
p_list = [0 1 2 3];
h_step = [1 4];
cv_split = round(0.2 * train_pos.endpos);
ncv = train_pos.endpos - cv_split + 1 - max(h_step);

endonames = {'logtexp', 'logGDP', 'logownp', 'logtimp'};
nvendo = numel(endonames);

mape_cv_cty = struct();
mape_forc_cty = struct();
for i = 1:cnum
    ytrain = [];
    for j = 1:nvendo
        tmp = endodata.(endonames{j});
        ytrain = [ytrain tmp(:,i)];
    end
    
    ytest = [];
    for j = 1:nvendo
        tmp = endodata_test.(endonames{j});
        ytest = [ytest tmp(:,i)];
    end
    
    % estimate the cointegration rank with Johansen H1 Model
    rank_list = (1:nvendo) - 1;
    h = table2array(jcitest(ytrain, 'model', 'H1'));
    try
        coint_rank = rank_list(find(~h, 1, 'first'));
    catch
        warning('Cointegration ranks == numDims.');
    end
    if isempty(coint_rank), coint_rank = nvendo - 1; end
    
    % select the optimal lag by AIC and BIC
    aic = 9999; bic = 9999; aic_p = 0; bic_p = 0;
    for j = 1:numel(p_list)
        p = p_list(j);
        defMdl = vecm(size(ytrain,2), coint_rank, p);
        estMdl = estimate(defMdl, ytrain);
        results = summarize(estMdl);
        
        if results.AIC <= aic, aic = results.AIC; aic_p = p; end
        if results.BIC <= bic, bic = results.BIC; bic_p = p; end
    end
    p = min(aic_p, bic_p);
    
    % define the optimal model
    defMdl = vecm(size(ytrain,2), coint_rank, p);
    
    % Time Series Cross Validation in the Training Set
    mape_cv = struct();
    for k = 1:numel(h_step)
        mape_cv.(['h' num2str(h_step(k))]) = []; 
    end
    
    for j = 1:ncv
        y_train = ytrain(1:cv_split,:);
        y_test = ytrain(h_step + cv_split,:);
        
        estMdl = estimate(defMdl, y_train);
        forc = forecast(estMdl, max(h_step), y_train);
        forc = forc(h_step,:);
        
        for k = 1:numel(h_step)
            tmp = abs(y_test(k,:) - forc(k,:)) ./ y_test(k,:) .* 100;
            mape_cv.(['h' num2str(h_step(k))]) = ...
                [mape_cv.(['h' num2str(h_step(k))]); tmp];
        end
        
    end
    
    mape_cv_cty.(cnames{i}) = mape_cv;
    
    % Point Forecasting: h-step rolling forecasting
    estMdl = estimate(defMdl, ytrain);
    mape_forc = struct();
    for k = 1:numel(h_step)
        mape_forc.(['h' num2str(h_step(k))]) = [];
    end
    
    for h = 1:fhorz
        if h > 1
            ytrnew = [ytrain; ytest(1:h,:)]; 
            ytstnew = ytest(h+1:end,:); 
        else
            ytrnew = ytrain;
            ytstnew = ytest;
        end
        
        estMdl = estimate(defMdl, ytrnew);
        forc = forecast(estMdl, max(h_step), ytrnew);
        forc = forc(h_step,:);
        
        for k = 1:numel(h_step)
            if h_step(k) <= size(ytstnew,1)
                real = ytstnew(h_step(k),:);
                tmp = abs(real- forc(k,:)) ./ real .* 100;
                mape_forc.(['h' num2str(h_step(k))]) = ...
                    [mape_forc.(['h' num2str(h_step(k))]); tmp];
            else
                break;
            end
        end
        
        mape_forc_cty.(cnames{i}) = mape_forc;
    end
end

mape_cv_aver = zeros(cnum, nvendo);
mape_forc_aver = zeros(cnum, nvendo);
for i = 1:cnum
    mape_cv_aver(i,:) = mean(mape_cv_cty.(cnames{i}).h4,1);
    mape_forc_aver(i,:) = mean(mape_forc_cty.(cnames{i}).h4,1);
end

dlmwrite(fullfile(wd_ana,'mape_cv_cty_endog4_h4.csv'), mape_cv_aver);
dlmwrite(fullfile(wd_ana,'mape_forc_cty_endog4_h4.csv'), mape_forc_aver);

%% Endogenous (3) + Dummies + Oil Price

% DUMMIES: {'dumAll','dum95c','dum97c', 'dum00c','dum03s','dum05c','dum08c'}
% GNV: {'logoilp'}

% MOD_TYPE: {'exog_x', 'coint_free', 'coint_restr'}
mod_type = 'coint_free';

dumnames = {'dum95c','dum00c','dum03s','dum08c'};
gnvnames = {'logoilp'};
endonames = {'logtexp','logGDP'};
gvnames = [gnvnames dumnames];
var_we = gnvnames;
var_status = [ones(size(endonames)) ones(size(gvnames))*2];
% 1 for endogenous, 2 for exogenous
[train_data,test_data,cnames,freq] = split_data(tr_start,tr_end,obs_test,...
    endonames,gvnames,dataPath,var_we, weightPath,datePath,cityPath);

obs_train = size(train_data.endo.(endonames{1}),1);

gvtrain = []; gvtest = [];
for j = 1:numel(gvnames)
    gvtrain = [gvtrain train_data.gv.(gvnames{j})];
    gvtest = [gvtest test_data.gv.(gvnames{j})];
end

[row,col] = find(gvtrain(:,2:end));
row = max(row);

signlev = 0.05;
p_list = [1 2 3];
h_step = [1 4];
cv_split = max(round(0.5 * obs_train),row);
ncv = obs_train - cv_split + 1 - max(h_step);
nvendo = numel(endonames);
cnum = numel(cnames);

mape_cv_cty = struct();
mape_forc_cty = struct();
for i = 1:cnum
    fprintf('>>> %d. country %s:\n', i, cnames{i});
    %% variables
    ytrain = [];
    for j = 1:nvendo
        tmp = train_data.endo.(endonames{j});
        ytrain = [ytrain tmp(:,i)];
    end

    ytest = [];
    for j = 1:nvendo
        tmp = test_data.endo.(endonames{j});
        ytest = [ytest tmp(:,i)];
    end
    
    if strcmp(mod_type,'coint_free')
        ytrain = [ytrain gvtrain];
        ytest = [ytest gvtest];
    end
    
    %% select lag for coint_restr
    if strcmp(mod_type,'coint_restr')
        info = 2; % 2=AIC, 3=SBC
        
        aic_sbc = [];
        F_serialcorr = [];
        for p = p_list(1):p_list(end)
            for q = p_list(1):p_list(end)
                [logl_aic_sbc, psc_degfrsc_Fcrit_Fsc] = ...
                    select_varxlag(p_list(end),psc,ytrain,p,gvtrain,q);
                aic_sbc = [logl_aic_sbc p q];
                F_serialcorr = [psc_degfrsc_Fcrit_Fsc];
            end
        end
        
        varxlag = [];
        for r = 1:size(aic_sbc,1)
            if aic_sbc(r,info) == max(aic_sbc(:,info))
                varxlag = aic_sbc(r,4:5); % optiP, optiQ
            end
        end
        
    end

    %% estimate the cointegration rank with Johansen H1 Model
    coint_rank = [];
    
    if ~strcmp(mod_type,'coint_restr')
        rank_list = (1:size(ytrain,2)) - 1;
        h = table2array(jcitest(ytrain, 'model', 'H1'));
        try
            coint_rank = rank_list(find(~h, 1, 'first'));
        catch
            warning('Cointegration ranks == numDims.');
        end
    else
        curpath = cd;
        filepath = fullfile(curpath, 'preset', 'coint_critvalues.xls');
        [trace_crit95_c4, trace_crit95_c3, trace_crit95_c2, ...
            maxeig_crit95_c4, maxeig_crit95_c3, maxeig_crit95_c2] = ...
            critical_values(filepath);

        estcase = 4;
        [junk,trace_out,maxeig_out] = cointegration_test(max(p_list),...
            estcase,ytrain,varxlag(1),gvtrain,varxlag(2));
        
        endo_tmp = struct(); gv_tmp = struct();
        estcase_tmp = struct(); trace = struct(); maxeig = struct();
        
        endo_tmp.(cnames{i}) = ytrain;
        gv_tmp.(cnames{i}) = gvtrain;
        estcase_tmp.(cnames{i}) = estcase;
        trace.(cnames{i}) = trace_out;
        maxeig.(cnames{i}) = maxeig_out;
        
        coint_rank = get_rank(1,cnames(i),endo_tmp,gv_tmp,trace,...
            trace_crit95_c2,trace_crit95_c3,trace_crit95_c4,...
            maxeig,maxeig_crit95_c2,maxeig_crit95_c3,...
            maxeig_crit95_c4,estcase_tmp);
        coint_rank = coint_rank.(cnames{i});
        
    end
    
    if isempty(coint_rank), coint_rank = size(ytrain,2) - 1; end
    
    %% select the optimal lag by AIC and BIC: exog_x, coint_free
    if ~strcmp(mod_type,'coint_restr')
        aic = 9999; bic = 9999; aic_p = 0; bic_p = 0;
        for j = 1:numel(p_list)
            p = p_list(j);
            defMdl = vecm(size(ytrain,2), coint_rank, p);
            estMdl = estimate(defMdl, ytrain);
            results = summarize(estMdl);
        
            if results.AIC <= aic, aic = results.AIC; aic_p = p; end
            if results.BIC <= bic, bic = results.BIC; bic_p = p; end
        end
        varxlag = min(aic_p, bic_p);
        
        % define the optimal model
        defMdl = vecm(size(ytrain,2), coint_rank, varxlag);
    end
    
    %% Time Series Cross Validation in the Training Set
    mape_cv = struct();
    for k = 1:numel(h_step)
        mape_cv.(['h' num2str(h_step(k))]) = []; 
    end
    
    for j = 1:ncv
        y_train = ytrain(1:cv_split,:);
        y_test = ytrain(h_step + cv_split,:);
        gv_train = gvtrain(1:cv_split,:);
        gv_test = gvtrain(cv_split+1:cv_split+max(h_step),:);
        
        switch mod_type
            case 'exog_x'
                estMdl = estimate(defMdl,y_train,'X',gv_train);
                forc = forecast(estMdl,max(h_step),y_train,'X',gv_test);
                forc = forc(h_step,1:2);
            case 'coint_free'
                estMdl = estimate(defMdl,y_train);
                forc = forecast(estMdl,max(h_step),y_train);
                forc = forc(h_step,1:2);
            case 'coint_restr'
                % vecm estimation
                z_tmp = []; z_tmp.(cnames{i}) = [y_train gv_train]';
                endolist = []; endolist.(cnames{i}) = endonames;
                exoglist = []; exoglist.(cnames{i}) = gvnames;
                exognxlist = []; exognxlist.(cnames{i}) = gvnames;
                vlag = []; vlag.(cnames{i}) = varxlag;
                
                [beta.(cnames{i}),alpha.(cnames{i}),Psi.(cnames{i}),...
                    epsilon,Omega,ecm,std] = mlcoint(max(varxlag(:)),...
                    coint_rank,estcase,psc,y_train,varxlag(1),...
                    gv_train,varxlag(2));
                
                % vecm to varx
                [a0,a1,Theta,Lambda0,Lambda] = vecx2varx(max(varxlag(:)),...
                    1,cnames(i),z_tmp,endolist,vlag,...
                    alpha,beta,Psi,estcase_tmp);
                
                % forecast
                forc = forecast_GVAR_onestep([],h_step,freq,y_train',...
                    train_data.gv,max(varxlag(:)),a0,a1,Theta,[],[],...
                    Lambda0,Lambda,[],endolist,exognxlist,exoglist);
                forc = forc(1:2,h_step)';
                
        end
        
        for k = 1:numel(h_step)
            tmp = abs(y_test(k,1:2) - forc(k,:)) ./ y_test(k,1:2) .* 100;
            mape_cv.(['h' num2str(h_step(k))]) = ...
                [mape_cv.(['h' num2str(h_step(k))]); tmp];
        end
        
    end
    
    mape_cv_cty.(cnames{i}) = mape_cv;
    
    %% Point Forecasting: estimate model
    switch  mod_type
        case 'exog_x'
            estMdl = estimate(defMdl, ytrain, 'X', gvtrain);
            results = summarize(estMdl);
            isbeta = contains(results.Table.Properties.RowNames,'Beta');
            betaresults = results.Table(isbeta,:);
            betaidx = mod(find(betaresults.PValue<0.05),nvendo);
            betaidx(betaidx==0) = nvendo;
            whichsig = estMdl.SeriesNames(betaidx);
            disp(whichsig);
        case 'coint_free'
            estMdl = estimate(defMdl, ytrain);
        case 'coint_restr'
    end
    
    %% Point Forecasting: h-step rolling forecasting
    mape_forc = struct();
    for k = 1:numel(h_step)
        mape_forc.(['h' num2str(h_step(k))]) = [];
    end
    
    ytrnew = []; ytstnew = [];
    gvtrnew = []; gvtstnew = [];
    for h = 1:(obs_test-max(h_step))
        ytrnew = [ytrain; ytest(1:h,:)];
        ytstnew = ytest(h+1:end,:);
            
        gvtrnew = [gvtrain; gvtest(1:h,:)];
        gvtstnew = gvtest(h+1:end,:);
        
        switch mod_type
            case 'exog_x'
                estMdl = estimate(defMdl,ytrnew,'X',gvtrnew);
                forc = forecast(estMdl,max(h_step),ytrnew,'X',gvtstnew);
                forc = forc(h_step,1:2);
            case 'coint_free'
                estMdl = estimate(defMdl,ytrnew);
                forc = forecast(estMdl,max(h_step),ytrnew);
                forc = forc(h_step,1:2);
            case 'coint_restr'
        end
        
        for k = 1:numel(h_step)
            if h_step(k) <= size(ytstnew,1)
                real = ytstnew(h_step(k),1:2);
                tmp = abs(real- forc(k,:)) ./ real .* 100;
                mape_forc.(['h' num2str(h_step(k))]) = ...
                    [mape_forc.(['h' num2str(h_step(k))]); tmp];
            else
                break;
            end
        end
        
        mape_forc_cty.(cnames{i}) = mape_forc;
    end
end

%%
hlabel = 'h1';

mape_cv_aver = zeros(cnum, nvendo);
mape_forc_aver = zeros(cnum, nvendo);
for i = 1:cnum
    mape_cv_aver(i,:) = mean(mape_cv_cty.(cnames{i}).(hlabel),1);
    mape_forc_aver(i,:) = mean(mape_forc_cty.(cnames{i}).(hlabel),1);
end

dlmwrite(fullfile(wd_ana,...
    ['mape_cv_cty_' mod_type '_endog2_dum4sep_vecm_' hlabel '.csv']), ...
    mape_cv_aver);
dlmwrite(fullfile(wd_ana,...
    ['mape_forc_cty_'+mod_type+'endog2_dum4sep_vecm_restr_' hlabel '.csv']), ...
    mape_forc_aver);

%% Endogenous (4) + Dummies + Oil Price

%% Endogenous + Foreign + Global
