function [estMdl,rank] = train_vecm(var_info,trdata,lag,varargin)
nargs = -nargin('train_vecm') - 1;
rank = [];
if nargin >= nargs + 1, rank = varargin{1}; end

cnum = numel(var_info.cnames);

if ~isfield(var_info,'endoglist')
var_info.endoglist = get_endoglist(var_info.cnames,var_info.endonames,...
    var_info.endoflag,var_info.gvnames,var_info.gvflag);
end
endog = create_endogvars(trdata.endodata,trdata.gvdata,...
    var_info.endoglist,var_info.cnames);

%%
if isempty(rank)
    rank = struct();
    for n = 1:cnum
        tmp = endog.(var_info.cnames{n});
        h = jcitest(tmp,'lags',lag,'test','maxeig','display','off');
        rr = find(table2array(h)==1,1,'last');
        if isempty(rr), rr=0; end
        rank.(var_info.cnames{n}) = rr;
    end
end
   
%%
estMdl = struct();
for n = 1:cnum
    tmp = endog.(var_info.cnames{n});
    rr = rank.(var_info.cnames{n});
    mdl = vecm(size(tmp,2), rr, lag);
    estMdl.(var_info.cnames{n}) = estimate(mdl, tmp);
end

end