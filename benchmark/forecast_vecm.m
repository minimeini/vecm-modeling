function [xforc,eta] = forecast_vecm(estMdl,var_info,trdata,tsdata,fhorz,varargin)
% function xsim = forecast_vecm(estMdl,var_info,trdata,tsdata,fhorz)
% -------------------------------------------------------
% >>> Required Input
%
% - estMdl: struct, fieldnames = cnames, field = varm estimation
% - rank: struct
% - lag: numeric
% - trdata: struct, training data
%   - endodata: struct, fieldnames = endonames, field = [ntime cnum] 
%   numeric matrix
%   - gvdata: struct, fieldnames = gvnames, field = [ntime 1] numeric
% matrix
%   - wdata: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info: struct, fieldnames = {'start', 'end', 'freq'}
% - tsdata: struct, testing data
%   - endodata_test: struct, fieldnames = endonames, field = [ntime cnum]
%   numeric matrix
%   - gvdata_test: struct, fieldnames = gvnames, field = [ntime 1] 
%   numeric matrix
%   - wdata_test: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info_test: struct, fieldnames = {'start', 'end', 'freq'}
% - var_info: struct, variable information
%   - cnames: 1d char array, name of countries
%   - endonames: 1d char array, names of endogenous variable and foreign
%   variable
%   - gvnames: 1d char array, names of global (strong+weak) variable
%   - gnvnames: 1d char array, names of global (weak) variable
%   - endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
%   - gvflag: [cnum gvnum] numeirc matrix, 0=not included, 1=included as
%   exogenous, 2=included as endogenous
% - fhorz: numeric, forecasting horizons, default = 1
% -------------------------------------------------------
% >>> Optional Input
% - h_step: numeric, step of forecasting, default = []
% - update_params: logical, if re-estimate model parameters after perform
% 'h_step' ahead forecasting, default = 0
% -------------------------------------------------------
% >>> Output
%
% - xforc: 2d numeric matrix, forecasting results, [nvar fhorz]
% - eta: 2d numeric matrix, forecasting error, [nvar fhorz]

%%
nargs = -nargin('forecast_vecm') - 1;
h_step = [];
update_params = 0;

if nargin >= nargs + 1, h_step = varargin{1}; end
if nargin >= nargs + 2, update_params = varargin{2}; end

if update_params==1 && isempty(h_step), h_step = 1; end
if update_params==1 && isempty(tsdata),error('INPUT ERROR.'); end

%% Input formatting
cnum = numel(var_info.cnames);

train_real = struct();
[train_real.endog,train_real.endoglist,train_real.exog,...
    train_real.exoglist,train_real.exognx,train_real.exognxlist,...
    train_real.wm_t,train_real.foreign,train_real.X] = ...
    gen_params(var_info.cnames,trdata.date_info,var_info.endonames,...
    var_info.gvnames,var_info.gnvnames,var_info.endoflag,...
    var_info.gvflag,trdata.endodata,trdata.gvdata,trdata.wdata);

test_real = struct();
if ~isempty(tsdata)
    [test_real.endog,test_real.endoglist,test_real.exog,...
        test_real.exoglist,test_real.exognx,test_real.exognxlist,...
        test_real.wm_t,test_real.foreign,test_real.X] = ...
        gen_params(var_info.cnames,tsdata.date_info_test,...
        var_info.endonames,var_info.gvnames,var_info.gnvnames,...
        var_info.endoflag,var_info.gvflag,...
        tsdata.endodata_test,tsdata.gvdata_test,tsdata.wdata_test);
end

Klist = arrayfun(@(x) numel(train_real.endoglist.(var_info.cnames{x})),...
    1:cnum);
Klist_ext = [0 Klist]; K = sum(Klist(:));

xforc = zeros(K,fhorz);
%%  
for i = 1:cnum    
    ytrain = train_real.endog.(var_info.cnames{i}); %ntime*nvar
    estMdl_tmp = estMdl.(var_info.cnames{i});
    lag = estMdl_tmp.P-1; rank = estMdl_tmp.Rank;
    Mdl = vecm(size(ytrain,2),rank,lag);
    
    if update_params==0
        tmp_sim = forecast(estMdl_tmp,fhorz,ytrain); %ntime*nvar
        xforc(sum(Klist_ext(1:i))+1:sum(Klist_ext(1:i+1)),:) = tmp_sim';
    else
        for j = 1:fhorz
            % trim for rolling h-step ahead forecasting
            ytrain_tmp = ytrain(1:end-h_step+1,:);
            
            % update model parameters
            estMdl_tmp = estimate(Mdl,ytrain_tmp);
            
            % perform forecasting
            tmp = forecast(estMdl_tmp,h_step,ytrain_tmp);
            tmp = tmp(end,:);
            xforc(sum(Klist_ext(1:i))+1:sum(Klist_ext(1:i+1)),j) = tmp;

            % update training set
            ytrain_new = test_real.endog.(var_info.cnames{i})(j,:);
            ytrain = [ytrain; ytrain_new];
        end
    end
end

%% forecasting error
if ~isempty(tsdata)
    eta = test_real.X - xforc;
else
    eta = [];
end
end