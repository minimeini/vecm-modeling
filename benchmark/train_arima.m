function [estMdl,eta,p,d,q] = train_arima(var_info,trdata,varargin)
nargs = -nargin('train_arima') - 1;
p = -1; d = -1; q = -1;
maxp = 2; maxd = 1; maxq = 2;
if nargin >= nargs+1, p=varargin{1}; end
if nargin >= nargs+2, d=varargin{2}; end
if nargin >= nargs+3, q=varargin{3}; end
if nargin >= nargs+4, maxp=varargin{4}; end
if nargin >= nargs+5, maxd=varargin{5}; end
if nargin >= nargs+6, maxq=varargin{6}; end

%%
[endog,endoglist,exog,exoglist,exognx,exognxlist,wm_t,foreign,X] = ...
    gen_params(var_info.cnames,trdata.date_info,var_info.endonames,...
    var_info.gvnames,var_info.gnvnames,var_info.endoflag,...
    var_info.gvflag,trdata.endodata,trdata.gvdata,trdata.wdata);

if any([p d q]==-1)
    [p,d,q] = find_optimal_lags(var_info,trdata,p,d,q,maxp,maxd,maxq);
end

if numel(p)~=numel(q)~=numel(d)
    error('COMPUTATIONAL ERROR: unexpected error in lags estimation.');
end

%%
K = size(X,1);
estMdl = {};
eta = zeros(size(X));
for i = 1:K
    if numel(p)>1
        ptmp = p(i); dtmp = d(i); qtmp = q(i);
    else
        ptmp = p; dtmp = d; qtmp = q;
    end
    Mdl = arima(ptmp,dtmp,qtmp);
    estMdl{i} = estimate(Mdl,X(i,:)','Display','off');
    eta(i,:) = infer(estMdl{i},X(i,:)')';
end

end

%% Find Optimal p, d, q
function [optiP,optiD,optiQ,minCrit] = find_optimal_lags(X,origp,origd,...
    origq,maxp,maxd,maxq)
minp = 0; mind = 0; minq = 0;

if origp>-1
    piter = origp;
else
    piter = minp:maxp;
end
if origd>-1
    diter = origd;
else
    diter = mind:maxd;
end
if origq>-1
    qiter = origq;
else
    qiter = minq:maxq;
end

optiP = zeros(1,K); optiQ = zeros(1,K); optiD = zeros(1,K); 
minCrit = zeros(1,K)+9999;
for i = 1:K
    tmpdata = X(i,:)';
    for d = diter
        for p = piter
            for q = qiter 
                if p==0 && q==0 && d==0, continue; end
                mdl = arima(p,d,q);
                try
                    [~,~,logL] = estimate(mdl, tmpdata, ...
                        'Display', 'off');
                    [~,bic] = aicbic(logL, p+q+1, size(tmpdata,1));
                
                    if bic < minCrit(i)
                        minCrit(i) = bic; optiP(i) = p; 
                        optiQ(i) = q; optiD(i) = d;
                    end
                catch
                    break;
                end
            end
        end
    end
    fprintf('optimal p: %d, optimal d: %d, optimal q: %q, minCrit: %f\n', ...
        optiP(i), optiD(i), optiQ(i), minCrit(i));
end

end