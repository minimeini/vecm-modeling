function [estMdl,eta] = train_var(var_info,trdata,lag)
cnum = numel(var_info.cnames);

if ~isfield(var_info,'endoglist')
var_info.endoglist = get_endoglist(var_info.cnames,var_info.endonames,...
    var_info.endoflag,var_info.gvnames,var_info.gvflag);
end
endog = create_endogvars(trdata.endodata,trdata.gvdata,...
    var_info.endoglist,var_info.cnames);

estMdl = struct();
eta = struct();
for n = 1:cnum
    tmp = endog.(var_info.cnames{n});
    mdl = varm(size(tmp,2), lag);
    [estMdl.(var_info.cnames{n}),~,~,eta.(var_info.cnames{n})] = ...
        estimate(mdl, tmp);
end

end