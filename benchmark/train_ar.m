function estMdl = train_ar(var_info,trdata,lag)
cnum = numel(var_info.cnames);

[endog,endoglist,exog,exoglist,exognx,exognxlist,wm_t,foreign,X] = ...
    gen_params(var_info.cnames,trdata.date_info,var_info.endonames,...
    var_info.gvnames,var_info.gnvnames,var_info.endoflag,...
    var_info.gvflag,trdata.endodata,trdata.gvdata,trdata.wdata);

K = size(X,1);
estMdl = {};
for i = 1:K, estMdl{i} = ar(X(i,:)',lag); end

end