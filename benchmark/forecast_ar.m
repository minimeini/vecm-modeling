function [xforc,eta] = forecast_ar(estMdl,var_info,trdata,tsdata,fhorz)
% function xsim = forecast_ar(estMdl,var_info,trdata,tsdata,fhorz)
% -------------------------------------------------------
% >>> Procedure
%
% 1. [Parameter Uncertainty] In-sample simulation: simulate a training
% set using precomputed model parameters
% 2. [Parameter Uncertainty] Paramter re-estimation: re-estimate (MLE) model
% paramters using the simulated training set
% 3. [Future Uncertainty] h-step ahead forecasting with bootstrapped
% residuals (eta)
% -------------------------------------------------------
% >>> Required Input
%
% - estMdl: struct, fieldnames = cnames, field = varm estimation
% - lag
% - trdata: struct, training data
%   - endodata: struct, fieldnames = endonames, field = [ntime cnum] 
%   numeric matrix
%   - gvdata: struct, fieldnames = gvnames, field = [ntime 1] numeric
% matrix
%   - wdata: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info: struct, fieldnames = {'start', 'end', 'freq'}
% - tsdata: struct, testing data
%   - endodata_test: struct, fieldnames = endonames, field = [ntime cnum]
%   numeric matrix
%   - gvdata_test: struct, fieldnames = gvnames, field = [ntime 1] 
%   numeric matrix
%   - wdata_test: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info_test: struct, fieldnames = {'start', 'end', 'freq'}
% - var_info: struct, variable information
%   - cnames: 1d char array, name of countries
%   - endonames: 1d char array, names of endogenous variable and foreign
%   variable
%   - gvnames: 1d char array, names of global (strong+weak) variable
%   - gnvnames: 1d char array, names of global (weak) variable
%   - endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
%   - gvflag: [cnum gvnum] numeirc matrix, 0=not included, 1=included as
%   exogenous, 2=included as endogenous
% - fhorz
% -------------------------------------------------------
% >>> Output
%
% - xforc: 2d numeric matrix, forecasting results, [nvar fhorz]

%% Input formatting
cnum = numel(var_info.cnames);

train_real = struct();
[train_real.endog,train_real.endoglist,train_real.exog,...
    train_real.exoglist,train_real.exognx,train_real.exognxlist,...
    train_real.wm_t,train_real.foreign,train_real.X] = ...
    gen_params(var_info.cnames,trdata.date_info,var_info.endonames,...
    var_info.gvnames,var_info.gnvnames,var_info.endoflag,...
    var_info.gvflag,trdata.endodata,trdata.gvdata,trdata.wdata);

test_real = struct();
if ~isempty(tsdata)
    [test_real.endog,test_real.endoglist,test_real.exog,...
        test_real.exoglist,test_real.exognx,test_real.exognxlist,...
        test_real.wm_t,test_real.foreign,test_real.X] = ...
        gen_params(var_info.cnames,tsdata.date_info_test,...
        var_info.endonames,var_info.gvnames,var_info.gnvnames,...
        var_info.endoflag,var_info.gvflag,...
        tsdata.endodata_test,tsdata.gvdata_test,tsdata.wdata_test);
end

Klist = arrayfun(@(x) numel(train_real.endoglist.(var_info.cnames{x})),...
    1:cnum);
K = sum(Klist(:));
xforc = zeros(K,fhorz);
%%  
for i = 1:K
    for jter = 1:nfuture
        tmp_sim = forecast(estMdl{i},train_real.X(i,:)',fhorz);
        xforc(i,:) = tmp_sim;
    end
end

%%
if ~isempty(tsdata)
    eta = test_real.X - xforc;
else
    eta = [];
end
end
