function xsim = simulate_var(lag,var_info,trdata,tsdata,varargin)
% function xsim = simulate_var(lag,var_info,trdata,tsdata,varargin)
% -------------------------------------------------------
% >>> Method
%
% 1. [Parameter Uncertainty] In-sample simulation: simulate a training
% set using precomputed model parameters
% 2. [Parameter Uncertainty] Paramter re-estimation: re-estimate (MLE) model
% paramters using the simulated training set
% 3. [Future Uncertainty] h-step ahead forecasting with bootstrapped
% residuals (eta) 
% -------------------------------------------------------
% >>> Workflow
%
% > iterated by forecasting horizon/step
%
% Step 1. Get the model parameters and burnin training set based on 
% original training set
% Step 2. In-sample simulation of training set using parameters
% obtain in step 0.
% Step 3. Re-estimate model parameters using the simulated
% training set
% Step 4. h-step ahead rolling forecasting using the real training
% set `trdata_hs`
% Step 5. Update the original training set by 1 time step using 
% real value
% -------------------------------------------------------
% >>> Required Input
%
% - lag: numeric, lag of var model
% - trdata: struct, training data
%   - endodata: struct, fieldnames = endonames, field = [ntime cnum] 
%   numeric matrix
%   - gvdata: struct, fieldnames = gvnames, field = [ntime 1] numeric
% matrix
%   - wdata: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info: struct, fieldnames = {'start', 'end', 'freq'}
% - tsdata: struct, testing data
%   - endodata_test: struct, fieldnames = endonames, field = [ntime cnum]
%   numeric matrix
%   - gvdata_test: struct, fieldnames = gvnames, field = [ntime 1] 
%   numeric matrix
%   - wdata_test: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info_test: struct, fieldnames = {'start', 'end', 'freq'}
% - var_info: struct, variable information
%   - cnames: 1d char array, name of countries
%   - endonames: 1d char array, names of endogenous variable and foreign
%   variable
%   - gvnames: 1d char array, names of global (strong+weak) variable
%   - gnvnames: 1d char array, names of global (weak) variable
%   - endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
%   - gvflag: [cnum gvnum] numeirc matrix, 0=not included, 1=included as
%   exogenous, 2=included as endogenous
% -------------------------------------------------------
% >>> Optional Input
%
% - nparams: numeric, # of iterations for parameter uncertainty, default =
% 10
% - nfuture: numeric, # of iterations for future uncertainty, default = 50
% - fhorz: numeric, forecasting horizons, default = 1
% - h_step: numeric, step of forecasting, default = []
% - update_params: logical, if re-estimate model parameters after perform
% 'h_step' ahead forecasting, default = 0 
% -------------------------------------------------------
% >>> Output
%
% - xsim: 3d numeric matrix, forecasting results, 
% [nvar fhorz nparams*nfuture]

%% Get optional input
nargs = -nargin('simulate_var') - 1;
% >>> simulation options
nparams = 10; % # of iterations for parameter uncertainty, 0 == no parameter uncertainty
nfuture = 50; % # of iterations for future uncertainty, 0 == no future uncertainty
fhorz = 1;
h_step = 1;
update_params = 0;

if nargin >= nargs + 1, nparams = varargin{1}; end
if nargin >= nargs + 2, nfuture = varargin{2}; end
if nargin >= nargs + 3, fhorz = varargin{3}; end
if nargin >= nargs + 4, h_step = varargin{4}; end
if nargin >= nargs + 5, update_params = varargin{5}; end

%% Input validation
err_msg = [];
err_msg.dont_run = 'Finish without execution.';
err_msg.invalid_input = 'Invalid Input.';
err_msg.compute_error = 'Computational error.';

if nparams<0 || nfuture<0, error(err_msg.invalid_input); end
if nparams==0 && nfuture==0, error(err_msg.dont_run); end
if fhorz<0, error(err_msg.invalid_input); end
if nfuture>1 && fhorz==0, error(err_msg.invalid_input); end

if update_params==1 && isempty(h_step), h_step = 1; end

%% Input formatting
nparams = nparams + 1;
cnum = numel(var_info.cnames);

Klist = arrayfun(@(x) numel(var_info.endoglist.(var_info.cnames{x})),1:cnum);
K = sum(Klist(:));
xsim = zeros(K,fhorz,nparams*nfuture);
%%
for i = 1:fhorz
    % Step 0. Get the model parameters and burnin training set based on 
    % original training set
    trdata_hs = trim_data_train_orig(var_info,trdata,h_step);
    estMdl_hs = train_var(var_info,trdata_hs,lag);
    nsim_hs = trdata_hs.obs_train - lag;
    burnin_hs = get_data_burnin(var_info,trdata_hs,estMdl_hs);

    for iter = 1:nparams
        % Step 1. In-sample simulation of training set using parameters
        % obtain in step 0.
        if iter>1
            simdata = struct();
            for n = 1:cnum
                tmp_burnin = burnin_hs.(var_info.cnames{n}); %ntime*nendog
                tmp_sim = simulate(estMdl_hs.(var_info.cnames{n}),...
                    nsim_hs,'NumPaths',1,'Y0',tmp_burnin);
                simdata.(var_info.cnames{n}) = [tmp_burnin; tmp_sim];
                % simdata: fieldnames=cnames, field=[nsim_hs nendog]
            end
            trdata_sim = update_data_train_sim(var_info,trdata_hs,simdata);
        
            % Step 2. Re-estimate model parameters using the simulated
            % training set
            estMdl_sim = train_var(var_info,trdata_sim,lag);
        else % iter==1, using the parameters estimated by the original 
            % training set
            estMdl_sim = estMdl_hs;
        end
        
        % Step 3. h-step ahead rolling forecasting using the real training
        % set `trdata_hs`
        xsim(:,i,(iter-1)*nfuture+1:iter*nfuture) = ...
            simulate_var_roll_1step(estMdl_sim,var_info,trdata_hs,...
            h_step,nfuture);
    end
    
    % Step 4. Update the original training set by 1 time step using 
    % real value
    trdata = update_data_train_orig(var_info,trdata,tsdata);
end

Xvar = sum(abs(xsim),2);
if any(Xvar(:)==0), error('CALCULATION FAILED: empty value in xsim.'); end

end

%% get burnin training set
function burnin = get_data_burnin(var_info,trdata,estMdl)
cnum = numel(var_info.cnames);
endog = create_endogvars(trdata.endodata,trdata.gvdata,...
    var_info.endoglist,var_info.cnames);

burnin = struct();
for i = 1:cnum
    lag = estMdl.(var_info.cnames{i}).P;
    burnin.(var_info.cnames{i}) = endog.(var_info.cnames{i})(1:lag,:);
end
end


%% h-step ahead forecasting with bootstrapped residuals (eta)
% Simulated with Monte Carlo method
function xforc = simulate_var_roll_1step(estMdl,var_info,trdata,...
    h_step,nfuture)
cnum = numel(var_info.cnames);
endog = create_endogvars(trdata.endodata,trdata.gvdata,...
    var_info.endoglist,var_info.cnames);

Klist = arrayfun(@(x) numel(var_info.endoglist.(var_info.cnames{x})),1:cnum);
K = sum(Klist(:)); Klist_ext = [0 Klist];
xforc = zeros(K,1,nfuture);

for i = 1:cnum
    y0 = endog.(var_info.cnames{i});
    tmp = simulate(estMdl.(var_info.cnames{i}),h_step,...
        'NumPaths',nfuture,'Y0',y0);
    tmp = permute(tmp,[2 1 3]);
    xforc(sum(Klist_ext(1:i))+1:sum(Klist_ext(1:i+1)),1,:) = tmp(:,end,:);
end
end