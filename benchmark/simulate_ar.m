function xsim = simulate_ar(estMdl,lag,var_info,trdata,tsdata,varargin)
% function xsim = simulate_ar(estMdl,lag,var_info,trdata,tsdata,varargin)
% -------------------------------------------------------
% >>> Procedure
%
% 1. [Parameter Uncertainty] In-sample simulation: simulate a training
% set using precomputed model parameters
% 2. [Parameter Uncertainty] Paramter re-estimation: re-estimate (MLE) model
% paramters using the simulated training set
% 3. [Future Uncertainty] h-step ahead forecasting with bootstrapped
% residuals (eta)
% -------------------------------------------------------
% >>> Required Input
%
% - estMdl: struct, fieldnames = cnames, field = varm estimation
% - lag
% - trdata: struct, training data
%   - endodata: struct, fieldnames = endonames, field = [ntime cnum] 
%   numeric matrix
%   - gvdata: struct, fieldnames = gvnames, field = [ntime 1] numeric
% matrix
%   - wdata: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info: struct, fieldnames = {'start', 'end', 'freq'}
% - tsdata: struct, testing data
%   - endodata_test: struct, fieldnames = endonames, field = [ntime cnum]
%   numeric matrix
%   - gvdata_test: struct, fieldnames = gvnames, field = [ntime 1] 
%   numeric matrix
%   - wdata_test: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info_test: struct, fieldnames = {'start', 'end', 'freq'}
% - var_info: struct, variable information
%   - cnames: 1d char array, name of countries
%   - endonames: 1d char array, names of endogenous variable and foreign
%   variable
%   - gvnames: 1d char array, names of global (strong+weak) variable
%   - gnvnames: 1d char array, names of global (weak) variable
%   - endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
%   - gvflag: [cnum gvnum] numeirc matrix, 0=not included, 1=included as
%   exogenous, 2=included as endogenous
% -------------------------------------------------------
% >>> Optional Input
%
% - nparams: numeric, # of iterations for parameter uncertainty, default =
% 10
% - nfuture: numeric, # of iterations for future uncertainty, default = 50
% - fhorz: numeric, forecasting horizons, default = 1
% -------------------------------------------------------
% >>> Output
%
% - xsim: 3d numeric matrix, forecasting results, 
% [nvar fhorz nparams*nfuture]

%% Get optional input
nargs = -nargin('simulate_ar') - 1;
% >>> simulation options
nparams = 10; % # of iterations for parameter uncertainty, 0 == no parameter uncertainty
nfuture = 50; % # of iterations for future uncertainty, 0 == no future uncertainty
fhorz = 1;

if nargin >= nargs + 1, nparams = varargin{1}; end
if nargin >= nargs + 2, nfuture = varargin{2}; end
if nargin >= nargs + 3, fhorz = varargin{3}; end

%% Input validation
err_msg = [];
err_msg.dont_run = 'Finish without execution.';
err_msg.invalid_input = 'Invalid Input.';
err_msg.compute_error = 'Computational error.';

if nparams<0 || nfuture<0, error(err_msg.invalid_input); end
if nparams==0 && nfuture==0, error(err_msg.dont_run); end
if fhorz<0, error(err_msg.invalid_input); end
if nfuture>1 && fhorz==0, error(err_msg.invalid_input); end

%% Input formatting
nparams = nparams + 1;
cnum = numel(var_info.cnames);

train_real = struct();
[train_real.endog,train_real.endoglist,train_real.exog,...
    train_real.exoglist,train_real.exognx,train_real.exognxlist,...
    train_real.wm_t,train_real.foreign,train_real.X] = ...
    gen_params(var_info.cnames,trdata.date_info,var_info.endonames,...
    var_info.gvnames,var_info.gnvnames,var_info.endoflag,...
    var_info.gvflag,trdata.endodata,trdata.gvdata,trdata.wdata);

test_real = struct();
[test_real.endog,test_real.endoglist,test_real.exog,...
    test_real.exoglist,test_real.exognx,test_real.exognxlist,...
    test_real.wm_t,test_real.foreign,test_real.X] = ...
    gen_params(var_info.cnames,tsdata.date_info_test,var_info.endonames,...
    var_info.gvnames,var_info.gnvnames,var_info.endoflag,var_info.gvflag,...
    tsdata.endodata_test,tsdata.gvdata_test,tsdata.wdata_test);

Klist = arrayfun(@(x) numel(train_real.endoglist.(var_info.cnames{x})),...
    1:cnum);
Klist_ext = [0 Klist];
K = sum(Klist(:));
xsim = zeros(K,fhorz,nparams*nfuture);
%%  
% Step 0. Get burnin-set
nsim = trdata.obs_train - lag;
burnin = train_real.X(:,1:lag);

for iter = 1:nparams
    if iter > 1 % parameter uncertainty + future uncertainty        
        % Step 1. In-sample simulation of training set
        simdata = zeros(K,trdata.obs_train);
        for i = 1:K
            tmp_sim = forecast(estMdl{i},burnin(i,:)',nsim);
            simdata(i,1:lag) = burnin(i,:);
            simdata(i,lag+1:end) = tmp_sim;
        end
        trdata_sim = get_data_train_sim(var_info,trdata,simdata);
        
        % Step 2. Re-estimate model parameters using the simulated training
        % set
        estMdl_sim = train_ar(var_info,trdata_sim,lag);
        
        % Step 3. h-step ahead forecasting with bootstrapped residuals (eta)
        for i = 1:K
            for jter = 1:nfuture
                tmp_sim = forecast(estMdl_sim{i},train_real.X(i,:)',fhorz);
                xsim(i,:,(iter-1)*nfuture+jter) = tmp_sim;
            end
        end
        
    else % no parameter uncertainty, just future uncertainty
        % Step 3. h-step ahead forecasting with bootstrapped residuals (eta)
        for i = 1:K
            for jter = 1:nfuture
                tmp_sim = forecast(estMdl{i},train_real.X(i,:)',fhorz);
                xsim(i,:,(iter-1)*nfuture+jter) = tmp_sim;
            end
        end
    end
end
    
Xvar = sum(abs(xsim(:)));
if Xvar==0, error('CALCULATION FAILED: empty value in xsim.'); end

end

%%
function trdata_sim = get_data_train_sim(var_info,trdata,simdata)
trdata_sim = trdata;
for i = 1:numel(var_info.endonames)
    trdata_sim.endodata.(var_info.endonames{i}) = ...
        zeros(trdata.obs_train,0);
end
trdata_sim.gvdata = struct();
for i = 1:numel(var_info.gnvnames)
    trdata_sim.gvdata.(var_info.gnvnames{i}) = zeros(trdata.obs_train,0);
end

[~,endoglist] = gen_params(var_info.cnames,trdata.date_info,...
    var_info.endonames,var_info.gvnames,var_info.gnvnames,...
    var_info.endoflag,var_info.gvflag,trdata.endodata,trdata.gvdata,...
    trdata.wdata);

Ktype = [];
for i = 1:numel(var_info.cnames)
    Ktype = [Ktype endoglist.(var_info.cnames{i})];
end
K = numel(Ktype);

for i = 1:K
    if ismember(Ktype{i},var_info.endonames)
        trdata_sim.endodata.(Ktype{i}) = [trdata_sim.endodata.(Ktype{i}) ...
            simdata(i,:)'];
    elseif ismember(Ktype{i},var_info.gnvnames)
        trdata_sim.gvdata.(Ktype{i}) = [trdata_sim.gvdata.(Ktype{i}) ...
            simdata(i,:)'];
    end
end

end