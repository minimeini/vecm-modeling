function yest = naive(yreal, fhorz, type, varargin)
% function yest = naive(yreal, fhorz, type, varargin)
%
% [simple] Naive:   Yhat_{t+1} = Y_t
% [trend] Naive:    Yhat_{t+1} = Y_t + (Y_t - Y_{t-1})
% [lag] Naive: Yhat_{t+1} = Y_{t-k}

if ~ismember(type, {'simple', 'trend', 'lag'})
    error('type = {simple, trend, lag}');
end

if strcmp(type, 'lag')
    if nargin <= 3
        error('Insufficient parameters.');
    end
    lag = varargin{1};
elseif strcmp(type, 'simple')
    lag = 1;
elseif strcmp(type, 'trend')
    lag = 2;
end

if nargin <= 4
    resid_rand = [];
else
    resid_rand = varargin{2};
end

if size(yreal, 2) < fhorz + lag - 1
    error('Insufficient real future data');
elseif size(yreal, 2) > fhorz + lag -1
    yreal = yreal(:, (end-fhorz-lag+2):end);
end


K = size(yreal, 1);
yest = zeros(K, fhorz);
if isempty(resid_rand)
    resid_rand = zeros(size(yest));
end

idx = (1:size(yreal, 2)) - lag;
start = find(idx == 1);
for i = 1:fhorz
    switch type
        case 'simple'
            yest(:,i) = yreal(:,(start-1)) + resid_rand(:, i);
        case 'trend'
            yest(:,i) = 2 * yreal(:,(start-1)) - yreal(:,(start-2)) + ...
                resid_rand(:, i);
        case 'lag'
            yest(:,i) = yreal(:,(start-lag)) + resid_rand(:, i);
    end
    start = start + 1;
end
end