function [train_data,test_data,cnames,freq] = split_data(...
    tr_start,tr_end,obs_test,endonames,gvnames,dataPath,varargin)
% function [train_data,test_data,cnames] = split_data(...
%     tr_start,tr_end,obs_test,endonames,gvnames,var_status,dataPath,varargin)
%
% >>>> Required Input <<<<
%
% - tr_start: start date of training, '1995Q1' for quarterly data, '1995'
% for yearly data, '1995M01' for monthly data (string)
%   -- str2double schema
%       ---- quarterly data: 1995.25 for 1995Q1, 1995.50 for 1995Q2, 1995.75 for
%       1995Q3, 1996.00 for 1995Q4
%       ---- monthly data: base=1995, 1995M01=1, 1996M02=12+2
%       ---- yearly data: base=1995, 1995=1, 1996=2
% - tr_end: end date of training (string)
% - obs_test: length of forecasting horizon (integer)
% - endonames: names of endogenous variables (cell string)
% - gvnames: names of global variables (weak & strong, cell string)
% - dataPath: folder of all the data/variables (string)
%
%
% >>>> Optional Input <<<<
% - varnames_we: names of weak exogenous variables (cell string)
% - weightPath: location of the csv file for 2D weights (string)
% - datePath: location of the csv file of date (string)
% - cityPath: location of the csv file of city (string)

%% Preparation
nargs = -nargin('split_data')-1;
varnames_we = []; weightPath = []; datePath = []; cityPath = [];
if nargin >= nargs + 1, varnames_we = varargin{1}; end
if nargin >= nargs + 2, weightPath = varargin{2}; end
if nargin >= nargs + 3, datePath = varargin{3}; end
if nargin >= nargs + 4, cityPath = varargin{4}; end

if ~iscell(endonames)||~iscell(gvnames)...
        ||(~isempty(varnames_we)&&~iscell(varnames_we))
    error('INPUT ERROR: Variable names should be in the cell array.');
end
if ~isnumeric(obs_test),error('INPUT ERROR: obs_test is an integer'); end
if ~ischar(dataPath)...
        ||(~isempty(weightPath)&&~ischar(weightPath))...
        ||(~isempty(datePath)&&~ischar(datePath))...
        ||(~isempty(cityPath)&&~ischar(cityPath))
    error('INPUT ERROR: file path should be a string.');
end

%% prepare information data
if contains(tr_start, 'Q') && numel(tr_start)==6 && numel(tr_end)==6
    freq = 4; freq_type = 'Q';
    tr_start = str2double(tr_start(1:4)) + str2double(tr_start(end))/4;
    tr_end = str2double(tr_end(1:4)) + str2double(tr_end(end))/4;
    if tr_end < tr_start, error('CALCULATION ERROR'); end
    obs_train = (tr_end - tr_start)/0.25 + 1;
    
elseif contains(tr_start, 'M') && numel(tr_start)==7 && numel(tr_end)==7
    freq = 12; freq_type = 'M';
    base_year = str2double(tr_start(1:4));
    tr_start = str2double(tr_start(6:end));
    tr_end = (str2double(tr_end(1:4))-base_year)*12 + ...
        str2double(tr_end(6:end));
    if tr_end < tr_start, error('CALCULATION ERROR'); end
    obs_train = tr_end - tr_start + 1;
    
elseif numel(tr_start)==4 && numel(tr_end)==4
    freq = 1; freq_type = 'Y';
    tr_start = str2double(tr_start);
    tr_end = str2double(tr_end);
    if tr_end < tr_start, error('CALCULATION ERROR'); end
    obs_train = tr_end - tr_start + 1;
    
else
    error(['INPUT ERROR: ''1995Q1'' for quarterly data,'...
        ' ''1995'' for yearly data,'...
        ' ''1995M01'' for monthly data (string)']);
end

start_pos = 1; end_pos = start_pos + obs_train - 1;

if ~isempty(cityPath)
    cty_list = readtable(cityPath,'Delimiter',',','ReadVariableNames',1);
    cnames = cty_list.cty_short';
else
    cnames = [];
end

%% generate training and testing set
train_data = struct();
test_data = struct();

[train_data.endo,train_data.gv,train_data.weight,train_data.date] = ...
    getData(endonames,gvnames,dataPath,datePath,weightPath,...
    start_pos,end_pos);
[test_data.endo,test_data.gv,test_data.weight,test_data.date] = ...
    getData(endonames,gvnames,dataPath,datePath,weightPath, ...
    end_pos+1, end_pos+obs_test);

end