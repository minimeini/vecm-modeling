%% initialization
clear all
wd = 'C:\Users\meini\Dropbox\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';

wd_data = fullfile(wd, 'data');
wd_vis = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd_data);

%% index info about splitting
train_pos.startpos = 1; train_pos.startdate = '1993Q1';
train_pos.endpos = 72; train_pos.enddate = '2010Q4';

test_pos.startpos = 73; test_pos.startdate = '2011Q1';
test_pos.endpos = 96; test_pos.enddate = '2016Q4';

fhorz = test_pos.endpos - train_pos.endpos;
%% generate information for training

% dumnames = {'dum95c','dum00c','dum03s','dum05c','dum08c'};
dumnames = [];
gnvnames = {'logoilp'};

gvnames = [gnvnames dumnames];
endonames = {'logtexp','logtimp','logGDP','logownp'};
varnames = [endonames gvnames];

ctyPath = fullfile(wd_data, 'info', 'ctylist.csv');
fileID = fopen(ctyPath, 'r');
cnames = textscan(fileID, '%s');
fclose(fileID);
cnames = cnames{1};

obs_train = train_pos.endpos - train_pos.startpos + 1;
obs_test = test_pos.endpos - test_pos.startpos + 1;

%%
gnvflag = ismember(gvnames, gnvnames);
gnvloc = find(gnvflag==1);

endoflag = zeros(numel(cnames), numel(endonames));
gvflag = zeros(numel(cnames), numel(gvnames));

endoflag = endoflag + 1;
gvflag = gvflag + 1;
gvflag(end, gnvloc) = 2;

save(fullfile(wd_data, 'parse', 'split_info.mat'), ...
    'train_pos', 'test_pos', 'fhorz', 'endoflag', 'gvflag', ...
    'cnames', 'endonames', 'gvnames', 'gnvnames');
%% train/test splitting
dataPath = fullfile(wd_data, 'working');
datePath = fullfile(wd_data, 'info', 'date_info.csv');
weightPath = fullfile(wd_data, 'trade_weight', ...
    'weights2D_extended_xcld.csv');

[endodata, gvdata, wdata, date_info] = getData(endonames, gvnames, ...
    dataPath, datePath, weightPath, train_pos.startpos, train_pos.endpos);
save(fullfile(wd_data, 'parse', 'train_data.mat'), 'obs_train', ...
    'date_info', 'endodata', 'gvdata', 'wdata');

[endodata_test, gvdata_test, wdata_test, date_info_test] = ...
    getData(endonames, gvnames, dataPath, datePath, weightPath, ...
    test_pos.startpos, test_pos.endpos);
save(fullfile(wd_data, 'parse', 'test_data.mat'), 'obs_test', ...
    'date_info_test', 'endodata_test', 'gvdata_test', 'wdata_test');

