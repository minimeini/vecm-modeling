%% Initialization
clear all

wd = 'C:\Users\meini\OneDrive\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';

wd_data = fullfile(wd, 'data');
wd_vis = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd_code);

load(fullfile(wd_data, 'parse', 'train_data.mat'));
load(fullfile(wd_data, 'parse', 'test_data.mat'));
load(fullfile(wd_data, 'parse', 'split_info.mat'));

cnames = cnames_info.short;

%% find change points shared by all countries over all variables
% Procedure
% ---- 1. Find the change points of every variable shared by all countries using
% group fused Lasso
% ---- 2. Score the importance of change points over all countries and all
% ---- 3. Add user-defined change points
% variables and thus make selection

% selected variables
fm = {'logtexp', 'logGDP'};
figlab = 'endo2';

savepath = fullfile(wd_vis, 'change_pnts', ['shared_cpnts_' figlab]);
checkPath(savepath);

% pos: Q1 = 0.25, Q2 = 0.5, Q3 = 0.75, Q4 = 0 (pos + 1)
timeline = 1993:0.25:2011 + 0.25; 
timeline = round(timeline(1:end-1), 2);

ticklen = 20;
ticknum = 1:ticklen:numel(timeline);
ticklab = arrayfun(@(x) num2str(timeline(ticknum(x))), 1:numel(ticknum), ...
    'UniformOutput', false);

% user-defined dummies
cpnts = [];
cpnts(1).user_date = [1994.75 1997 1998.5 2000.75 2001.75 2005.5 2008 2009]; % logtexp
cpnts(2).user_date = [1994.75 1998.5 2000.75 2001.75 2003.25 2005.5 2008 2009]; % logGDP

% user-defined exclusion
cpnts(1).user_excld = [1995.75 1998.75 2010.75]; % logtexp
cpnts(2).user_excld = [1999.75 2003.5 2006.5 2010.75]; % logGDP

for i = 1:length(fm)
    % find the position of user-defined dummies
    num_usrd = numel(cpnts(i).user_date);
    cpnts(i).user_pos = zeros(1, num_usrd);
    for j = 1:num_usrd
        cpnts(i).user_pos(j) = find(timeline==cpnts(i).user_date(j));
    end
    
    % find the position of user-defined exclusion
    num_excld = numel(cpnts(i).user_excld);
    for j = 1:num_excld
        cpnts(i).user_excld(j) = find(timeline==cpnts(i).user_excld(j));
    end
    
    % dummies detected by group fused lasso
    data = endodata.(fm{i}); % nobs * ncty
    seg = simpleGFL(data);
    cpnts(i).gfl_pos = seg.jumps';
    
    % full dummies = user-defined dummies + dummies detected by GFL
    [cpnts(i).pos, ord] = unique([cpnts(i).gfl_pos cpnts(i).user_pos]);
    cpnts(i).pos_lab = [timeline(cpnts(i).gfl_pos) ...
        -timeline(cpnts(i).user_pos)];
    cpnts(i).pos_lab = cpnts(i).pos_lab(ord);
    
    % delete user-defined exclusion
    [cpnts(i).pos, idx_del] = setdiff(cpnts(i).pos, ...
        cpnts(i).user_excld, 'stable');
    cpnts(i).pos_lab = cpnts(i).pos_lab(idx_del);
    cpnts(i).name = fm{i};
    
    fprintf('Change points of %s is: ', fm{i});
    fprintf('%d ', cpnts(i).pos);
    fprintf('\n');
    
    nplt = size(data,2); % nplt == ncty
    f = figure;
    f.Units = 'normalized';
    f.OuterPosition = [0 0 1 1];
    for j = 1:nplt % for each country
%         seg_cty = simpleGFL(data(:,j));
%         cpnts_pos = seg_cty.jumps;
        
        subplot(5, 5, j);
        hold on
        plot(squeeze(data(:,j)));
        ax = gca;
        for k = 1:length(cpnts(i).pos)
            plot([cpnts(i).pos(k) cpnts(i).pos(k)], ...
                [ax.YLim(1) ax.YLim(2)], ...
                'LineStyle', '--', 'Color', [0.7 0 0]);
        end
        hold off
        title(cnames{j});
        xticks(ticknum-1);
        xticklabels(ticklab);
    end
    suptitle([fm{i} ': ' num2str(cpnts(i).pos_lab)]);
    
    print(f, fullfile(savepath, [fm{i} '_cpts_' figlab '.png']), '-dpng');
%     close all
%     delete(f);
%     delete(gcf);

end
save(fullfile(wd_data, 'change_points', [...
    'shared_cpnts_' figlab '.mat']), 'cpnts');
