%% initialization
clear all

wd = 'C:\Users\meini\OneDrive\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';

wd_data = fullfile(wd, 'data');
wd_vis = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd_code);

load(fullfile(wd_data, 'parse', 'train_data.mat'));
load(fullfile(wd_data, 'parse', 'test_data.mat'));
load(fullfile(wd_data, 'parse', 'split_info.mat'));

cnames = cnames_info.short';
vnames = var_info.vnames';
gvnames = var_info.gvnames';

cnum = numel(cnames);
vnum = numel(vnames);
freq = date_info.freq;

%% Defined shared change points across all variables and all countries
% Method
% 1. knowledge about financial events
% ---- 1995 Financial Crisis: 1994Q3, 1995Q4
% ---- Asian Financial Crisis: 1997Q2, 1998Q4
% ---- SARS: 2003Q2, 2004Q4
% ---- Sub-prime Housing Crisis and Global Recession: 2008Q1, 2009Q1
% 2. subjective indication from logtexp, logGDP( and logownp)
% 3. change point detection by group fused LASSO for endogenous variables
% over all countries
load(fullfile(wd_data,'change_points','shared_cpnts_endo2.mat'));
cpnts = union(cpnts(1).pos,cpnts(2).pos);

cpnts_split = struct();
cpnts_split(1).pos = cpnts(1);
cpnts_split(1).label = '1994.75 for 1995 crisis';
cpnts_split(2).pos = cpnts([2 3]);
cpnts_split(2).label = '1997 Crisis';
cpnts_split(3).pos = cpnts([4 5]);
cpnts_split(3).label = '2000 Crisis';
cpnts_split(4).pos = cpnts(6);
cpnts_split(4).label = '2003 SARS';
cpnts_split(5).pos = cpnts(7);
cpnts_split(5).label = '2005 Crisis';
cpnts_split(6).pos = cpnts([8 9]);
cpnts_split(6).label = '2008 Crisis';
%% create dummy variables
% intervention dummy
mat_cpts = zeros(obs_train+obs_test, size(cpnts,1));
mat_cpts(cpnts,1) = 1;

dlmwrite(fullfile(wd_data, 'working', 'interventionDummy.csv'), ...
    mat_cpts, 'delimiter', ',');

% % intervention dummy times trend
% trend = 1:size(mat_cpts, 1);
% mat_cptstr = size(mat_cpts);
% mat_cptstr = mat_cpts .* repmat(trend', 1, size(mat_cpts, 2));

% dlmwrite(fullfile(wd_data, 'working', 'interventionDummy_trend.csv'), ...
%     mat_cptstr, 'delimiter', ',');

% intervention dummy categorized by events
mat_cpts_cat = zeros(obs_train+obs_test, numel(cpnts_split));
for i = 1:numel(cpnts_split)
    mat_cpts_cat(cpnts_split(i).pos,i) = 1;
end
dlmwrite(fullfile(wd_data,'working','interventionDummyCat.csv'),...
    mat_cpts_cat,'delimiter',',');