function [varinfo,inputinfo] = gen_traininfo(varnames, ...
    var_status, cnames, var_we)
%% check input
err_datatype = 'Invalid data type.';
err_length = 'Variables should have the same length.';
err_we = 'Weakly exogeneous should be included in `var_short`';

if ~iscell(varnames), error(err_datatype); end
if ~isnumeric(var_status), error(err_datatype); end
if ~iscell(cnames), error(err_datatype); end
if ~isempty(var_we)&&~iscell(var_we), error(err_datatype); end

len_check = [numel(varnames) numel(var_status)];
if any(len_check~=len_check(1)), error(err_length); end
if numel(var_we) > numel(varnames), error(err_we); end
if ~isempty(var_we) &&...
        sum(arrayfun(@(x) ismember(var_we{x},varnames),...
        1:numel(var_we))) < numel(var_we)
    error(err_we);
end

if size(varnames,1) ~= 1, varnames = varnames'; end
if size(var_status,1) ~= 1, var_status = var_status'; end
if size(cnames,1) ~= 1, cnames = cnames'; end
if ~isempty(var_we)&&size(var_we, 1) ~= 1, var_we = var_we'; end

%% generate variable information
varinfo = array2table(varnames', 'VariableNames', {'varnames'});
varinfo.STATUS = var_status';

%% generate specification of country-specified variables
varlist = [varnames(var_status==1) ...
    arrayfun(@(x) [varnames{x} 's'], find(var_status==1), ...
    'UniformOutput', 0) varnames(var_status==2)];

vnum = numel(varlist);
cnum = numel(cnames);

varlog = ones(cnum, vnum);
for i = 1:numel(var_we)
    if ~isempty(var_we)
        varlog(end, ismember(varlist, var_we{i})) = 2;
    end
end

inputinfo = array2table(cnames', 'VariableNames', {'cnames'});
for i = 1:numel(varlist)
    inputinfo.(varlist{i}) = varlog(:,i);
end

end