library(imputeTS)
options(warn=0)

wd_data = 'C:/vecmModeling/data'
filepath = paste(wd_data, 'trim_extract', 'rawvar_trim_GDP.csv', sep='/')
data = read.csv(filepath, header=TRUE)
print(colSums(is.na(data)))

nacty = colnames(data)[as.logical(colSums(is.na(data)))]
for (cty in nacty) {
  tmp_ts = ts(data[,`cty`], start=c(1993,1), end=c(2016,4), frequency=4)
  tmp_new = tryCatch({
      na.seadec(tmp_ts,algorithm='kalman')
    }, warning=function(w) {
      na.seadec(tmp_ts,algorithm='interpolation')
    })
  data[,`cty`] = tmp_new
}

savepath = paste(wd_data, 'impute', 'rawvar_trim_impute_GDP.csv', sep='/')
write.csv(data, savepath, row.names=FALSE)
