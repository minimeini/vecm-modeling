%% Initialization
wd = 'C:\Users\meini\OneDrive\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';
wd_data = [wd '\data'];
wd_vis = [wd '\figure'];
cd(wd_data);

err_calc = 'Unexpected calculation error.';
%% calculate bilateral trade link
% col = reporting countries, row = partner countries
weights_exp = load(fullfile(wd_data, 'trade_weight', ...
    'weights_extended_exp.mat'));
weights_imp = load(fullfile(wd_data, 'trade_weight', ...
    'weights_extended_imp.mat'));
weights_exp = weights_exp.weights; weights_exp = weights_exp(:,:,1:24);
weights_imp = weights_imp.weights; weights_imp = weights_imp(:,:,1:24);

% all missing values are treated as zero
weights_exp(isnan(weights_exp)) = 0;
weights_imp(isnan(weights_imp)) = 0;

weights = (abs(weights_exp) + abs(weights_imp)) ./ 2;
for i = 1:24
    tmp = weights(:,:,i);
    sm = sum(tmp, 1);
    tmp = nanmax(tmp ./ sm, 0);
    
    for j = 1:size(tmp, 2)
        if sum(tmp(:, j)) == 0
            tmp(:, j) = 1 / (size(tmp,1) - 1);
            tmp(j, j) = 0;
        end
    end
    
    weights(:,:,i) = tmp;
end

tmp = permute(weights, [2 1 3]);
weights2D = reshape(tmp, size(tmp,1), [], 1);
weights2D = permute(weights2D, [2 1 3]);

dlmwrite(fullfile(wd_data, 'trade_weight', 'weights2D_extended.csv'), weights2D);
save(fullfile(wd_data, 'trade_weight', 'weights_extended.mat'), 'weights');
clear weights weights2D weights_exp weights_imp tmp
%% calculate tourism imports and exports
folder = fullfile(wd_data, 'trim_cty');

load(fullfile(folder, 'weights_extended_xcld.mat')); % `weights`
bpm6_imp = xlsread(fullfile(folder, 'trim_cty_tim6.csv'));
bpm6_exp = xlsread(fullfile(folder, 'trim_cty_tex6.csv'));

ntime = size(bpm6_imp, 1);
ncty = size(bpm6_imp, 2);

[exr, strs] = xlsread(fullfile(folder, 'trim_cty_exrate.csv'));
cnames_short = strs(1, 2:end);
times = strs(2:end,1);
years = arrayfun(@(x) str2double(times{x}(1:4)), 1:ntime);
years_idx = years - years(1) + 1;

exr_ref = exr(arrayfun(@(x) (contains(times(x), '2010Q')), ...
    1:length(times)),:);
exr_ref = mean(exr_ref, 1);

cpi = xlsread(fullfile(folder, 'trim_cty_CPI.csv'));

timp = zeros(size(bpm6_imp));
texp = zeros(size(bpm6_exp));
ownp = zeros(size(bpm6_exp));
for t = 1:ntime
    for c = 1:ncty
        tmp = sum(weights(:,c,years_idx(t))' .* cpi(t,:) .* exr(t,:));
        if tmp<=0, error(err_calc); end
        
        timp(t,c) = (bpm6_imp(t,c) / tmp) * (exr_ref(c) / exr(t,c));
        if timp(t,c)<=0, error(err_calc); end
        
        ownp(t,c) = cpi(t,c) * exr(t,c);
        if ownp(t,c)<=0, error(err_calc); end
        
        texp(t,c) = bpm6_exp(t,c) / ownp(t,c) * exr_ref(c);
        if texp(t,c)<=0, error(err_calc); end
    end
end

if ~isreal(log(timp)) || ~isreal(timp), error(err_calc); end
if ~isreal(log(texp)) || ~isreal(texp), error(err_calc); end
if ~isreal(log(ownp)) || ~isreal(ownp), error(err_calc); end

timp = real(timp);
texp = real(texp);
ownp = real(ownp);

dlmwrite(fullfile(wd_data, 'calcvar', 'timp_noseas.csv'), timp);
dlmwrite(fullfile(wd_data, 'calcvar', 'texp_noseas.csv'), texp);
dlmwrite(fullfile(wd_data, 'calcvar', 'ownp_noseas.csv'), ownp);
