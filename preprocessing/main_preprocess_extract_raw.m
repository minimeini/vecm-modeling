%% Initialization
wd = 'C:\Users\meini\OneDrive\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';
wd_data = [wd '\data'];
wd_vis = [wd '\figure'];
cd(wd);

if exist(wd_data, 'dir') ~= 7
    mkdir(wd_data)
end
if exist(wd_vis, 'dir') ~= 7
    mkdir(wd_vis)
end

%% Manipulation Info
cnames_delete = {'Argentina'};
times_start = '1993Q1';

rfname = 'globe_extended.xlsx';
datapath = fullfile(wd_data, 'raw', rfname);
[status, sheets, xlFormat] = xlsfinfo(datapath);
disp(sheets);

%% Generate Countries Info
cnames_all = readtable([wd_data '\raw\scrapped_out.csv'], ...
    'ReadVariableNames', true);
cnames_long = {'Australia', 'Belgium', 'Brazil', 'Canada', 'China', ...
    'France', 'Germany', 'Greece', 'India', 'Italy', 'Japan', 'Korea', ...
    'Mexico', 'Netherlands', 'New Zealand', 'Norway', 'Philippines', ...
    'Portugal', 'Singapore', 'Slovenia', 'South Africa', 'Spain', ...
    'Sweden', 'Thailand', 'United Kingdom', 'United States of America'};
cnames_long = table(cnames_long');
cnames_long.Properties.VariableNames = ...
    cnames_all.Properties.VariableNames(1);

[~, ia, ib] = intersect(cnames_all(:,1), cnames_long);
suspects = cnames_long(setdiff(1:size(cnames_long,1),ib),1);
disp(suspects);
kwd = {'Korea', 'United States'};

for idx = 1:length(kwd)   
    candidates = cnames_all(contains(cnames_all.cty_long, kwd{idx}), 1);
    disp(candidates);
    select = input('Select a candidate:');
    newidx = find(strcmp(cnames_all.cty_long, ...
        char(candidates{select,1})), 1);
    ia = [ia; newidx];
end
ia = sort(ia);

cnames_short = cnames_all(ia, 2);
creflist = [cnames_long, cnames_short];
writetable(creflist, fullfile(wd_data, 'raw', 'cty_abbr.csv'));

cnames_short = table2array(cnames_short);
%%
if exist(fullfile(wd_data, 'full_extract'), 'dir') ~= 7
    mkdir(fullfile(wd_data, 'full_extract'))
end

if exist(fullfile(wd_data, 'trim_extract'), 'dir') ~= 7
    mkdir(fullfile(wd_data, 'trim_extract'))
end

%% travel exports and imports BMP6: D6 - EC32
varnames = {'tex6', 'tim6'};
for i = 1:2
    [data, cnames] = xlsread(datapath, sheets{i}, 'B4:EC32');
    times = cnames(1, 4:end);
    for j = 1:numel(times)
        times{j} = [times{j}(4:end) times{j}(1:2)];
    end
    nocty = find(strcmp(cnames(3:end,2), cnames_delete{1}));
    data = data(setdiff(1:size(data,1), nocty), 1:end)';
    data = array2table(data, 'VariableNames', cnames_short);
    data.time = times';
    data = data(:, [size(data,2) 1:(size(data,2)-1)]);
    
    writetable(data, fullfile(wd_data, 'full_extract', ...
        ['rawvar_' varnames{i} '.csv']), 'Delimiter', ',');

    writetable(data(find(strcmp(times, '1993Q1')):end, :), ...
        fullfile(wd_data, 'trim_extract', ...
        ['rawvar_trim_' varnames{i} '.csv']), ...
        'Delimiter', ',');
end

%% travel exports and imports BMP5
varnames = {'tex5', 'tim5'};
for i = 3:4
    [data, cnames] = xlsread(datapath, sheets{i}, 'A2:AD261');
    times = cnames(1:end, 1);
    for j = 1:numel(times)
        times{j} = strrep(times{j}, 'q', 'Q');
    end
    times = times((end-203):(end-1));
    
    bels = data(:, 3:4);
    belrep = bels(~isnan(bels(:,2)), 2);
    idx_end = find(~isnan(bels(:, 1)), 1);
    idx_start = idx_end - numel(belrep) + 1;
    data(idx_start:idx_end, 3) = belrep;
    
    data = data(:, [2:3 6:end]);
    data = array2table(data, 'VariableNames', cnames_short);
    data.time = times;
    data = data(:, [size(data,2) 1:(size(data,2)-1)]);
    
    writetable(data, fullfile(wd_data, 'full_extract', ...
        ['rawvar_' varnames{i-2} '.csv']), 'Delimiter', ',');
    writetable(data(find(strcmp(times, '1993Q1')):end, :), ...
        fullfile(wd_data, 'trim_extract', ...
        ['rawvar_trim_' varnames{i-2} '.csv']), ...
        'Delimiter', ',');
end

%% GDP: F4 - EC31
[data, cnames] = xlsread(datapath, 'GDP', 'F4:EC31');
[~, satemp] = xlsread(datapath, 'GDP', 'B6:B31');

seasAdjust = zeros(numel(satemp), 1);
for i = 1:numel(satemp)
    seasAdjust(i) = strcmp(satemp{i}, 'Y');
end

times = cnames(1, :);
for i = 1:numel(times)
    times{i} = [times{i}(4:end) times{i}(1:2)];
end
times = times';
data = data';

data = array2table(data, 'VariableNames', cnames_short);
data.time = times;
data = data(:, [size(data,2) 1:(size(data,2)-1)]);
    
writetable(data, fullfile(wd_data, 'full_extract', ...
    ['rawvar_GDP.csv']), 'Delimiter', ',');
writetable(data(find(strcmp(times, '1993Q1')):end, :), ...
    fullfile(wd_data, 'trim_extract', ['rawvar_trim_GDP.csv']), ...
    'Delimiter', ',');
dlmwrite(fullfile(wd_data, 'log', 'GDP_orig_seasAdjust.csv'), ...
    seasAdjust, ',');

%% CPI and Exchange Rate
varnames = {'CPI', 'exrate'};
for i = 7:8
    [data, cnames] = xlsread(datapath, sheets{i}, 'A2:AA241');
    times = cnames(:, 1);
    for j = 1:numel(times)
        times{j} = [times{j}(4:end) times{j}(1:2)];
    end
    
    data = array2table(data, 'VariableNames', cnames_short);
    data.time = times;
    data = data(:, [size(data,2) 1:(size(data,2)-1)]);
    
    writetable(data, fullfile(wd_data, 'full_extract', ...
        ['rawvar_' varnames{i-6} '.csv']), 'Delimiter', ',');
    writetable(data(find(strcmp(times, '1993Q1')):end, :), ...
        fullfile(wd_data, 'trim_extract', ...
        ['rawvar_trim_' varnames{i-6} '.csv']), ...
        'Delimiter', ',');
end

%% crude oil price
[data, times] = xlsread(datapath, sheets{end}, 'D5:EA7');
for i = 1:numel(times)
    times{i} = [times{i}(4:end) times{i}(1:2)];
end
times = times';
data = data';

data = array2table(data, 'VariableNames', {'oilprc'});
data.time = times;
data = data(:, [size(data,2) 1:(size(data,2)-1)]);
    
writetable(data, fullfile(wd_data, 'full_extract', ...
    ['rawvar_oilprc.csv']), 'Delimiter', ',');
writetable(data(find(strcmp(times, '1993Q1')):end, :), ...
    fullfile(wd_data, 'trim_extract', ['rawvar_trim_oilprc.csv']), ...
    'Delimiter', ',');

%% bilateral trade matrix
data = load(fullfile(wd_data, 'trade_weight', 'weights.mat'));
weights = data.weights;