%% Initialization
wd = 'C:\Users\meini\OneDrive\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';
wd_data = [wd '\data'];
wd_vis = [wd '\figure'];
cd(wd);

%% delete specific countries
del_cty = 'GRC';
[~, ~, filelist] = getFilename(fullfile(wd_data, 'trim_extract'), '*.csv');
filelist = filelist([1 3 6]); % CPI, exrate, oilprice
[~, ~, flisttmp] = getFilename(fullfile(wd_data, 'impute'), '*.csv');
filelist = [filelist flisttmp];
checkPath(fullfile(wd_data, 'trim_cty'));

for i = 1:numel(filelist)
    fname = split(filelist{i}, '_');
    fname = fname{end};
    [data, coln] = xlsread(filelist{i});
    ctys = coln(1,2:end);
    del_idx = find(contains(ctys, del_cty));
    
    data = data(:,setdiff(1:size(data,2), del_idx));
    coln = coln(:,setdiff(1:size(coln,2), del_idx+1));
    
    data = array2table(data, 'VariableNames', coln(1,2:end));
    data.time = coln(2:end, 1);
    data = data(:, [size(data,2) 1:(size(data,2)-1)]);
    
    writetable(data, fullfile(wd_data, 'trim_cty', ...
        ['trim_cty_' fname]), 'Delimiter', ',');
end

fcty = fullfile(wd_data, 'raw', 'cty_abbr.csv');
[~, data] = xlsread(fcty);
del_idx = find(contains(data(:,2), del_cty));
data = data(setdiff(1:size(data,1), del_idx), :);
data = array2table(data(2:end,:), 'VariableNames', data(1,:));
writetable(data, fullfile(wd_data, 'trim_cty', 'trim_cty_ctylist.csv'), ...
    'Delimiter', ',');

fgdplog = fullfile(wd_data, 'log', 'GDP_orig_seasAdjust.csv');
data = xlsread(fgdplog);
data = data(setdiff(1:numel(data), del_idx-1));
dlmwrite(fullfile(wd_data, 'trim_cty', 'GDP_orig_seasAdjust.csv'), data);