%% Initialization
clear all
wd = 'C:\Users\meini\OneDrive\vecmModeling';
wd_code = 'C:\respository\vecm-modeling';
wd_data = [wd '\data'];
wd_vis = [wd '\figure'];
cd(wd_data);

err_calc = 'Unexpected calculation error.';
%% logarithm transform
filepath = fullfile(wd_data, 'calcvar');
[filename, ~, filelist] = getFilename(filepath, '*noseas.csv');
filelist = [filelist fullfile(wd_data, 'trim_cty', 'trim_cty_GDP.csv')];
filelist = [filelist fullfile(wd_data, 'trim_cty', 'trim_cty_oilprc.csv')];

fname = arrayfun(@(x) filename{x}(1:4), 1:numel(filename), ...
    'UniformOutput', 0);
fname = [fname {'GDP', 'oilprc'}];

for i = 1:length(filelist)   
    data = xlsread(filelist{i});
    if ~isreal(data), error(err_calc); end
    
    lndata = log(data);
    if ~isreal(lndata), error(err_calc); end
    
    dlmwrite(fullfile(wd_data, 'logarithm', ['log' fname{i} '_noseas.csv']), ...
        lndata);
end
