# -*- coding: utf-8 -*-
"""
Created on Sat Aug 18 17:21:29 2018

@author: Welki
"""

import os
import pandas as pd
import numpy as np
import scipy.io as sio

# Country Name == Reporter Country ==> 'cty' (col)
# Counterpart Country Name = Partner Country ==> 'ctp_cty' (row)
# Output Format: 

wd = "C:/vecmModeling/data"
labels = ['imp', 'exp']
reps = [('Korea, Republic of', 'Korea'),
        ('United States', 'United States of America'),
        ('China, P.R.: Mainland', 'China')]
reps = pd.DataFrame(reps, columns=['old', 'new'])

for lab in labels:
    dots = pd.read_csv(os.path.join(wd, 'trade_weight', 'raw_weight_'+lab+'.csv'))
    for index, row in reps.iterrows():
        dots.replace(row[0], row[1], inplace=True)
    
    
    dots.sort_values(by=['cty', 'ctp_cty'], inplace=True)

    cty_list = pd.DataFrame(list(set(dots['cty'].tolist())), columns=['ctp_cty'])
    cty_list.sort_values(by='ctp_cty', inplace=True)
    cty_list.reset_index(drop=True, inplace=True)
    year_list = [int(x) for x in dots.columns[2:].tolist()]
    ncty=cty_list.shape[0]
    nyr = len(year_list)

    weights = np.ndarray(shape=(ncty,ncty,nyr), dtype=float, order='F')
    for yr,i in zip(year_list, range(nyr)):
        tmp = dots[['cty', 'ctp_cty',str(yr)]]
        tmp_mat = cty_list
        for index, row in cty_list.iterrows():
            cty = row.tolist()[0]
            tmpp = tmp[tmp['cty']==cty]
            tmpp.drop('cty', axis=1, inplace=True)
        
            tmp_mat = tmp_mat.merge(tmpp, how='left', on='ctp_cty')
            tmp_mat.rename(columns={str(yr): cty}, inplace=True)

    
        tmp_mat.drop('ctp_cty', axis=1, inplace=True)
        weights[:,:,i] = tmp_mat.values


    np.save(os.path.join(wd, 'trade_weight', 'weights_'+lab+'.npy'), weights)
    sio.savemat(os.path.join(wd, 'trade_weight', 'weights_'+lab+'.mat'), 
                {'weights': weights})