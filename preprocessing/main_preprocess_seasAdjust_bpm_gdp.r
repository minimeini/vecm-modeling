source('C:/respository/vecm-modeling/preprocessing/seasAdjust.r')
wd_data = 'C:/Users/meini/OneDrive/vecmModeling/data'

filelist = list.files(paste(wd_data, 'logarithm', sep='/'))
tmp = strsplit(filelist, '_')
savelist = NULL
for (t in tmp) {
  savelist = c(savelist, t[1])
}
filelist = paste(wd_data, 'logarithm', filelist, sep='/')

fdate = paste(wd_data, 'info', 'date_info.csv', sep='/')
date = read.csv(fdate, header=T)
date = as.character(date[,1])

for (i in 3:5) {
  data = read.csv(filelist[i], header=F)
  out = seasAdjust(data, date)
  
  data_new = out$data
  savepath = paste(wd_data, 'cleaned', paste0(savelist[i], '_seas.csv'), sep='/')
  write.table(data_new, savepath, sep=',', row.names=FALSE, col.names=F)
  rm(out)
}
rm(data)
rm(data_new)

gdp_log = read.csv(paste(wd_data, 'trim_cty', 'GDP_orig_seasAdjust.csv', sep='/'), header=F)
gdp_log = as.logical(as.matrix(gdp_log)[,1])
data = read.csv(filelist[1], header=F)
for (col in 1:ncol(data)) {
  if (!gdp_log[col]) {
    vec_new = seasAdjustVec(data[,col], date)
    data[,col] = vec_new
  }
}

savepath = paste(wd_data, 'cleaned', paste0(savelist[1],'_seas.csv'), sep='/')
write.table(data, savepath, sep=',', row.names=FALSE, col.names=F)