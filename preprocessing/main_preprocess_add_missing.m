%% Initialization
wd = 'C:\vecmModeling';
wd_code = 'C:\Respository\vecm-modeling';
wd_data = [wd '\data'];
wd_vis = [wd '\figure'];
cd(wd);

[~, cnames] = xlsread(fullfile(wd_data, 'raw', 'cty_abbr.csv'));
cnames_short = cnames(2:end, 2);
cnames_long = cnames(2:end, 1);
labels = {'ex', 'im'};

%% fillna: tex6, tim6

for i = 1:2
    [data, strs] = xlsread(fullfile(wd_data, 'trim_extract', ...
        ['rawvar_trim_t' labels{i} '6.csv']));
    [dataref, stref] = xlsread(fullfile(wd_data, 'trim_extract', ...
        ['rawvar_trim_t' labels{i} '5.csv']));
    data = data(1:end-2,:);
    time = strs(2:end-2,1);
    timeref = stref(2:end,1);

    colidx = find(sum(isnan(data), 1));
    for j = 1:length(colidx)
        if isnan(data(1, colidx(j)))
            idx_end = find(~isnan(data(:,colidx(j))), 1) - 1;
            data(1:idx_end, colidx(j)) = abs(dataref(1:idx_end, colidx(j)));
        end
    end

    data = array2table(data, 'VariableNames', cnames_short);
    data.time = time;
    data = data(:, [size(data,2) 1:(size(data,2)-1)]);
    
    writetable(data, fullfile(wd_data, 'trim_extract', ...
        ['rawvar_trim_fill_t' labels{i} '6.csv']), 'Delimiter', ',');
end

%% visualize
for i = 1:length(labels)
    savepath = fullfile(wd_vis, ['raw_t' labels{i}]);
    if exist(savepath, 'dir') ~= 7
        mkdir(savepath);
    end
    
    [data, strs] = xlsread(fullfile(wd_data, 'trim_extract', ...
        ['rawvar_trim_fill_t' labels{i} '6.csv']));
    times = strs(2:end, 1);
    
    for j = 1:size(data,2)
        f = figure;
        plot(data(:,j));
        title(cnames_long{j});
        print(f, fullfile(savepath, ...
            ['raw_t' labels{i} '_' cnames_long{j} '.png']), '-dpng');
        close all
        delete(f)
        delete(gcf)
    end
    
end