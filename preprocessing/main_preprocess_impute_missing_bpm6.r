library(imputeTS)
options(warn=0)

wd_data = 'C:/vecmModeling/data'
labels = c('im', 'ex')

# kalman with structual time series: SGP, CHN
# kalman with seasonality: NOR

for (lab in labels) {
  filename = paste0('rawvar_trim_fill_t',lab,'6.csv')
  loadpath = paste(wd_data, 'trim_extract', filename, sep='/')
  data = read.csv(loadpath, header=TRUE)
  
  flag = FALSE
  
  if (anyNA(data$GRC)) {
    impt = na.seadec(ts(data$GRC, start=c(1993,1), end=c(2016,4), frequency=4), 
                     algorithm = "interpolation")
    data$GRC = impt
    flag = TRUE
  }
  
  if (anyNA(data$NOR)) {
    tmp = na.seadec(ts(data$NOR, start=c(1993,1), end=c(2016,4), frequency=4), 
                    algorithm='kalman')
    data$NOR = tmp
    flag = TRUE
  }
  
  if (anyNA(data$CHN)) {
    tmp_ts = ts(data$CHN, start=c(1993,1), end=c(2016,4), frequency=4)
    
    tmp_decomp = decompose(tmp_ts, type='multiplicative')
    tmp_err = tmp_decomp$random
    tmp_err = sample(tmp_err[!is.na(tmp_err)], sum(is.na(tmp_ts)))
    tmp_err = c(tmp_err, rep(1, sum(!is.na(tmp_ts))))
    
    tmp = na.kalman(tmp_ts, model="StructTS", smooth=FALSE) * tmp_err
    data$CHN = tmp
    flag = TRUE
  }
  
  if (anyNA(data$SGP)) {
    tmp_ts = ts(data$SGP, start=c(1993,1), end=c(2016,4), frequency=4)
    
    tmp_decomp = decompose(tmp_ts, type='multiplicative')
    tmp_err = tmp_decomp$random
    tmp_err = sample(tmp_err[!is.na(tmp_err)], sum(is.na(tmp_ts)))
    tmp_err = c(tmp_err, rep(1, sum(!is.na(tmp_ts))))
    
    tmp = na.kalman(tmp_ts, model="StructTS", smooth=FALSE) * tmp_err
    data$SGP = tmp
    flag = TRUE
  }
  
  if (flag) {
    filename = paste0('rawvar_trim_fill_impute_t',lab,'6.csv')
    loadpath = paste(wd_data, 'impute', filename, sep='/')
    write.csv(data, loadpath, row.names=FALSE)
  }
  
  
}
