# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 13:04:04 2018

@author: Welki
"""

import os
import pandas as pd
import numpy as np
import scipy.io as sio

wd = "C:/Users/Welki/OneDrive/vecmModeling/data/trade_weight"
labels = ['imp', 'exp']
comment = '_colRep_rowPart'

for lab in labels:
    data = pd.read_csv(os.path.join(wd, 'raw_weight_'+lab+comment+'.csv'))
    ncty=data.shape[0]
    nyr = int((data.shape[1]-1) / ncty)
    cols = pd.Series(data.columns)
    grp_idx=pd.Series(cols[cols.str.contains('Australia')].index)
    
    grp_size = []
    weights = np.ndarray(shape=(ncty,ncty,nyr), dtype=float, order='F')
    
    yr_cnt = 0
    for idx in range(nyr):
        if idx == 0:
            grp_size.append(0)
            tmp_size = 0
        else:
            tmp_size = grp_idx[idx] - grp_idx[idx-1]
            grp_size.append(tmp_size)
            
        if tmp_size == ncty and idx < nyr-1:
            weights[:,:,yr_cnt] = data.iloc[:, grp_idx[idx-1]:grp_idx[idx]]
            yr_cnt += 1
        elif tmp_size == ncty and idx == nyr-1:
            weights[:,:,yr_cnt] = data.iloc[:, grp_idx[idx]:]
            yr_cnt += 1
        elif tmp_size == 0:
            continue
        else:
            raise IndexError('Incomplete reporting countries')
    
    np.nan_to_num(weights, copy=False)
    
#    tmp_sum = np.sum(weights, axis=1)
#    tmp_sum = np.expand_dims(tmp_sum, axis=1)
#    tmp_sum = np.tile(tmp_sum, (1,ncty,1))
#    
#    weights = weights / tmp_sum
#    np.nan_to_num(weights, copy=False)
    
    np.save(os.path.join(wd, 'weights_extended_'+lab+'.npy'), weights)
    sio.savemat(os.path.join(wd, 'weights_extended_'+lab+'.mat'),
                {'weights':weights})