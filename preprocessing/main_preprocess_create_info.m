%% Initialization
wd = 'C:\vecmModeling';
wd_code = 'C:\Respository\vecm-modeling';
wd_data = [wd '\data'];
wd_vis = [wd '\figure'];
cd(wd);

%% data_info
[~,cty_cl] = xlsread(fullfile(wd_data, 'raw', 'cty_abbr.csv'));
cty_cl = cty_cl(2:end,:);
ncty = size(cty_cl, 1);

% dv: lnrtim, lnrtex, lny (GDP), lnp
% fv: lnrtim, lnrtex, lny (GDP), lnp
% gv: lnoil

dvflag = ones(ncty, 4); dvflag = mat2cell(dvflag, ones(1,ncty), ones(1,4));
fvflag = ones(ncty, 4); fvflag = mat2cell(fvflag, ones(1,ncty), ones(1,4));
gvflag = ones(ncty, 1); gvflag(end) = 2; 
gvflag = mat2cell(gvflag, ones(1,ncty));


data_info = [cty_cl, dvflag, fvflag, gvflag];
data_info = array2table(data_info, 'VariableNames', ...
    {'cnames_long', 'cnames_short', 'lnrtim', 'lnrtex', 'lny', 'lnp', ...
    'lnrtims', 'lnrtexs', 'lnys', 'lnps', 'lnoil'});
writetable(data_info, fullfile(wd_data, 'info', 'input_info.csv'));

%% var_info
opts=detectImportOptions(fullfile('C:\vecmModeling\data\info\var_info.csv'));
var_info=readtable('C:\vecmModeling\data\info\var_info.csv', opts);

%% date_info
% 1993Q1 - 2016Q4
yrs = reshape(repmat(1993:2016,4,1),1,[]);
qts = reshape(repmat(1:4,1,24),1,[]);
dates = arrayfun(@(x) [num2str(yrs(x)) 'Q' num2str(qts(x))], ...
    1:numel(yrs), 'UniformOutput', 0);

date_info = array2table(dates', 'VariableNames', {'date'});
writetable(date_info, fullfile(wd_data, 'info', 'date_info.csv'));
