seasAdjust <- function(data, date, signlev=0.05, allAdjusted=F) {
  options(warn=2)
  
  seasSheet <- vector('numeric', ncol(data))
  for (j in 1:ncol(data)) { # the j-th country
    data[,j] <- seasAdjustVec(data[,j], date, signlev, allAdjusted)
  }
  
  out <- list(data=data, log=seasSheet)
  return(out)
}

seasAdjustVec <- function(vec, date, signlev=0.05, allAdjusted=F) {
  # `vec`: vector
  # `date`: vector
  options(warn=2)
  if (class(vec)!='numeric') {
    stop('vector to be adjusted should be numeric vector.')
  }
  if (class(date)!='character') {
    stop('date should be character vector.')
  }
  
  nobs <- length(date)
  start <- as.numeric(strsplit(date[2], 'Q')[[1]]) # start from the 2nd obs cuz we will diff it
  end <- as.numeric(strsplit(date[nobs], 'Q')[[1]])
  
  # generate seasonal dummies
  seasInd <- sapply(date, function(x) as.numeric(strsplit(as.character(x), 'Q')[[1]][2]))
  seasDum <- matrix(0, nrow=length(date), ncol=4)
  for (i in 1:length(seasInd)) {
    seasDum[i, seasInd[i]] <- 1
  }
  seasDum[,1] <- seasDum[,1] - seasDum[,4]
  seasDum[,2] <- seasDum[,2] - seasDum[,4]
  seasDum[,3] <- seasDum[,3] - seasDum[,4]
  seasDum <- seasDum[,c(1:3)]
  
  hold <- vec[1]
  difftemp <- as.matrix(diff(vec))
  datatemp <- cbind(difftemp, seasDum[-1,])
  colnames(datatemp) <- c('deltaY', 'S14', 'S24', 'S34')
  
  # stage 1. testing the seasonality
  # mdl_reduce <- lm(deltaY~1)
  mdl_full <- lm(deltaY~S14+S24+S34, as.data.frame(datatemp))
  mdl_full.est <- lm.fit(datatemp[,-1], datatemp[,1])
  deltaY.est <- mdl_full.est$fitted.values
  deltaY.mean <- mean(datatemp[,1])
  
  # F-test
  # print(mdl_full.est$rank)
  df1 <- mdl_full.est$rank - 1
  df2 <- nobs - mdl_full.est$rank - 1
  MSR <- (deltaY.est-deltaY.mean)%*%(deltaY.est-deltaY.mean) / df1
  MSE <- (deltaY.est-datatemp[,1])%*%(deltaY.est-datatemp[,1]) / df2
  Fstat <- as.numeric(MSR/MSE)
  pval <- tryCatch({
    pf(Fstat,df1,df2,lower.tail=F)
  }, error=function(e) {
    # print(paste0('pval:', pval))
    if (df1 == -1) {
      # print(summary(mdl_full))
      print('No Seasonal Adjustment')
      return(vec)
    } else {
      print(paste0('Fstat: ', Fstat))
      print(paste0('DF1: ', df1))
      print(paste0('DF2: ', df2))
      stop('Fail to do test')
    }
    
  })
  
  
  if ((pval < signlev)||allAdjusted) {
    # stage 2. seasonal adjustment
    print('perform adjustment')
    
    difftemp <- ts(difftemp, start=start, end=end, frequency=4)
    
    decomp_mdl <- seasonal::seas(difftemp, transform.function ='none', x11='', estimate.maxiter = 2000)
    delta <- seasonal::final(decomp_mdl) # get the adjusted series
    delta <- c(hold, delta)
    vec <- cumsum(delta)
    
    rm(decomp_mdl)
  }
  
  return(vec)
}