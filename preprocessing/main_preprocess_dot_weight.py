# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import pandas as pd

def parse_datainfo(data, output=True):
    cty_list = list(set(data['Country Name'].tolist()))
    counter_cty_list = list(set(data['Counterpart Country Name'].tolist()))
    
    year_list = [int(x) for x in data.columns.tolist()[7:-1]]
    inds = list(set(data['Indicator Name'].tolist()))
    
    if output:
        print('Partner Countries: {}'.format(len(cty_list)))
        print('Reporter Countries: {}'.format(len(counter_cty_list)))
        print('Observed years: {}'.format(len(year_list)))
        
        if len(cty_list) != len(counter_cty_list):
            print('Incomplete countries.')
            return -1
    
    return cty_list, year_list, inds
    

def filter_value(data, cty, ind):
    inds = ['Goods, Value of Exports, Free on board (FOB), US Dollars',
            'Goods, Value of Imports, Cost, Insurance, Freight (CIF), US Dollars']
    
    if 'FOB' in ind.upper() or 'exp' in ind.lower():
        idx = inds[0]
    elif 'CIF' in ind.upper() or 'imp' in ind.lower():
        idx = inds[1]
    else:
        return -1
    
    tmp = pd.DataFrame({'cty':data['Country Name']==cty, 
                       'ind':data['Indicator Name']==idx})
    fltr = tmp.all(axis=1)
    data_new = data[fltr]
    
    return fltr, data_new


def add_missing(data, subject, ind):
    fltr, data_fltr = filter_value(data, sub, ind)
    obj = set(data['Counterpart Country Name'].tolist()) - set(data_fltr['Counterpart Country Name'].tolist()) - set([sub])
    obj = list(obj)[0]
    print('{} ==> {}'.format(sub, obj))

    fltr, data_add = filter_value(data, obj, ind)
    data_add = data_add[data_add['Counterpart Country Name'] == sub]

    tmp = data_add['Country Code'].tolist()[0]
    data_add.at[data_add.index[0],'Country Code'] = data_add.at[data_add.index[0],'Counterpart Country Code']
    data_add.at[data_add.index[0],'Counterpart Country Code'] = tmp

    data_add.at[data_add.index[0],'Country Name'] = sub
    data_add.at[data_add.index[0],'Counterpart Country Name'] = obj
    data_add.loc[data_add.index[0],7:32] = 0
    
    return data_add


def check_integrity(data, cty_list):
    ncty = len(cty_list)
    check_counter_0 = []
    check_counter_1 = []
    for cty in cty_list:
        fltr, data_tmp = filter_value(data, cty, 'exp')
        if data_tmp.shape[0] != (ncty - 1):
            check_counter_0.append(1)
        else:
            check_counter_0.append(0)
    
        fltr, data_tmp = filter_value(data, cty, 'imp')
        if data_tmp.shape[0] != (ncty - 1):
            check_counter_1.append(1)
        else:
            check_counter_1.append(0)

    return check_counter_0, check_counter_1


def skinny_columns(data):
    data = data.drop(data.columns[1:4], axis=1)
    data = data.drop(data.columns[2:4], axis=1)
    data = data.iloc[:,:-1]
    data.rename(columns={'Country Name': 'cty', 
                         'Counterpart Country Name': 'ctp_cty'}, inplace=True)
    
    return data
    



wd = "C:/vecmModeling/data"
data = pd.read_csv(wd+'/raw/DOT_timeSeries.csv')

# Country Name == Reporter Country ==> 'cty' (col)
# Counterpart Country Name = Partner Country ==> 'ctp_cty' (row)
cty_list, year_list, inds = parse_datainfo(data)
ncty = len(cty_list)
labels = ['exp', 'imp']

check_exp, check_imp = check_integrity(data, cty_list)
while sum(check_exp) + sum(check_imp) > 0:
    for lab in labels:
        if lab == 'exp' and len(cty_list[check_exp.index(1)]) > 0:
            sub = cty_list[check_exp.index(1)]
        elif lab == 'imp' and len(cty_list[check_imp.index(1)]) > 0:
            sub = cty_list[check_imp.index(1)]
    
        data_add = add_missing(data, sub, lab)
        data = pd.concat([data, data_add], ignore_index=True)

    check_exp, check_imp = check_integrity(data, cty_list)

data.to_csv(wd+'/raw/DOT_ts_clean.csv')


for lab,i in zip(labels, range(len(labels))):
    data_tmp = data[data['Indicator Name']==inds[i]]
    data_tmp = skinny_columns(data_tmp)
    data_tmp.to_csv(wd+'/trade_weight/raw_weight_'+lab+'.csv', index=False)
    



