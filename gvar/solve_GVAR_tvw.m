  function [G0, h0, h1, G, J0, J, zeta, Sigma_zeta, ...
    eta, Sigma_eta, G0y, stat] = solve_gvar_tvw(y, gxv, gvlist, gvloc, W_tv, ...
    freq, a0, a1, Theta, Lambda0, Lambda, Gamma0, Gamma, varargin)

%%
default_nargs = -nargin('solve_gvar_tvw') - 1;
mod_type = 'ex';
if nargin>=default_nargs+1, mod_type = varargin{1}; end

if ~ischar(mod_type)
    error('INPUT ERROR: mod_type = {''ex'', ''du''}');
end

cnames = fieldnames(Lambda);
cnum = numel(cnames);
maxlag = max(arrayfun(@(x) size(Lambda.(cnames{x}),3), 1:cnum));
maxlag = max(maxlag,max(arrayfun(@(x) size(Theta.(cnames{x}),3),1:cnum)));

Klist = arrayfun(@(x) size(Theta.(cnames{x}),1), 1:cnum);

%% >>> Stacking VARX countries
%***********************
[G0, h0, h1, G, J0, J, stat] = stack_params(W_tv, gvlist, a0, a1, Theta, ...
    Lambda0, Lambda, Gamma0, Gamma, mod_type);
K = size(h0,1); 

%% create the united exogenous variables `dt` with foreign variables excluded
gvfull = [];
for i = 1:cnum
    gvfull = union(gvfull, gvlist.(cnames{i}), 'stable');
end
gvfull = sort_names(gvfull, gvlist)';
gvtype = arrayfun(@(x) any(gvloc(:,x)>0), 1:size(gvloc,2));

if size(gvloc,2) ~= numel(gvfull)
    error('INPUT ERROR: inconsistency between `gvflag` and `gvlist`.');
end
if numel(gvtype) ~= numel(gvfull)
    error('CALCULATION ERROR: inconsistency between `gvfull` and `gvtype`.');
end

% gvtype = 1, enter in one of the country model as endogenous
% gvtype = 0, as exogenous in all country models

dt = []; % dt = ngv * ntimes
if ~isempty(gvfull)
    for i = 1:numel(gvfull)
        globeVar = gvfull{i};
        globeType = gvtype(i);
        if globeType == 1
            model_idx = find(gvloc(:,i)>0);
            globe_idx = sum(Klist(1:model_idx-1)) + gvloc(model_idx,i);
            dt = [dt; y(globe_idx,:)]; % ngv * ntimes
        elseif globeType == 0 && ~isempty(gxv)
            tmp = gxv.(globeVar);
            if size(tmp,1) ~= 1
                tmp = tmp';
            end
            dt = [dt; tmp];
        else
            error('INPUT ERROR: empty gxv value.');
        end
    end
end

trend_t =[0:1:size(y,2)]'; %#ok
trend = trimr(trend_t,maxlag,1);
ntrain = size(y,2); % total train points / total time
nobst = size(trimr(y',maxlag,0),1); % nobst = ntrain - maxlag;

%% >>> Estimation and Calculate fitting error
% Estimation as a whole
Yest = zeros(K, ntrain);
Yest(:,1:maxlag) = y(:,1:maxlag);
est = zeros(K, nobst);
G0y = zeros(K, nobst);

for i = (maxlag+1):ntrain % nobst = ntrain - maxlag
    widx = timeMap(i, maxlag, ntrain, freq);
    % widx: i-maxlag, i-maxlag+1, ..., i-1, i
    widx = fliplr(widx);
    
    % cummulation of exogenous part
    ex_tmp = [];
    if ~isempty(J0) && ~isempty(dt)
        ex_tmp = J0 * dt(:,i);
    end
    
    for j = 1:maxlag % lag order
        % cummulation of endogenous part
        lag_tmp = lagm(y',j)'; % nendog * ntimes
        est(:,i-maxlag) = est(:,i-maxlag) + ...
            squeeze(G(:,:,j,widx(j + 1))) * lag_tmp(:,i);
        
        % cummulation of exogenous part
        if ~isempty(J) && ~isempty(dt)
            exlag_tmp = lagm(dt',j)'; % exlag_tmp: ngv * ntimes
            ex_tmp = ex_tmp + squeeze(J(:,:,j)) * exlag_tmp(:,i); % J: nendog * ngv * nlag
        end
    end
    % est: nendog * ntime, ex_tmp: nendog * ntime
    
    % estimation value of G0*y
    est(:,i-maxlag) = est(:,i-maxlag) + h0 + h1 * trend(i-maxlag);
    if ~isempty(ex_tmp)
        est(:,i-maxlag) = est(:,i-maxlag) + ex_tmp;
    end
    
    % real value of G0*y
    G0y(:,i-maxlag) = squeeze(G0(:,:,widx(1))) * y(:,i);
    
    % estimation value of y
    try
        Yest(:,i) = invMatrix(squeeze(G0(:,:,widx(1))), 'QR') * ...
            est(:,i-maxlag);
    catch
        Yest(:,i) = invMatrix(squeeze(G0(:,:,widx(1))), 'pseudo') * ...
            est(:,i-maxlag);
    end
end

zeta = G0y - est;
Sigma_zeta = (zeta*zeta') / cols(zeta);
eta = y - Yest;
Sigma_eta = (eta*eta') / cols(eta);

end

