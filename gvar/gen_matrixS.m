function mats = gen_matrixS(target_var, orig_var)
% function mats = gen_matrixS(target_var, orig_var)
% Generate selection matrix of the time-varying weights integration
%
% Required Input
% - target_var: cell array of targeted variable names
% - orig_var: cell array of original variable names

if ~iscell(target_var) || ~iscell(orig_var)
    error('INPUT ERROR: Invalid Inputs.');
end
tar_flag = arrayfun(@(x) ~ischar(target_var{x}), 1:numel(target_var));
orig_flag = arrayfun(@(x) ~ischar(orig_var{x}), 1:numel(orig_var));
flag = sum(tar_flag(:)) + sum(orig_flag(:));
if flag > 0
    error('INPUT ERROR: Input must be cell array of characters.');
end

if numel(target_var) > numel(orig_var)
    error('INPUT ERROR: More exogenous variables than endogenous variables.');
end

%%
ntar = numel(target_var);
norig = numel(orig_var);
mats = zeros(ntar, norig);
for i = 1:ntar
    mats(i,:) = strcmp(target_var{i}, orig_var);
end

end