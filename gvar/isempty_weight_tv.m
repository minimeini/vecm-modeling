function flag = isempty_weight_tv(weight)
% function flag = isempty_weight_tv(weight)
% Check if W_tv or wm_t is empty.

if isempty(weight)||~isstruct(weight)
    error('INPUT ERROR: invalid W_tv or wm_t structure. Ref: def_weight_tv');
end

fm = fieldnames(weight);
if ~isempty(fm)
    flag = isempty(weight.(fm{1}));
else
    flag = 1;
end
end