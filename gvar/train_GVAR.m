function estMdl = train_gvar(cnames, date_info, endonames, gvnames, ...
    gnvnames, endoflag, gvflag, endodata, gvdata, wdata, varargin)
% function estMdl = train_gvar(cnames, date_info, endonames, ...
%     gvnames, gnvnames, endoflag, gvflag, endodata, gvdata, maxlag_p, ...
%     maxlag_q, wdata, varargin)
%
% ***** Workflow *****
%
% Step 0. gen_params: endodata,gvdata,wdata -> endog,exog,exognx,wm_t,X
% Step 1. Determining the lag for country-specific VARX if lag is not
% specified yet
% Step 2. Determining the number of cointegrating relations for each
% countr-specific VECMX if rank is not specified yet
% Step 3. Create time-varying link matrices `W_tv`
% Step 4. Estimate GVAR model with `build_gvar`

%% generate related information and parameters
nargs = -nargin('train_gvar') - 1;
varxlag = [];
rank = [];
maxlag_p = 2;
maxlag_q = 2;

if nargin >= nargs + 1, varxlag = varargin{1}; end
if nargin >= nargs + 2, rank = varargin{2}; end
if nargin >= nargs + 3, maxlag_p = varargin{3}; end
if nargin >= nargs + 4, maxlag_q = varargin{4}; end

criterion = 'aic'; psc = 4;

[endog,endoglist,exog,exoglist,exognx,exognxlist,wm_t,foreign,X] = ...
    gen_params(cnames,date_info,endonames,gvnames,gnvnames, ...
    endoflag,gvflag,endodata,gvdata,wdata);

maxlag = max(maxlag_p, maxlag_q);
freq = date_info.freq;
cnum = numel(cnames);

%% Determining the lag length of each country model
if isempty(varxlag)
    if strcmp(criterion,'aic') == 1
        info = 2;
    elseif strcmp(criterion,'sbc') == 1
        info = 3;
    end
    % Information criterion used for model lag selection:
    % info = 2 for Akaike Information criterion;
    % info = 3 for Schwartz Bayesian Information Criterion.

    % retrieve maximum lag order for serial correlation test
    for n=1:cnum
        aic_sbc.(cnames{n}) = [];
        F_serialcorr.(cnames{n}) = [];
        for p=1:maxlag_p
            for q=1:maxlag_q
                [logl_aic_sbc, psc_degfrsc_Fcrit_Fsc] = ...
                    select_varxlag(maxlag,psc,endog.(cnames{n}),p,...
                    exog.(cnames{n}),q);
                aic_sbc.(cnames{n}) = [aic_sbc.(cnames{n}); ...
                    logl_aic_sbc p q];
                F_serialcorr.(cnames{n}) = ...
                    [F_serialcorr.(cnames{n}); psc_degfrsc_Fcrit_Fsc];
            end
        end
    end
    for n=1:cnum
        varxlag.(cnames{n}) = [];
        for i=1:rows(aic_sbc.(cnames{1}))
            if aic_sbc.(cnames{n})(i,info) == ...
                    max(aic_sbc.(cnames{n})(:,info))
                varxlag.(cnames{n}) = aic_sbc.(cnames{n})(i,4:5);
            end
        end
    end

    clear psc_degfrsc_Fcrit_Fsc
end
%% Determining the number of cointegrating relations for each country
% retrieve VECMX* estimation cases for each country-model
if isempty(rank)
    curpath = cd; filepath = fullfile(curpath, 'preset');
    prefix = 'coint_critvalues_Trace_case';
    trace_crit95_c2 = dlmread(fullfile(filepath,...
        [prefix num2str(2) '.csv']),',',1,1);
    trace_crit95_c3 = dlmread(fullfile(filepath,...
        [prefix num2str(3) '.csv']),',',1,1);
    trace_crit95_c4 = dlmread(fullfile(filepath,...
        [prefix num2str(4) '.csv']),',',1,1);

    estcase = struct();
    for n=1:cnum, estcase.(cnames{n}) = 4; end
    % compute cointegration statistics
    for n=1:cnum
        [junk, trace_out, maxeig_out] = cointegration_test(maxlag,...
            estcase.(cnames{n}),endog.(cnames{n}),...
            varxlag.(cnames{n})(1),exog.(cnames{n}), ...
            varxlag.(cnames{n})(2)); 

        trace.(cnames{n}) = trace_out;
    end

% determining the number of cointegrating relations
    rank = get_rank(cnames,endog,exog,trace,trace_crit95_c2,...
        trace_crit95_c3,trace_crit95_c4,estcase);
end

%% create (time-varying) link matrices
W_tv = create_tv_linkmatrices(wm_t, endoglist, exognxlist);

%% Build gvar model
estMdl = struct();
estMdl.rank = rank; estMdl.varxlag = varxlag;
[estMdl.y,estMdl.gxv,estMdl.eta,estMdl.Sigma_eta,estMdl.a0,estMdl.a1,...
    estMdl.Theta,estMdl.Lambda0,estMdl.Lambda,estMdl.Gamma0,...
    estMdl.Gamma,estMdl.logl,estMdl.aic] = build_gvar(X,W_tv,freq,...
    endoglist,exog,exoglist,exognxlist,varxlag,rank);

end