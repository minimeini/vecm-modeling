function xsim = simulate_gvar(rank,varxlag,var_info,trdata,tsdata,varargin)
% function xsim = simulate_gvar(rank,varxlag,var_info,trdata,tsdata,varargin)
% -------------------------------------------------------
% >>> Procedure
%
% TARGET: simulate the testing set
% > iterated by forecasting horizon/step
%
% For each forecasting step:
% 0. [Train GVAR]: get the training set (time length=T-h_step+1) and get
% the model parameters
% 1. [Parameter Uncertainty] In-sample simulation: simulate a training
% set using precomputed model parameters obtained in step 0
% 2. [Parameter Uncertainty] Paramter re-estimation: re-estimate (MLE) model
% paramters using the simulated training set
% 3. [Future Uncertainty] h-step ahead forecasting with bootstrapped
% residuals (eta)
% -------------------------------------------------------
% >>> Workflow
%
% > iterated by forecasting horizon/step
%
% Step 1. Get the model parameters and burnin training set based on 
% original training set
% Step 2. In-sample simulation of training set using parameters
% obtain in step 0.
% Step 3. Re-estimate model parameters using the simulated
% training set
% Step 4. h-step ahead rolling forecasting using the real training
% set `trdata_hs`
% Step 5. Update the original training set by 1 time step using 
% real value
% -------------------------------------------------------
% >>> Required Input
%
% - varxlag: struct, lag of country-specific var, fieldnames = cnames,
%   field = [1 2] numeric array
% - rank: struct, rank of country-specific vecm, fieldnames = cnames, 
%   field = numeric number
% - trdata: struct, training data
%   - endodata: struct, fieldnames = endonames, field = [ntime cnum] 
%   numeric matrix
%   - gvdata: struct, fieldnames = gvnames, field = [ntime 1] numeric
% matrix
%   - wdata: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info: struct, fieldnames = {'start', 'end', 'freq'}
% - tsdata: struct, testing data
%   - endodata_test: struct, fieldnames = endonames, field = [ntime cnum]
%   numeric matrix
%   - gvdata_test: struct, fieldnames = gvnames, field = [ntime 1] 
%   numeric matrix
%   - wdata_test: [(nyear*cnum) cnum] 2d numeric matrix
%   - date_info_test: struct, fieldnames = {'start', 'end', 'freq'}
% - var_info: struct, variable information
%   - cnames: 1d char array, name of countries
%   - endonames: 1d char array, names of endogenous variable and foreign
%   variable
%   - gvnames: 1d char array, names of global (strong+weak) variable
%   - gnvnames: 1d char array, names of global (weak) variable
%   - endoflag: [cnum vnum] numeric matrix, 0=not included, 1=included
%   - gvflag: [cnum gvnum] numeirc matrix, 0=not included, 1=included as
%   exogenous, 2=included as endogenous
% -------------------------------------------------------
% >>> Optional Input
%
% - nparams: numeric, # of iterations for parameter uncertainty, default =
% 10
% - nfuture: numeric, # of iterations for future uncertainty, default = 50
% - fhorz: numeric, forecasting horizons, default = 1
% - h_step: numeric, step for forecasting, default = 1 ==> 1-step ahead
% - trEIG: numeric, critical values to trim the eigen values, default =
% 1e-05
% - sim_type: char, simulation method, {'nonparametric','parametric'},
% default = 'nonparametric'
% -------------------------------------------------------
% >>> Output
%
% - xsim: 3d numeric matrix, forecasting results, 
% [nvar fhorz nparams*nfuture]

%% Get optional input
nargs = -nargin('simulate_gvar') - 1;

% >>> simulation options
nparams = 10; 
nfuture = 50;
fhorz = 1;
h_step = 1;

% >>> tuning the simulation of residuals
trEIG = 1e-05;
sim_type = 'nonparametric';

if nargin >= nargs + 1, nparams = varargin{1}; end
if nargin >= nargs + 2, nfuture = varargin{2}; end
if nargin >= nargs + 3, fhorz = varargin{3}; end
if nargin >= nargs + 4, h_step = varargin{4}; end
if nargin >= nargs + 5, trEIG = varargin{5}; end
if nargin >= nargs + 6, sim_type = varargin{6}; end

%% Input validation
err_msg = [];
err_msg.dont_run = 'Finish without execution.';
err_msg.invalid_input = 'Invalid Input.';
err_msg.compute_error = 'Computational error.';

if nparams<0 || nfuture<0, error(err_msg.invalid_input); end
if nparams==0 && nfuture==0, error(err_msg.dont_run); end
if fhorz<0, error(err_msg.invalid_input); end
if nfuture>1 && fhorz==0, error(err_msg.invalid_input); end
if ~isnumeric(trEIG)||trEIG<=0, error(err_msg.invalid_input); end
if ~ismember(sim_type,{'nonparametric','parametric'})
    error(err_msg.invalid_input);
end
if h_step<1, error(err_msg.invalid_input); end

tsdata = normalize_tsdata(tsdata);

%% Input formatting
nparams = nparams + 1;
cnum = numel(var_info.cnames);

gvdata_full = struct();
for i = 1:numel(var_info.gvnames)
    gvdata_full.(var_info.gvnames{i}) = ...
        [trdata.gvdata.(var_info.gvnames{i}); ...
        tsdata.gvdata.(var_info.gvnames{i})];
end

[~,X] = create_endogvars(trdata.endodata,trdata.gvdata,...
    var_info.endoglist,var_info.cnames);
lowbnd = prctile(X',1)'; upbnd = prctile(X',99)';

Klist = arrayfun(@(x) numel(var_info.endoglist.(var_info.cnames{x})),1:cnum);
K = sum(Klist(:)); Klist_ext = [0 Klist];
xsim = zeros(K,fhorz,nparams*nfuture);
%%  
for i = 1:fhorz
%%
    % Step 0. Get model parameters and burnin training set based on
    % original training set
    [trdata_hs,wdata_forc] = trim_data_train_orig(var_info,trdata,...
        h_step,tsdata);
    wm_t = gen_weight(trdata_hs.wdata,...
        trdata_hs.date_info.start,trdata_hs.date_info.end);
    
    estMdl_hs = train_gvar(var_info.cnames,trdata_hs.date_info,...
        var_info.endonames,var_info.gvnames,var_info.gnvnames,...
        var_info.endoflag,var_info.gvflag,trdata_hs.endodata,...
        trdata_hs.gvdata,trdata_hs.wdata,varxlag,rank);
    
    %%
    burnin_hs = get_data_burnin(var_info,trdata_hs);
    %%
    for iter = 1:nparams
        % Step 1. In-sample simulation of training set using parameters
        % obtain in step 0.
        % tmp_sim_full: 2d numeric matrix, [K nsim]
        % simdata: struct,fieldnames=cnames, field=[ntime nvar]
        if iter>1
            %%
            err_fit = simulate_residuals(estMdl_hs.eta,burnin_hs.nsim,...
                trEIG,sim_type);
            tmp_sim_full = forecast_gvar(burnin_hs.nsim,burnin_hs.wm_t,...
                burnin_hs.wdata_sim,burnin_hs.date_info.freq,burnin_hs.X,...
                trdata_hs.gvdata,estMdl_hs.varxlag,estMdl_hs.rank,...
                estMdl_hs.a0,estMdl_hs.a1,estMdl_hs.Theta,...
                estMdl_hs.Lambda0,estMdl_hs.Lambda,estMdl_hs.Gamma0,...
                estMdl_hs.Gamma,var_info.endoglist,var_info.exoglist,...
                var_info.exognxlist,0,[],err_fit,1,lowbnd,upbnd);
            check_variable(tmp_sim_full);

            simdata = struct();
            for n = 1:cnum
                % tmp_burnin: [nburnin nvar]
                tmp_burnin = burnin_hs.endog.(var_info.cnames{n});
                tmp_sim = tmp_sim_full(sum(Klist_ext(1:n))+1:sum(Klist_ext(1:n+1)),:)';
                simdata.(var_info.cnames{n}) = [tmp_burnin; tmp_sim];
            end
            
            trdata_sim = update_data_train_sim(var_info,trdata_hs,simdata);
            
            % Step 2. Re-estimate model parameters using the simulated
            % training set
            estMdl_sim = train_gvar(var_info.cnames,...
                trdata_sim.date_info,var_info.endonames,...
                var_info.gvnames,var_info.gnvnames,...
                var_info.endoflag,var_info.gvflag,trdata_sim.endodata,...
                trdata_sim.gvdata,trdata_sim.wdata,varxlag,rank);
        else
            estMdl_sim = estMdl_hs;
        end
        
        % Step 3. h-step ahead rolling forecasting using the real training
        % set `trdata_hs` and re-estimated model parameters

        %%
        xsim(:,i,(iter-1)*nfuture+1:iter*nfuture) = ...
            simulate_gvar_roll_1step(estMdl_sim,var_info,trdata_hs,...
            gvdata_full,wm_t,wdata_forc,h_step,nfuture,trEIG,sim_type); 
    end
    
    %% Step 4. Update the original training set by 1 time-step using
    % real value
    [trdata,tsdata] = update_data_train_orig(var_info,trdata,tsdata);
end

check_variable(xsim);
end

%% Check variable
function status = check_variable(data)
data = sum(abs(data),2);
status = any(data(:)==0);
if status==1
    error('CALCULATION FAILED: empty value in data.');
end
 
end

%% normalize tsdata
function tsdata_norm = normalize_tsdata(tsdata)
fm = fieldnames(tsdata);
tsdata_norm = struct();
for i = 1:numel(fm)
    if contains(fm{i}, '_test')
        tsdata_norm.(fm{i}(1:end-5)) = tsdata.(fm{i});
    else
        tsdata_norm.(fm{i}) = tsdata.(fm{i});
    end
end
end

%%
function burnin_hs = get_data_burnin(var_info,trdata_hs,tsdata)
%%
cnum = numel(var_info.cnames);

burnin_hs = struct();
burnin_hs.nobs = size(trdata_hs.endodata.(var_info.endonames{1}),1);
burnin_hs.ntrain = 4; 
burnin_hs.nsim = burnin_hs.nobs - burnin_hs.ntrain; % fhorz
    
burnin_hs.endodata = struct();
for i = 1:numel(var_info.endonames)
    burnin_hs.endodata.(var_info.endonames{i}) = ...
        trdata_hs.endodata.(var_info.endonames{i})(1:burnin_hs.ntrain,:);
end

burnin_hs.gvdata = struct();
for i = 1:numel(var_info.gvnames)
    burnin_hs.gvdata.(var_info.gvnames{i}) = ...
        trdata_hs.gvdata.(var_info.gvnames{i})(1:burnin_hs.ntrain,1);
end

widx = ceil(burnin_hs.ntrain/trdata_hs.date_info.freq);
burnin_hs.wdata_tr = trdata_hs.wdata(1:widx*cnum,1:cnum);
burnin_hs.wdata_sim = trdata_hs.wdata((widx*cnum+1):end,:);

burnin_hs.date_info = trdata_hs.date_info;
burnin_hs.date_info.end = trdata_hs.date_info.start + widx - 1;

burnin_hs.wm_t = gen_weight(burnin_hs.wdata_tr,...
    burnin_hs.date_info.start, burnin_hs.date_info.end);

[burnin_hs.endog,burnin_hs.X] = create_endogvars(burnin_hs.endodata,...
    burnin_hs.gvdata,var_info.endoglist,var_info.cnames);

end

%% h-step ahead forecasting with bootstrapped residuals (eta)
% Simulated with Monte Carlo method
function xforc = simulate_gvar_roll_1step(estMdl_sim,var_info,...
    trdata_hs,gvdata_full,wm_t,wdata_forc,h_step,nfuture,trEIG,sim_type)
%%
freq = trdata_hs.date_info.freq;
[~,xtrain] = create_endogvars(trdata_hs.endodata,trdata_hs.gvdata,...
    var_info.endoglist,var_info.cnames);

cnum = numel(var_info.cnames);
Klist = arrayfun(@(x) numel(var_info.endoglist.(var_info.cnames{x})),1:cnum);
K = sum(Klist(:));
xforc = zeros(K,h_step,nfuture);

for jter = 1:nfuture
    err_forc = simulate_residuals(estMdl_sim.eta,h_step,trEIG,sim_type);
    xforc(:,:,jter) = forecast_gvar(h_step,wm_t,wdata_forc,freq,xtrain,...
        gvdata_full,estMdl_sim.varxlag,estMdl_sim.rank,estMdl_sim.a0,...
        estMdl_sim.a1,estMdl_sim.Theta,estMdl_sim.Lambda0,...
        estMdl_sim.Lambda,estMdl_sim.Gamma0,estMdl_sim.Gamma,...
        var_info.endoglist,var_info.exoglist,var_info.exognxlist,...
        0,[],err_forc);
end

xforc = xforc(:,end,:);
end