function W_tv = ...
    create_tv_linkmatrices(wm_t, endoglist, exognxlist, varargin)
%%
cnames = fieldnames(endoglist); cnum = numel(cnames);
K = sum(arrayfun(@(x) (numel(endoglist.(cnames{x}))), 1:cnum));

%%
if ~isempty_weight_tv(wm_t)
    yrnames = fieldnames(wm_t);
    nyear = numel(yrnames);

    fvlist = struct();
    gnvlist = struct();
    for i = 1:cnum
        fvlist.(cnames{i}) = intersect(endoglist.(cnames{i}), ...
            exognxlist.(cnames{i}), 'stable');
        gnvlist.(cnames{i}) = setdiff(exognxlist.(cnames{i}), ...
            endoglist.(cnames{i}), 'stable');
    end
    Zlist = arrayfun(@(x) numel(endoglist.(cnames{x})) + ...
        numel(fvlist.(cnames{x})), 1:cnum);

    W_tv = struct();
    for i = 1:cnum, W_tv.(cnames{i}) = zeros(Zlist(i), K, nyear); end 
%%
    for i = 1:nyear
        linkmat_z = gen_linkmatrix(wm_t.(yrnames{i}), endoglist, fvlist);
        for j = 1:cnum
            W_tv.(cnames{j})(:,:,i) = linkmat_z.(cnames{j});
            if sum(reshape(W_tv.(cnames{j})(:,:,i),1,[])) == 0
                error('CALCULATION ERROR: empty weights.');
            end
        end
    end

else
    W_tv = def_weight_tv(cnames);
end

end


