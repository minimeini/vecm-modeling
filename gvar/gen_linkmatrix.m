function [linkmat_z, linkmat_ast] = gen_linkmatrix(weight, endoglist, fvlist, varargin)
% function [linkmat_z, linkmat_ast] = gen_linkmatrix(weight, endoglist, fvlist, varargin)
% Generate single constant link matrix.
% For time-varying matrices, pleas refer to `FUN_UNDEFINED`
% 
% z_{it} = linkmat_z * x_t 
%        = (linkmat_x', linkmat_ast')' * (x_{it}', x*_{it}')'
%
% endog (endoglist, x_{it}): country-specific endogenous variables
% fv (fvlist, x*_{it,f}): proxy for the global unobserved variables (weak exogenous)
%
% Any global observable variables entering in one or more of the individual
% country models as `weakly exogenous` can be resolved through the link
% matrix W__cty, providing they also enter as a domestic variable in one
% country model.
% 
% Assumption: the weakly-exogenous global observed variables enter the last
% country model as endogenous variables.
% And thus by default, we will take the global observed variables into
% Z_{it} but not X_t

nargs = -nargin('gen_linkmatrix') - 1;
randflag = 0;
if nargin >= nargs + 1, randflag = varargin{1}; end

if ~isnumeric(randflag)
    error('INPUT ERROR: randflag should be 0 or 1.');
end

if numel(fieldnames(endoglist)) ~= numel(fieldnames(fvlist))
    error('INPUT ERROR: unequal number of countries.');
end

cnames = fieldnames(endoglist);
cnum = numel(cnames);

if ~isnumeric(weight) 
    if isstruct(weight) && numel(fieldnames(weight))==1
        fm = fieldnames(weight);
        weight = weight.(fm{1});
    else
        if size(weight,1)~=cnum || size(weight,2)~=cnum || sum(weight(:))==0
            error('INPUT ERROR: invalid weight matrix.');
        end
    end
end

if randflag == 1, weight = weight + rand(size(weight)); end

wnorm = repmat(sum(weight,1), cnum, 1);
weight = weight ./ wnorm;
K = sum(arrayfun(@(x) numel(endoglist.(cnames{x})), 1:cnum));
%% create link matrix
linkmat_ast = struct(); % create link matrix for foreign variables
linkmat_x = struct(); % create link matrix for endogenous variables
linkmat_z = struct(); % create link matrix for z

for i = 1:cnum
    linkmat_ast.(cnames{i}) = [];
    linkmat_x.(cnames{i}) = [];
    
    %% PREP: link matrix for foreign variables proxied for the unobserved
    % target_var: foreign variables to be generated
    target_var = fvlist.(cnames{i});
    
    % sort `target_var` to ensure it is in its original order
    pos = []; 
    for v = 1:numel(target_var)
        pos = [pos find(contains(endoglist.(cnames{i}), target_var{v}))];
    end
    [~, pos] = sort(pos);
    target_var = target_var(pos);
    
    %% PREP: link matrix for endogenous variables
    nendog_i = numel(endoglist.(cnames{i}));
    
    %%
    for j = 1:cnum
        %% link matrix for foreign variables proxied for the unobserved
        mats_j = gen_matrixS(target_var, endoglist.(cnames{j})); % selection matrix
        w = weight(j, i);
        wmat = w .* mats_j;
        linkmat_ast.(cnames{i}) = [linkmat_ast.(cnames{i}) wmat];
        
        clear wmat
        %% link matrix for endogenous variables
        nendog_j = numel(endoglist.(cnames{j}));
        if i == j
            if nendog_i ~= nendog_j
                error('CALCULATION ERROR: unexpected error.');
            end
            wmat = diag(diag(ones(nendog_i))); % diagonal matrix
        else
            wmat = zeros(nendog_i, nendog_j); % zero matrix
        end
        linkmat_x.(cnames{i}) = [linkmat_x.(cnames{i}) wmat];
    end
    
    %% link matrix for z
    linkmat_z.(cnames{i}) = [linkmat_x.(cnames{i}); ...
        linkmat_ast.(cnames{i})];
    if size(linkmat_z.(cnames{i}),2) ~= K
        error('CALCULATION ERROR: unexpected error.');
    end
    
    if sum(reshape(linkmat_z.(cnames{i}),1,[])) == 0
        error('CALCULATION ERROR: empty weights.');
    end
    
    if cond(linkmat_z.(cnames{i})) > 100
        error('CALCULATION ERRR: singular link matrix.');
    end
    
end



end