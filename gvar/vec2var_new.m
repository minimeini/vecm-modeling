function [const, trend, Theta, beta] = vec2var_new(vecMdl)

beta = vecMdl.Beta;
const = vecMdl.Constant + vecMdl.Adjustment * vecMdl.CointegrationConstant;
trend = vecMdl.Adjustment * vecMdl.CointegrationTrend;

nlag = vecMdl.P;
nvar = vecMdl.NumSeries;

Theta = zeros(nvar, nvar, nlag);
for i = 1:nlag
    if i == 1
        Theta(:,:,i) = diag(diag(ones(nvar))) + ...
            vecMdl.Impact + ...
            vecMdl.ShortRun{i};
    elseif i == nlag
        Theta(:,:,i) = - vecMdl.ShortRun{i-1};
    else
        Theta(:,:,i) = vecMdl.ShortRun{i} - vecMdl.ShortRun{i-1};
    end
end

end