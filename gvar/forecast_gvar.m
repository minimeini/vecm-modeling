function [yforc,eta,wm_t] = forecast_gvar(fhorz,wm_t,wdata_new,freq,...
    ytrain,gv_full,varxlag,rank,a0,a1,Theta,Lambda0,Lambda,Gamma0,...
    Gamma,endoglist,exoglist,exognxlist,varargin)

% function yforc = forecast_gvar(fhorz,wm_t,wdata_new,freq,y,gv_full,...
%     varxlag,rank,a0,a1,Theta,Lambda0,Lambda,Gamma0,Gamma,...
%     endog,endoglist,exoglist,exognxlist,varargin)
%
% ***** Workflow *****
% * iterated by forecasting horizon/step
%
% > Step 1. Get the corresponding weight, global variable and training set,
% which are trimmed for rolling h-step ahead forecasting
% > Step 2. Update model parameters and residuals by re-estimation
% > Step 3. Forecasting (horizon=h_step) and keep the last one
% > Step 4. Update training set by real and model parameters
%
% ***** Required Input *****
% > fhorz: numeric, forecasting step 'h'
% > wm_t: struct, weight matrix for the training period, fieldnames = 'y[yearnum]'
% > wdata_new: 2d numeric matrix, new slice of wdata for the forecasting period
% > freq: numeric
% > ytrain: 2d numeric matrix, training data or burn-in data, nvar * ntime
% > gv_full: global variable for training period and forecasting period
% > varxlag: struct, lag of country-specific varx, fieldnames = country
% abbrev
% > rank: struct, rank of country-specific vecm, fieldnames = country
% abbrev
%
% ***** Optional Input *****
% > update_params: 0 - no update; 1 - update by estimation; 2 - update by
% real. Default = 0
% > yreal: 2d numeric matrix, [nvar fhorz], real value of y for the 
% forecasting period, provide if update_params == 2, default = []
% > resid_rand: simulation of residuals, default = []
% > h_step: step length of forecasting, default = 1
% > lowbnd: nvar*1
% > upbnd: nvar*1
%
% ***** Output *****
% > yforc: 2d numeric matrix, forecasting results, [nvar fhorz]
% > eta: 2d numeric matrix, forecasting error, [nvar fhorz]
%
%***** EXAMPLE *****
% wdata_new = wdata_test;
% fhorz = 20;
% freq = date_info.freq;
% update_params = 0;

%% varargin: update_params, resid_rand
nargs = -nargin('forecast_gvar') - 1;

update_params = 0;
yreal = [];
resid_rand = [];
h_step = 1;
lowbnd = -9999;
upbnd = 9999;

if nargin >= nargs + 1, update_params = varargin{1}; end
if nargin >= nargs + 2, yreal =varargin{2}; end
if nargin >= nargs + 3, resid_rand = varargin{3}; end
if nargin >= nargs + 4, h_step = varargin{4}; end
if nargin >= nargs + 5, lowbnd = varargin{5}; end
if nargin >= nargs + 6, upbnd = varargin{6}; end

if update_params==2 && isempty(yreal) && fhorz>1
    error('Please provide real future values to update parameters.');
end
if ~isempty(yreal)
    if (size(ytrain,1)~=size(yreal,1)) || (size(yreal,2)<(fhorz - 1))
        error('Illegal real future values');    
    end
end
if ~isempty(resid_rand)
    if size(resid_rand,2)<fhorz
        error('samples of randomized residuals do not match with forecasting horizon');
    end
    rand_flag = 1;
else
    rand_flag = 0;
end

%%
maxlag = max(struct2array(varxlag));
cnames = fieldnames(endoglist);
K = arrayfun(@(x) numel(endoglist.(cnames{x})),1:numel(cnames));
K = sum(K(:));
yforc = zeros(K, fhorz);

%%
for i = 1:fhorz
    %%    
    % [Optional] Update training set (ytrain exog) by real every
    % forecasting step
    if i>1 && update_params~=0
        ytmp = yreal(:,i-1);
        gv_slice_update = trim_gv(gv_full,size(ytrain,2)+1);
    elseif i>1 && update_params==0
        ytmp = yforc(:,i-1);
        gv_slice_update = [];
    else
        ytmp = [];
        gv_slice_update = trim_gv(gv_full,size(ytrain,2));
    end

    [ytrain,exog] = update_data_forc(ytrain,ytmp,gv_slice_update,...
        wm_t,freq,endoglist,exoglist,exognxlist);
    ytrain_tmp = ytrain(:,1:end-h_step+1); % Trim training set by h-step
    
    % [Optional] Update model parameters and residuals by re-estimation
    if update_params ~= 0 % yes, update
        Wtv = create_tv_linkmatrices(wm_t,endoglist,exognxlist);
        [ytrain_tmp,gv_slice,eta,Sigma_eta,a0,a1,Theta,Lambda0,...
            Lambda,Gamma0,Gamma] = build_gvar(ytrain_tmp,...
            Wtv,freq,endoglist,exog,exoglist,exognxlist,varxlag,rank); 
        resid_rand = simulate_residuals(eta, fhorz);
    end
    
    % Get the corresponding weight slice and global variable for
    % forecasting
    wdata_oneyear = trim_wdata(wdata_new,i,freq);
%     gv_slice_forc = trim_gv(gv_full,size(ytrain,2)+max(h_step));
    
    % Forecasting and update wm_t
    [ynew, wm_t] = ...
        forecast_gvar_onestep(wdata_oneyear,h_step,freq,ytrain_tmp,...
        gv_full,maxlag,a0,a1,Theta,Lambda0,Lambda, ...
        Gamma0,Gamma,wm_t,endoglist,exognxlist,exoglist);
        
    if rand_flag == 1
        resid_tmp = resid_rand(:,i);
        if size(ynew,2)>1,resid_tmp=repmat(resid_tmp,1,size(ynew,2));end
        ynew = ynew + resid_tmp;
    end
    ynew = trim_by_bound(ynew,lowbnd,upbnd);
    yforc(:,i) = ynew(:,end);

end % end forecasting

%% forecasting error
eta = [];
if ~isempty(yreal), eta = yreal - yforc; end

end

%%
function yforc = trim_by_bound(yforc,lowbnd,upbnd)
ntime = size(yforc,2);
for h = 1:ntime
    del_low = yforc(:,h) < lowbnd; % =1 if <lowbnd, col vector
    yforc = double(del_low==0).*yforc(:,h) + double(del_low==1).*lowbnd;
    del_up = yforc(:,h) > upbnd;
    yforc = double(del_up==0).*yforc(:,h) + double(del_up==1).*upbnd;
end
end

%%
function gv_slice = trim_gv(gv,length)
gv_slice = [];
if ~isempty(gv)
    gvnames = fieldnames(gv);
    for g = 1:numel(gvnames)
        gv_slice.(gvnames{g}) = gv.(gvnames{g})(1:length,1);
    end
end
end

%% Update training set
function [ytrain, exog_new] = update_data_forc(yold, ynew, ...
    gvdata, wmt_new, freq, endoglist, exoglist, exognxlist)
% ***** Variables *****
% - [Output] ytrain
%   - required: yold, ynew
%   - notes: ynew can be empty and then ytrain==yold
% - [Output] exog_new
%   - required: exognx_new(yold,ynew,wmt_new,gv_full,...), gxv_slice(...)

[cnames,~,~,gvnames] = getNames(endoglist,exoglist,exognxlist);

if ~isempty(ynew)
    ytrain = [yold ynew];
else
    ytrain = yold;
end

if ~isempty(gvdata)
    endog_new = y2endog(ytrain,cnames,gvnames,endoglist);
    X = endog2X(endog_new,cnames);
    foreign = create_foreignvars(freq,wmt_new,X,endoglist,exognxlist);
    exog_new = create_exogvars(foreign,gvdata,endoglist,exoglist,...
        exognxlist);
else
    exog_new = [];
end
end