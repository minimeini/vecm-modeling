function [foreign,fvlist] = ...
    create_foreignvars(freq,wm_t,X,endoglist,exognxlist,varargin)
% function [foreign_cty, fvlist] = create_foreignvars(wm_t, dv_t, gnv, ...
%     endoglist, exoglist, varargin)
%
% ***** Input *****
% - wm_t: struct, fieldnames=yearnames'y[num]', field=[dv+fv all(dv+fv)]
% - X: [ntime nvar ntrial]
% - gnv: struct, fieldnames=gnvnames, field=[ntime ntrial] 
%
% ***** Output *****
% - foreign_cty.(cnames) = [ntimes * nforeign * ntrial]
% - fvlist.(cnames) = {fv1, ...}

%%
yearnames = fieldnames(wm_t); nyear = numel(yearnames);
cnames = fieldnames(endoglist); cnum = numel(cnames);

wm_t = normalize_wm_t(wm_t);
[ntime,~,ntrial] = size(X);

%% seperate X by year
% Xt: struct, fieldnames='y[num]', field=[K freq ntrial]
Xt = struct(); ylen = ones(1,nyear)*freq;
for i = 1:nyear
    s = (i-1)*freq+1; e = i*freq;
    if e>size(X,2), e = size(X,2); ylen(i) = e-s+1; end
    Xt.(yearnames{i}) = X(:,s:e,:);
end
%% create `fvlist`
fvlist = struct();
for n = 1:cnum
    fvlist.(cnames{n}) = intersect(endoglist.(cnames{n}), ...
        exognxlist.(cnames{n}), 'stable');
end

%% create `foreign` = [ntimes * nforeign * ntrials]
foreign = struct();
for i = 1:cnum, foreign.(cnames{i}) = []; end
fvidx = [0 arrayfun(@(x) numel(fvlist.(cnames{x})),1:cnum)];

for j = 1:nyear
    [linkmat_z, linkmat_ast] = gen_linkmatrix(wm_t.(yearnames{j}), ...
        endoglist, fvlist);
    for i = 1:cnum
        tmp = zeros(ylen(j),fvidx(i+1),ntrial);
        % tmp: freq*fvnum*ntrial
        % linkmat_ast: struct, fieldnames=cnames, field=[fvnum K]
        % Xt: struct, fieldnames='y[num]', field=[K freq ntrial]
        for k = 1:ntrial
            % tmpp: fvnum*freq
            tmpp = linkmat_ast.(cnames{i}) * Xt.(yearnames{j})(:,:,k);
            tmp(:,:,k) = tmpp'; % freq*fvnum
        end
        foreign.(cnames{i}) = cat(1,foreign.(cnames{i}),tmp);
    end
end

clear tmp
end

%%
function wm_t = normalize_wm_t(wm_t)
% simplified wm_t
% sum(wm_t.yearxx, 1) = ones add up to 1 by col (over all rows)
yearnames = fieldnames(wm_t); nyear = numel(yearnames);

for i = 1:nyear
    wm_cur = wm_t.(yearnames{i});
    if isstruct(wm_cur)
        fm = fieldnames(wm_cur);
        if numel(fm) == 1
            wm_cur = wm_cur.(fm{1});
        end
    end
    
    wnorm = repmat(sum(wm_cur,1), size(wm_cur,1), 1);
    wm_t.(yearnames{i}) = wm_cur ./ wnorm;
end
end

%% 
