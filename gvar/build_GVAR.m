function [X,gxv,eta,Sigma_eta,a0,a1,Theta,Lambda0,Lambda,Gamma0,Gamma,...
    logl,aic] = build_gvar(X,W_tv,freq,endoglist,exog,exoglist,...
    exognxlist,varxlag,rank,varargin)

%% Get Names
[cnames,endonames,fvnames,gvnames,gnvnames,cnum,~,fvnum,gvnum,gnvnum] = ...
    getNames(endoglist,exoglist,exognxlist);

%%
endog = y2endog(X,cnames,gvnames,endoglist);
maxlag = max(arrayfun(@(x) max(varxlag.(cnames{x})), 1:cnum));

nobs = size(endog.(cnames{1}), 1);
check_endog = arrayfun(@(x) size(endog.(cnames{x}),1)==nobs, 1:cnum);
if any(check_endog~=1), error('INPUT ERROR: inconsistent nobs in endog.'); end
check_exog = arrayfun(@(x) size(exog.(cnames{x}),1)==nobs, 1:cnum);
if any(check_exog~=1), error('INPUT ERROR: inconsistent nobs in exog.'); end

criterion_we = 'aic'; psc = 4; px_we = 2; qx_we = 2;
estcase = struct();
for n=1:cnum, estcase.(cnames{n}) = 4; end
%% format information about variables
fvflag = zeros(cnum, fvnum);
gvflag = zeros(cnum, gvnum);
gnvflag = zeros(cnum, gnvnum);
for n = 1:cnum
    for v = 1:fvnum
        fvflag(n,v) = ismember(fvnames{v}, exoglist.(cnames{n}));
    end
    
    if gvnum == 0, continue; end
    for g = 1:gvnum
        gvflag(n,g) = ismember(gvnames{g}, exoglist.(cnames{n}));
        if gvflag(n,g) == 0 && ismember(gvnames{g}, endoglist.(cnames{n}))
            gvflag(n,g) = 2;
        end
    end
    
    if gnvnum == 0, continue; end
    for g = 1:gnvnum
        gnvflag(n,g) = ismember(gnvnames{g}, exognxlist.(cnames{n}));
        if gnvflag(n,g) == 0 && ismember(gnvnames{g}, endoglist.(cnames{n}))
            gnvflag(n,g) = 2;
        end
    end
end

fv = exog2fv(exog, exoglist, fvnames);
gnv = endog2gv(endog, endoglist, gnvnames);
gv = endog2gv(exog, exoglist, gvnames);

gxvnames = setdiff(gvnames, gnvnames);
gxv = endog2gv(exog, exoglist, gxvnames);
%% Estimate individual VECMX*
% Estimate VECMX* for each country (exactly identified estimation)

for n = 1: cnum
    [beta.(cnames{n}), alpha.(cnames{n}), Psi.(cnames{n}), ...
        epsilon.(cnames{n}), Omega.(cnames{n}), ecm.(cnames{n}), ...
        std.(cnames{n}), logl.(cnames{n}), aic.(cnames{n}), ...
        sbc.(cnames{n}), r2.(cnames{n}), rbar2.(cnames{n}), ...
        aics.(cnames{n}), sbcs.(cnames{n}), hcwstd.(cnames{n}), ...
        nwcstd.(cnames{n}), psc_degfrsc_Fcrit_Fsc.(cnames{n})] = ...
        mlcoint(maxlag, rank.(cnames{n}), estcase.(cnames{n}), ...
        psc, endog.(cnames{n}), varxlag.(cnames{n})(1), ...
        exog.(cnames{n}), varxlag.(cnames{n})(2));
end


%% test for weak exogeneity
% test_we = ~isempty(px_we) + ~isempty(qx_we) + ~isempty(psc) + ...
%     ~isempty(criterion_we);
% 
% if test_we == 4 % perform WE test
% %%
% if strcmp(criterion_we,'aic') == 1
%     lagselect_we = 1;
%     info_we = 2;
% elseif strcmp(criterion_we,'sbc') == 1
%     lagselect_we = 1;
%     info_we = 3;
% elseif strcmp(criterion_we,'no') == 1
%     lagselect_we = 0;
% end
% 
% gvflag_we = gvflag == 1;   
% fvflag_we = fvflag;
% 
% %%
% 
% % for n=1:cnum
% %     exog_we.(cnames{n}) = [];  % creating the block of (weakly) exogenous variables
% %     exoglist_we.(cnames{n}) = {}; % creating the list of exogenous var names
% %         
% %     for i=1:fvnum
% %         if fvflag_we(n,i) == 1
% %             exog_we.(cnames{n}) = [exog_we.(cnames{n}) ...
% %                 fv.(fvnames{i}).(cnames{n})];
% %             exoglist_we.(cnames{n}) = [exoglist_we.(cnames{n}) ...
% %                 fvnames{i}];
% %         end
% %     end
% %         
% %     if not(gvnum == 0)
% %         for j=1:gvnum
% %             if gvflag_we(n,j) == 1
% %                 exog_we.(cnames{n}) = [exog_we.(cnames{n}) ...
% %                     gv.(gvnames{j})];
% %                 exoglist_we.(cnames{n}) = [exoglist_we.(cnames{n}) ...
% %                     gvnames{j}];
% %             end
% %         end
% %     end
% % end 
% 
% 
% for n = 1:cnum
%     aic_sbc_we.(cnames{n}) = [];
%     F_serialcorr_we.(cnames{n}) = [];
%     for p=1:px_we
%        for q=1:qx_we
%            [logl_aic_sbc_we  psc_degfrsc_Fcrit_Fsc_we] = ...
%                select_lags_we(psc,endog.(cnames{n}),p,...
%                exog.(cnames{n}),q,exog.(cnames{n}),...
%                ecm.(cnames{n})); 
%                                        
%            aic_sbc_we.(cnames{n}) = [aic_sbc_we.(cnames{n}); ...
%                logl_aic_sbc_we p q];
%            F_serialcorr_we.(cnames{n}) = ...
%                [F_serialcorr_we.(cnames{n}); ...
%                psc_degfrsc_Fcrit_Fsc_we];
%         end
%     end
% end
% for n=1:cnum
%     varxlag_we.(cnames{n}) = [];
%     for i=1:rows(aic_sbc_we.(cnames{1}))
%         if aic_sbc_we.(cnames{n})(i,info_we) == ...
%                 max(aic_sbc_we.(cnames{n})(:,info_we))
%             varxlag_we.(cnames{n}) = ...
%                 aic_sbc_we.(cnames{n})(i,4:5);
%         end
%     end
% end
% 
% 
% %%
% we_test = test_weakexogeneity(cnames, cnum, endog, exog, ...
%     varxlag_we, exog, ecm, rank);
% 
% else
%     we_test = [];
%     if test_we > 0
%         error('Insufficient parameters if you want to test WE.');
%     end
% end

%% SOLVING THE gvar MODEL AS A WHOLE
%% Creating link matrices W_i
% Creating W_i link matrices, z_it vectors and the  y_t global vector
% define the matrix which is gonna be used to solve the gvar

% z.(cnames{n}) = [nvar_cty, ntimes]

% z = [endog foreign]
z = struct();
for n = 1:cnum
    nvar_cty = size(W_tv.(cnames{n}), 1);
    z.(cnames{n}) = zeros(nvar_cty, nobs);
    for i = 1:nobs
        yearidx = ceil(i/freq);
        tmp = W_tv.(cnames{n})(:,:,yearidx) * X(:,i);
        
        if sum(sum(abs(tmp),2)) == 0
            error('CALCULATION ERROR: empty value.');
        end
        
        z.(cnames{n})(:, i) = tmp;
    end
end

% add strong exogenous and weakly exogenous that is not foreign
% xi = [z exog]
xi = []; 
for n = 1:cnum
    xi.(cnames{n}) = z.(cnames{n});
    exogvar = setdiff(exoglist.(cnames{n}), fvnames);
    for g = 1:numel(exogvar)
        xi.(cnames{n}) = [xi.(cnames{n}); gv.(exogvar{g})'];
    end
end


%% Retrieving the parameters of the VARX* models from the VECMX* estimates
[a0, a1, Theta, Lambda0_tmp, Lambda_tmp] =  vecx2varx(maxlag,cnum,...
    cnames,xi,endoglist,varxlag,alpha,beta,Psi,estcase);

%%
% partition the exogenous into two categories:
% * Lambda for foreign variables
% * Gamma for other exogenous: oil price, intervention dummies
Lambda0 = []; Lambda = []; Gamma0 = []; Gamma = [];
for n=1:cnum
    fvars = fvnames(fvflag(n, :)==1);
    
    Lambda0.(cnames{n}) = [];
    Lambda.(cnames{n}) = [];
    
    Gamma0.(cnames{n}) = [];
    Gamma.(cnames{n}) = [];

    for j=1:length(exoglist.(cnames{n}))
        var = exoglist.(cnames{n}){j};
        tmp = Lambda_tmp.(cnames{n})(:,j,:);
        tmp0 = Lambda0_tmp.(cnames{n})(:,j);
                
        if ismember(var, fvars)
            Lambda0.(cnames{n}) = [Lambda0.(cnames{n}) tmp0];
            Lambda.(cnames{n}) = [Lambda.(cnames{n}) tmp];
        else
            Gamma0.(cnames{n}) = [Gamma0.(cnames{n}) tmp0];
            Gamma.(cnames{n}) = [Gamma.(cnames{n}) tmp];
        end
    end
end

%% generate `gvlist`
gvlist = struct();
for i = 1:cnum
    tmp = setdiff(exoglist.(cnames{i}),fvnames(fvflag(i,:)==1),'stable');
    gvlist.(cnames{i}) = tmp;
end

%% Constructing and solving the gvar model
mod_type = 'ex'; % {'ex', 'du'}
gvloc = zeros(size(gvflag));
for i = 1:size(gvflag,1) % country
    for j = 1:size(gvflag,2) % globe variables
        varloc = 0;
        if gvflag(i,j) == 2 % as endogenous
            tmp = arrayfun(@(x) ...
                strcmp(gvnames{j}, endoglist.(cnames{i}){x}), ...
                1:numel(endoglist.(cnames{i})));
            tmp = find(tmp==1);
            if ~isempty(tmp), varloc = tmp; end
        end
        gvloc(i,j) = varloc;
    end
end

[H0, h0, h1, H, J0, J, zeta, Sigma_zeta, eta, Sigma_eta, H0y] = ...
    solve_gvar_tvw(X, gxv, gvlist, gvloc, W_tv, freq, a0, a1, ...
    Theta, Lambda0, Lambda, Gamma0, Gamma, mod_type);

% note: in the code above, we are correctly using the W matrices 
% (and not Wy, which are augmented for global exog variables)                          

end