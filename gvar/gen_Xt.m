function Xt = gen_Xt(endoglist, dv, gv)
% function Xt = gen_Xt(endoglist, dv, gv, varargin)
%
% Any global observable variables entering in one or more of the individual
% country models as `weakly exogenous` can be resolved through the link
% matrix W_cty, providing they also enter as a domestic variable in one
% country model.
% 
% Assumption: the weakly-exogenous global, such as the log of oil price, 
% observed variables enter the last country model as endogenous variables.
% 
% - Xt: 2d/3d numeric matrix, [nvar ntimes ntrials]

cnames = fieldnames(endoglist); vnames = fieldnames(dv);
ntrials = size(dv.(vnames{1}).(cnames{1}), 2);
ntimes = size(dv.(vnames{1}).(cnames{1}), 1);
%%
varidx = [0 arrayfun(@(x) numel(endoglist.(cnames{x})), 1:numel(cnames))];
K = sum(varidx);
Xt = zeros(K, ntimes, ntrials);
%%
for j = 1:numel(cnames) % country
    Kstart = sum(varidx(1:j)) + 1;
    Kend = sum(varidx(1:j+1));
    Klist = Kstart:Kend;
    cvarname = endoglist.(cnames{j});

    for k = 1:numel(cvarname) % variable
        var_tmp = cvarname{k};
        if ismember(var_tmp, fieldnames(dv))
            tmp = dv.(var_tmp).(cnames{j});
        elseif ismember(var_tmp, fieldnames(gv))
            tmp = gv.(var_tmp);
        end
        
        if size(tmp, 2)~=ntrials, tmp = tmp'; end
        Xt(Klist(k),:,:) = tmp;
    end
    
end

if size(Xt,1) ~= K
    error('CALCULATION FAILED: size(Xt,1) ~= K');
end

Xvar = sum(abs(Xt),2);
if any(Xvar == 0)
    error('CALCULATION FAILED: empty value in Xt.');
end
    
end