if we_flag == 1
    %% 3.8) Testing Weak Exogeneity

    % retrieve lag order criterion
    %*****************************
    [junk criterion_we] = xlsread(intfname,'MAIN','criterion_we'); %#ok
    
    if strcmp(criterion_we,'aic') == 1
        lagselect_we = 1;
        info_we = 2;
    elseif strcmp(criterion_we,'sbc') == 1
        lagselect_we = 1;
        info_we = 3;
    elseif strcmp(criterion_we,'no') == 1
        lagselect_we = 0;
    end
    % Information criterion used for weak exogeneity test
    % info = 2 for Akaike Information criterion;
    % info = 3 for Schwartz Bayesian Information Criterion.
    
    
    % read cells of lag orders
    % ************************************************************
    varxlag_we_tmp= xlsread(intfname,'MAIN','varxlag_we');    
        
    % retrieve specification of foreign specific vars for weak exog tests
    %******************************************************************
    fvflag_we = xlsread(intfname,'MAIN','fvflag_we');
    
    if not(gvnum == 0)
        gvflag_we_tmp = xlsread(intfname,'MAIN','gvflag_we');
        gvflag_we = [];
        for j=1:cols(gvflag_we_tmp)
            if isnan(gvflag_we_tmp(1,j))
                continue
            else
                gvflag_we(:,j) = gvflag_we_tmp(:,j); %#ok
            end
        end
    end
    
    for n=1:cnum
        exog_we.(cnames{n}) = [];  % creating the block of (weakly) exogenous variables
        exoglist_we.(cnames{n}) = {}; % creating the list of exogenous var names
        
        for i=1:fvnum
            if fvflag_we(n,i) == 1
                exog_we.(cnames{n}) = [exog_we.(cnames{n}) fv.(fvnames{i}).(cnames{n})];
                exoglist_we.(cnames{n}) = [exoglist_we.(cnames{n}) fvnames{i}];
            end
        end
        
        if not(gvnum == 0)
            for j=1:gvnum
                if gvflag_we(n,j) == 1
                    exog_we.(cnames{n}) = [exog_we.(cnames{n}) gv.(gvnames{j})];
                    exoglist_we.(cnames{n}) = [exoglist_we.(cnames{n}) gvnames{j}];
                end
            end
        end
    end        
    

    % Lag orders of weak exogeneity test
    % **********************************
    if  lagselect_we == 1 % lag order selection is performed

        % retrieve maximum lag order for serial correlation test
        %*******************************************************
        psc_we = xlsread(intfname,'MAIN','psc_we');
    
        % retrieve maximum lag orders of WE regression equations
        %*******************************************************
        px_we = xlsread(intfname,'MAIN','px_we'); 
        qx_we = xlsread(intfname,'MAIN','qx_we');
        
        for n=1:cnum
            aic_sbc_we.(cnames{n}) = [];
            F_serialcorr_we.(cnames{n}) = [];
            for p=1:px_we
                for q=1:qx_we
                   [logl_aic_sbc_we  psc_degfrsc_Fcrit_Fsc_we] = select_lags_we(psc_we,...
                    endog.(cnames{n}),p,exog.(cnames{n}),q,exog_we.(cnames{n}),ecm.(cnames{n})); 
                                       
                   aic_sbc_we.(cnames{n}) = [aic_sbc_we.(cnames{n}); logl_aic_sbc_we p q];
                   F_serialcorr_we.(cnames{n}) = [F_serialcorr_we.(cnames{n}); psc_degfrsc_Fcrit_Fsc_we];
                end
            end
        end
        for n=1:cnum
            varxlag_we.(cnames{n}) = [];
            for i=1:rows(aic_sbc_we.(cnames{1}))
                if aic_sbc_we.(cnames{n})(i,info_we) == max(aic_sbc_we.(cnames{n})(:,info_we))
                    varxlag_we.(cnames{n}) = aic_sbc_we.(cnames{n})(i,4:5);
                end
            end
        end
        
        varxlag_we_tmp = [];
        for n=1:cnum
            varxlag_we_tmp = [varxlag_we_tmp; varxlag_we.(cnames{n})]; %#ok
        end
        
    % print to the interface file the resulting lag orders
    %**************************************************************************
    
    end
    
    % retrieve lag orders from gvar.xls
    varxlag_we_tmp= xlsread(intfname,'MAIN','varxlag_we');  
    for n=1:cnum
        varxlag_we.(cnames{n}) = varxlag_we_tmp(n,:);
    end
    clear varxlag_we_tmp
    
    
    
    % do weak exogeneity test
    %**************************************************************************
    wetest_stat = test_weakexogeneity(cnames,cnum,endog,exog,varxlag_we,exog_we,ecm,rank);    
end