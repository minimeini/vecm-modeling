%% definition of W_tv or wm_t
function emptyW_tv = def_weight_tv(fname,varargin)

nargs = -nargin('def_weight_tv') - 1;
weight_type = 'wtv'; % wtv or wmt
if nargin >= nargs + 1
    if ~ischar(varargin{1})
        error('INPUT ERROR: weight_type = ''wtv'' or ''wmt''');
    end
    weight_type = varargin{1};
end

if isempty(fname), fname = 'init'; end

%%
switch weight_type
    case 'wtv'
        cnames = fname;
        emptyW_tv = struct();
        for i = 1:numel(cnames)
            emptyW_tv.(cnames{i}) = [];
        end
    case 'wmt'
        yrname = fname;
        emptyW_tv = struct();
        if ischar(yrname)
            emptyW_tv.(yrname) = [];
        elseif iscell(yrname)
            for i = 1:numel(yrname)
                emptyW_tv.(yrname{i}) = [];
            end
        end
end


end