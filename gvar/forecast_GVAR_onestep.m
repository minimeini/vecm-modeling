function [yest, wm_t] = ...
    forecast_gvar_onestep(wdata_slice, h_step, freq, y, gv_slice, maxlag, ...
    a0, a1, Theta, Lambda0, Lambda, Gamma0, Gamma, wm_t, ...
    endoglist, exognxlist, exoglist, varargin)

%% validate input
if isempty(wdata_slice)~=isempty(Lambda0) ...
        || isempty(wdata_slice)~=isempty(Lambda)
    error('INPUT ERROR: wdata_slice, Lambda0, Lambda, exognxlist, exoglist');
end
if ~isempty_weight_tv(wm_t)&&~match_weightANDdata(size(y,2), freq, wm_t)
    error('INPUT ERROR: time-varying weights do not match with data.');
end

%% OPTIONAL VARIABLES: default values
nargs = -nargin('forecast_gvar_onestep') - 1;
init_year = 1992;

%%
cnames = fieldnames(endoglist);
cnum = numel(cnames);
tridxlast = size(y, 2);

fvlist = struct();
gvlist = struct();
gvfull = [];
for i = 1:cnum
    fvlist.(cnames{i}) = intersect(endoglist.(cnames{i}), ...
        exognxlist.(cnames{i}),'stable');
    gvlist.(cnames{i}) = setdiff(exoglist.(cnames{i}), ...
        endoglist.(cnames{i}), 'stable');
    gvfull = union(gvfull, gvlist.(cnames{i}), 'stable');
end
gvfull = sort_names(gvfull, gvlist)';

gvcheck = arrayfun(@(x) ismember(gvfull{x},fieldnames(gv_slice)),...
    1:numel(gvfull));
if any(gvcheck==0), error('INPUT ERROR: error in `gv_slice`.'); end

%% find the corresponding year
yearnames = fieldnames(wm_t);
yearnames = arrayfun(@(x) str2double(yearnames{x}(2:end)),1:numel(yearnames));

estimate_year = ceil(tridxlast / freq);
yearidx = ceil((tridxlast+1) / freq);
isnextyear = yearidx > estimate_year;

if isempty_weight_tv(wm_t)
    wm_t = def_weight_tv(['y' num2str(init_year)],'wmt');
    isnextyear = 1;
end
W_tv = create_tv_linkmatrices(wm_t, endoglist, exognxlist);

if ~isempty(wdata_slice)&&~isempty(endoglist)&&~isempty(exognxlist)
    if isnextyear == 1
        yname = yearnames(end) + 1;
    else
        yname = yearnames(end);
    end
    wm_new = gen_weight(wdata_slice, yname, yname);
    
    if isnextyear == 1
        % update wm_t and
        fm = fieldnames(wm_new);
        if numel(fm)>1
            error('CALCULATION ERROR: wm_new is one year.');
        end
        wm_t.(fm{1}) = wm_new.(fm{1});

        if ~match_weightANDdata(tridxlast+1, freq, wm_t)
            fprintf('coly+1: %d, freq: %d, wm_t: %d', ...
                tridxlast+1, freq, numel(fieldnames(wm_t)));
            error('CALCULATION ERROR: new time-varying weights do not match with new data.');
        end
    end

    linkmat_z = create_tv_linkmatrices(wm_new,endoglist,exognxlist);
    widx = timeMap(tridxlast+1, maxlag, tridxlast+1, freq);
    widx = fliplr(widx);
else
    linkmat_z = [];
    widx = [];
end

%% stack all global variables
% include strong exogenous: exogenous in all models
% include weak exogenous: endogenous in one model
% exclude foreign variables

gvt = []; % dt = ngv * ntimes
if ~isempty(gvfull)
    for i = 1:numel(gvfull)
        tmp = gv_slice.(gvfull{i});
        if size(tmp,1) ~= 1, tmp = tmp'; end
        gvt = [gvt; tmp];
    end
end

%% stack parameters
[H0,h0,h1,H,J0,J] = stack_params(W_tv,gvlist,a0,a1,Theta,Lambda0,...
    Lambda,Gamma0,Gamma);
if ~isempty(wm_t) && ~isempty(W_tv)
    if isnextyear == 1   
        % update G0 (H0)
        % H0_new: K*K numeric matrix
        H0_new = gen_stackpar(linkmat_z, gvlist, a0, a1, Theta, ...
            Lambda0, Lambda, Gamma0, Gamma);
    else
        H0_new = H0(:,:,end);
    end
else
    H0_new = H0;
end

if numel(size(H0_new)) ~= 2
    disp(size(H0_new));
    error('CALCULATION ERROR: wrong dimension of H0_new.');
end

%% forecast next point: h-step ahead, as a whole
ytr = y; % nvar * nobs
for h = 1:max(h_step)
    yest = estY_onestep(h0,h1,H0_new,H,J0,J,ytr,gvt,widx);
    ytr = [ytr yest];
end

yest = ytr(:,size(y,2)+1:end); % nvar * nhstep


end

%% 1-step estimation
function yest = estY_onestep(h0,h1,H0,H,J0,J,y,gvt,widx)
tridxlast = size(y,2);
current_time = tridxlast + 1;
% current time index = tridxlast + 1

% add constant and trend
yest = h0 + h1 * (tridxlast+1);

% add contemperous exogenous
if ~isempty(gvt)
    diff = size(J0,2) - size(gvt,1);
    if diff ~= 0
        error('CALCULATION ERROR: unconsistent dimension between J0 and dt.');
    end
    yest = yest + J0 * gvt(:,current_time);
end

% add lagged exogenous and lagged endogenous
for j = 1:size(H,3)
    if numel(size(H)) == 4
        if ~isempty(widx)
            yest = yest + squeeze(H(:,:,j,widx(j+1))) * y(:,current_time-j);
        else
            yest = yest + squeeze(H(:,:,j,end)) * y(:,current_time-j);
        end
    elseif numel(size(H)) == 3
        yest = yest + squeeze(H(:,:,j)) * y(:,current_time-j);
    end
    
    diff = size(J,2) - size(gvt,1);
    if ~isempty(gvt)
        if diff ~= 0
            error('CALCULATION ERROR: unconsistent dimension between J0 and dt.');
        else
            Jtmp = J(:,:,j);
        end
        yest = yest + Jtmp * gvt(:,current_time-j);
    end
end

if cond(H0) < 100
    yest = invMatrix(H0, 'QR') * yest;
else
    yest = invMatrix(H0, 'pseudo') * yest;
end
end