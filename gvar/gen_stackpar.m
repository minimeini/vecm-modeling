function [G0, G, J0, J, A0, A1, stat] = gen_stackpar(linkmat_z,gvlist,...
    a0,a1,Theta,Lambda0,Lambda,Gamma0,Gamma,varargin)
% function stackpar = gen_stackpar(linkmat_z, a0, a1, Theta, Lambda0, Lambda, Gamma0, Gamma, mod_type)

%% Basic Information
cnames = fieldnames(a0);
cnum = numel(cnames);  

Klist = arrayfun(@(x) size(a0.(cnames{x}), 1), 1:cnum);
K = sum(Klist);
if ~isempty(Lambda)
    lagp = max([arrayfun(@(x) (size(Theta.(cnames{x}), 3)), 1:cnum) ...
        arrayfun(@(x) (size(Lambda.(cnames{x}), 3)), 1:cnum)]);
elseif ~isempty(Gamma)
    lagp = max([arrayfun(@(x) (size(Theta.(cnames{x}), 3)), 1:cnum) ...
        arrayfun(@(x) (size(Gamma.(cnames{x}), 3)), 1:cnum)]);
else
    lagp = max(arrayfun(@(x) (size(Theta.(cnames{x}), 3)), 1:cnum));
end

gvfull = [];
for i = 1:cnum
    gvfull = union(gvfull, gvlist.(cnames{i}));
end
gvfull = sort_names(gvfull, gvlist)';
gvnum = numel(gvfull);
%% Augment the `Gamma` so that we have the same dimension of exogs 
% Gamma and Gamma0 are for exogenous variables only, where foreign
% variables are excluded
% fill the Gamma and Gamm0 with all-zero columns
flagG = 0; flagG0 = 0;

if ~isempty(Gamma)
    Gamma_tmp = Gamma;
    Gamma = struct();
    flagG = 1;
end

if ~isempty(Gamma0)
    Gamma0_tmp = Gamma0;
    Gamma0 = struct();
    flagG0 = 1;
end

%%
for i = 1:cnum
    % we assume that gvars in each country model are sorted as in gvfull
    % but some of the variables in gvfull might disappear in country gvars
    % - gvfull includes exogenous which are treated as endogeous in one of
    % the country model, exogenous which are treated as exogenous in all
    % models
    % - foreign variables are excluded
    idx_tofill = arrayfun(@(x) ismember(gvfull{x}, gvlist.(cnames{i})), ...
        1:gvnum);
    idx_tofill = find(~idx_tofill);
    
    if ~isempty(idx_tofill)
        if flagG0, tmp0 = zeros(Klist(i), gvnum); end
        if flagG, tmp = zeros(Klist(i), gvnum, lagp); end
        
        cnt = 1;
        for j = 1:gvnum
            if ~ismember(j, idx_tofill)
                if flagG0==1 && ~isempty(Gamma0_tmp.(cnames{i}))
                    tmp0(:,j) = squeeze(Gamma0_tmp.(cnames{i})(:,cnt)); 
                end
                if flagG==1 && ~isempty(Gamma_tmp.(cnames{i}))
                    for k = 1:lagp
                        tmp(:,j,k) = squeeze(Gamma_tmp.(cnames{i})(:,cnt,k));
                    end
                end
                cnt = cnt + 1;
            end
        end
        
        if flagG==1, Gamma.(cnames{i}) = tmp; end
        if flagG0==1, Gamma0.(cnames{i}) = tmp0; end
    else
        if flagG==1, Gamma.(cnames{i}) = Gamma_tmp.(cnames{i}); end
        if flagG0==1, Gamma0.(cnames{i}) = Gamma0_tmp.(cnames{i}); end
    end
    
end
    
%% Augment `Theta`, `Lambda` and `Gamma` with zero matrices to satisfy `lagp`
for i = 1:cnum
    tmp = Theta.(cnames{i});
    dim_tmp = size(tmp);
    if numel(dim_tmp) == 2
        delta_lag = lagp - 1;
    else
        delta_lag = lagp - dim_tmp(3);
    end
    
    if delta_lag > 0
        augment = zeros([dim_tmp(1:2) delta_lag]);
        Theta.(cnames{i}) = cat(3, tmp, augment);
    end
    
    clear tmp dim_tmp augment delta_lag
    
    if ~isempty(Lambda)
        tmp = Lambda.(cnames{i});
        dim_tmp = size(tmp);
        if numel(dim_tmp) == 2
            delta_lag = lagp - 1;
        else
            delta_lag = lagp - dim_tmp(3);
        end
    
        if delta_lag > 0
            augment = zeros([dim_tmp(1:2) delta_lag]);
            Lambda.(cnames{i}) = cat(3, tmp, augment);
        end
    end
    
    if ~isempty(Gamma)
        tmp = Gamma.(cnames{i});
        dim_tmp = size(tmp);
        if numel(dim_tmp) == 2
            delta_lag = lagp - 1;
        else
            delta_lag = lagp - dim_tmp(3);
        end
    
        if delta_lag > 0
            augment = zeros([dim_tmp(1:2) delta_lag]);
            Gamma.(cnames{i}) = cat(3, tmp, augment);
        end
    end
end

%% stack matrices
stat = zeros(cnum, 2); % col1 = cond(Lambda[i0]), col2 = cond(G[i0])

A0 = zeros(K, 1); A1 = zeros(K, 1);
G0 = zeros(K, K); G = zeros(K, K, lagp);
J0 = zeros(K, gvnum); J = zeros(K, gvnum, lagp);
kstart = 1; 

for i = 1:cnum
    %%
    kend = kstart + Klist(i) - 1;
    
    if ~isempty(a0)
        % create A0
        A0(kstart:kend, 1) = a0.(cnames{i});
    end
    
    if ~isempty(a1)
        % create A1
        A1(kstart:kend, 1) = a1.(cnames{i});
    end
    
    % create G0
    if ~isempty(Lambda0)
        stat(i,1) = cond(-Lambda0.(cnames{i}));
        A_i0_tmp = [diag(diag(ones(Klist(i)))) -Lambda0.(cnames{i})];
        if cond(A_i0_tmp) > 100
            warning('CALCULATION: Singular A0[i].');
        end
        G_i0_tmp = A_i0_tmp;
        if ~isempty(linkmat_z)
            G_i0_tmp = A_i0_tmp * linkmat_z.(cnames{i});
        end
        stat(i,2) = cond(G_i0_tmp);
        if cond(G_i0_tmp) > 100
            warning('CALCULATION: Singular G0[i].');
        end
    else % isempty(Lambda0)
        G_i0_tmp = diag(diag(ones(Klist(i))));
    end
    G0(kstart:kend, :) = G_i0_tmp;
    
    if ~isempty(Theta)
    % create G
        for j = 1:lagp
            G_ij_tmp = Theta.(cnames{i})(:,:,j);
            if ~isempty(Lambda)
                G_ij_tmp = [G_ij_tmp Lambda.(cnames{i})(:,:,j)];
            end
            if ~isempty(linkmat_z)
                G_ij_tmp = G_ij_tmp * linkmat_z.(cnames{i});
            end
            G(kstart:kend, :, j) = G_ij_tmp;
        end
    end
    
    if flagG0 == 1
        % create J0
        J0(kstart:kend, :) = Gamma0.(cnames{i});
    end
    
    if flagG == 1
        % create J
        for j = 1:lagp
            J(kstart:kend, :, j) = Gamma.(cnames{i})(:,:,j);
        end
    end
    
    kstart = kend + 1;
end

%%
if isempty(G0) || sum(abs(G0(:))) == 0
    error('CALCULATION ERROR: empty G0.');
end

if cond(G0) > 200
    fprintf('condition number: %d, inverse error: %d.\n', ...
        cond(G0), norm(G0\G0-diag(diag(ones(101)))));
    warning('CALCULATION CONCERN: large condition number, G0 might be singular.');
end

end